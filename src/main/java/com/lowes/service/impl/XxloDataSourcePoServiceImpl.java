package com.lowes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowes.dao.XxloDataSourcePoDAO;
import com.lowes.entity.Usuario;
import com.lowes.entity.XxloDataSourcePo;
import com.lowes.service.XxloDataSourcePoService;

@Service
@Transactional
public class XxloDataSourcePoServiceImpl implements XxloDataSourcePoService {
	
	@Autowired
	private XxloDataSourcePoDAO xxloDataSourcePoDAO;

	@Override
	public Integer createXxloDataSourcePo(XxloDataSourcePo xxloDataSourcePo) {
		return xxloDataSourcePoDAO.createXxloDataSourcePo(xxloDataSourcePo);
	}

	@Override
	public XxloDataSourcePo updateViajeConcepto(XxloDataSourcePo xxloDataSourcePo) {
		return xxloDataSourcePoDAO.updateViajeConcepto(xxloDataSourcePo);
	}

	@Override
	public void deleteXxloDataSourcePo(Integer id) {
		xxloDataSourcePoDAO.deleteXxloDataSourcePo(id);		
	}

	@Override
	public List<XxloDataSourcePo> getAllXxloDataSourcePo() {
		return xxloDataSourcePoDAO.getAllXxloDataSourcePo();
	}

	@Override
	public XxloDataSourcePo getXxloDataSourcePo(Integer id) {
		return xxloDataSourcePoDAO.getXxloDataSourcePo(id);
	}

	@Override
	public boolean tieneRecibosNoFacturados(List<XxloDataSourcePo> ordenesCompra) {
		return xxloDataSourcePoDAO.tieneRecibosNoFacturados(ordenesCompra);
	}

	@Override
	public List<XxloDataSourcePo> getOrdenesCompraRestricciones(Usuario empleado, Integer tipoFactura) {
		return xxloDataSourcePoDAO.getOrdenesCompraRestricciones(empleado, tipoFactura);
	}

	@Override
	public List<XxloDataSourcePo> getOrdenCompra(String idOrdenCompra, Integer tipoFactura, Integer check_association) {
		return xxloDataSourcePoDAO.getOrdenCompra(idOrdenCompra, tipoFactura, check_association);
	}

	@Override
	public List<XxloDataSourcePo> getPOsByIDPO(String po) {
		return xxloDataSourcePoDAO.getPOsByIDPO(po);
	}

	@Override
	public List<XxloDataSourcePo> getSKUsByRecibos(String num_recibo, Integer tipoFactura, String ordenCompra, Boolean ignorarCheckAssociation) {
		return xxloDataSourcePoDAO.getSKUsByRecibos(num_recibo,tipoFactura,ordenCompra,ignorarCheckAssociation);
	}

	@Override
	public boolean updateRecibosEnFactura(String numero_recibo, Boolean reservado, Integer tipoFactura, String ordenCompra) {
		return xxloDataSourcePoDAO.updateRecibosEnFactura(numero_recibo, reservado, tipoFactura, ordenCompra);
	}
	
}
