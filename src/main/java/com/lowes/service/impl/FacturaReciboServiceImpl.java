package com.lowes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowes.dao.FacturaReciboDAO;
import com.lowes.entity.FacturaRecibo;
import com.lowes.service.FacturaReciboService;


@Service
@Transactional
public class FacturaReciboServiceImpl implements FacturaReciboService {
	
	@Autowired
	private FacturaReciboDAO facturaReciboDAO;

	@Override
	public Integer createFacturaRecibo(FacturaRecibo recibo) {
		return facturaReciboDAO.createFacturaRecibo(recibo);
	}

	@Override
	public FacturaRecibo updateFacturaRecibo(FacturaRecibo recibo) {
		return facturaReciboDAO.updateFacturaRecibo(recibo);
	}

	@Override
	public void deleteFacturaRecibo(FacturaRecibo recibo) {
		facturaReciboDAO.deleteFacturaRecibo(recibo);		
	}

	@Override
	public List<FacturaRecibo> getAllFacturaRecibo() {
		return facturaReciboDAO.getAllFacturaRecibo();
	}

	@Override
	public FacturaRecibo getFacturaRecibo(Integer id) {
		return facturaReciboDAO.getFacturaRecibo(id);
	}

	@Override
	public List<FacturaRecibo> getAllFacturaRecibobyFactura() {
		return null;
	}

	@Override
	public List<FacturaRecibo> getAllFacturaRecibobyFactura(Integer idFacura) {
		return facturaReciboDAO.getAllFacturaRecibobyFactura(idFacura);
	}

}
