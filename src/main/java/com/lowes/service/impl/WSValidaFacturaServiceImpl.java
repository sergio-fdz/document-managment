package com.lowes.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowes.service.WSValidaFacturaService;
import com.lowes.util.WSFacturaMessageResponse;
import com.lowes.util.WSValidaFacturaClient;

@Service
@Transactional
public class WSValidaFacturaServiceImpl implements WSValidaFacturaService {

	@Autowired
	private WSValidaFacturaClient wsValidaFacturaClient; 
	
	public WSValidaFacturaServiceImpl() {

	}
	
	@Override
	public List<WSFacturaMessageResponse> validaFactura(byte[] archivo,
			String nombreArchivo, String rfcEmisor, String rfcReceptor,
			String importeTotal, String endpoint, String token) {
		return wsValidaFacturaClient.validarFactura(archivo, nombreArchivo, rfcEmisor, rfcReceptor, importeTotal, endpoint, token);
	}
//Method to validate konesh webservice
	@Override
	public HashMap<String, String> validaFacturaKonesh(String archivo, String CuentaTimbrado, String TokenTimbrado,
			String UsuarioTimbrado, String PasswordTimbrado, String endpoint, String rfcReceptor) {
		// TODO Auto-generated method stub
		return wsValidaFacturaClient.validaFacturaKonesh(archivo, CuentaTimbrado, TokenTimbrado, UsuarioTimbrado, PasswordTimbrado, endpoint, rfcReceptor);
	}

}
