package com.lowes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowes.dao.XxloRequisitionPoDAO;
import com.lowes.entity.Usuario;
import com.lowes.entity.XxloDataSourcePo;
import com.lowes.entity.XxloRequisitionPo;
import com.lowes.service.XxloRequisitionPoService;

@Service
@Transactional
public class XxloRequisitionPoServiceImpl implements XxloRequisitionPoService {
	
	@Autowired
	private XxloRequisitionPoDAO xxloRequisitionPoDAO;

	@Override
	public Integer createXxloRequisitionPo(XxloRequisitionPo xxloRequisitionPo) {
		return xxloRequisitionPoDAO.createXxloRequisitionPo(xxloRequisitionPo);
	}

	@Override
	public XxloRequisitionPo updateViajeConcepto(XxloRequisitionPo xxloRequisitionPo) {
		return xxloRequisitionPoDAO.updateViajeConcepto(xxloRequisitionPo);
	}

	@Override
	public void deleteXxloRequisitionPo(Integer id) {
		xxloRequisitionPoDAO.deleteXxloRequisitionPo(id);		
	}

	@Override
	public List<XxloRequisitionPo> getAllXxloRequisitionPo() {
		return xxloRequisitionPoDAO.getAllXxloRequisitionPo();
	}

	@Override
	public XxloRequisitionPo getXxloRequisitionPo(Integer id) {
		return xxloRequisitionPoDAO.getXxloRequisitionPo(id);
	}

	@Override
	public List<XxloRequisitionPo> getOrdenesCompraRestricciones(Usuario empleado, Integer tipoFactura) {
		return xxloRequisitionPoDAO.getOrdenesCompraRestricciones(empleado, tipoFactura);
	}

	@Override
	public boolean tieneRecibosNoFacturados(List<XxloRequisitionPo> ordenesCompra) {
		return xxloRequisitionPoDAO.tieneRecibosNoFacturados(ordenesCompra);
	}

}
