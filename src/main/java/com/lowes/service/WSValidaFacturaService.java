package com.lowes.service;

import java.util.HashMap;
import java.util.List;

import com.lowes.util.WSFacturaMessageResponse;

public interface WSValidaFacturaService {

	public abstract List<WSFacturaMessageResponse> validaFactura(byte [] archivo, String nombreArchivo, String rfcEmisor, String rfcReceptor, String importeTotal, String endpoint, String token);
	public abstract HashMap<String, String> validaFacturaKonesh(String archivo, String CuentaTimbrado, String TokenTimbrado, String UsuarioTimbrado, String PasswordTimbrado, String endpoint, String rfcReceptor);
	
	
}