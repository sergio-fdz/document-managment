package com.lowes.service;

import java.util.List;

import com.lowes.entity.Usuario;
import com.lowes.entity.XxloDataSourcePo;

public interface XxloDataSourcePoService {
	
	public Integer createXxloDataSourcePo(XxloDataSourcePo xxloDataSourcePo);
    public XxloDataSourcePo updateViajeConcepto(XxloDataSourcePo xxloDataSourcePo);
    public void deleteXxloDataSourcePo(Integer id);
    public List<XxloDataSourcePo> getAllXxloDataSourcePo();
    public XxloDataSourcePo getXxloDataSourcePo(Integer id);
    public List<XxloDataSourcePo> getOrdenesCompraRestricciones(Usuario empleado, Integer tipoFactura);
    public boolean tieneRecibosNoFacturados(List<XxloDataSourcePo> ordenesCompra);
    public List<XxloDataSourcePo> getOrdenCompra(String idOrdenCompra, Integer tipoFactura,  Integer check_association);
    public List<XxloDataSourcePo> getPOsByIDPO(String po);
    public List<XxloDataSourcePo> getSKUsByRecibos(String num_recibo, Integer tipoFactura, String ordenCompra, Boolean ignorarCheckAssociation);
    public boolean updateRecibosEnFactura(String numero_recibo, Boolean reservado, Integer tipoFactura, String ordenCompra);





}
