package com.lowes.service;

import java.util.List;

import com.lowes.entity.FacturaRecibo;

public interface FacturaReciboService {
	
	public Integer createFacturaRecibo(FacturaRecibo recibo);
    public FacturaRecibo updateFacturaRecibo(FacturaRecibo recibo);
    public void deleteFacturaRecibo(FacturaRecibo recibo);
    public List<FacturaRecibo> getAllFacturaRecibo();
    public FacturaRecibo getFacturaRecibo(Integer id);
    public List<FacturaRecibo> getAllFacturaRecibobyFactura();
    public List<FacturaRecibo> getAllFacturaRecibobyFactura(Integer idFacura);



}
