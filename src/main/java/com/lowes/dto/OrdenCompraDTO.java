package com.lowes.dto;

public class OrdenCompraDTO {
	
	private String numeroOrdCompra;
	private String descripcion;
	private String numeroEmpleado;
	
	
	public String getNumeroOrdCompra() {
		return numeroOrdCompra;
	}
	public void setNumeroOrdCompra(String numeroOrdCompra) {
		this.numeroOrdCompra = numeroOrdCompra;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNumeroEmpleado() {
		return numeroEmpleado;
	}
	public void setNumeroEmpleado(String numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}
	
	
}
