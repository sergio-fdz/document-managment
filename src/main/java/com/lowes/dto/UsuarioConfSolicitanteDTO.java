package com.lowes.dto;

public class UsuarioConfSolicitanteDTO {
	
	private String nombreUsuario;
	private int idTipoSolicitud;
	private int locacion;
	private int cuentaContable;
	private int usuario;
	private Boolean activo;
	
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	public Integer getIdTipoSolicitud() {
		return idTipoSolicitud;
	}

	public void setIdTipoSolicitud(Integer idTipoSolicitud) {
		this.idTipoSolicitud = idTipoSolicitud;
	}
	
	public Integer getLocacion() {
		return locacion;
	}

	public void setLocacion(Integer locacion) {
		this.locacion = locacion;
	}
	
	public Integer getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable(Integer cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	
	public Integer getUsuario() {
		return usuario;
	}

	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}
	
	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
}