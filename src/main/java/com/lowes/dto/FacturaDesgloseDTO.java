package com.lowes.dto;

import java.util.ArrayList;
import java.util.List;

import com.lowes.entity.Aid;
import com.lowes.entity.CategoriaMayor;
import com.lowes.entity.CategoriaMenor;
import com.lowes.entity.Compania;
import com.lowes.entity.CuentaContable;
import com.lowes.entity.Locacion;

public class FacturaDesgloseDTO {
	
	private Locacion locacion;
	private CuentaContable cuentaContable;
	private String concepto;
	private Aid aid;
	private CategoriaMayor categoriaMayor;
	private CategoriaMenor categoriaMenor;
	private String strSubTotal;
	private Integer indexInput;
	private Integer indexFactura;
	private Integer usuarioSolicitante;
	private Integer tipoSolicitud;
	private Integer index;
	private boolean trackAsset;
	private String par;
	private Compania libroContable;
	
	/*Orden de compra */
	private String num_recibo;
	private Integer cantidad;
	private String sku;
	private String descripcion_articulo;
	private String precio_unitario;
	private Integer id_tabla_po;
	
	
	private List<CuentaContable> cuentasContables = new ArrayList<>();
	
	public FacturaDesgloseDTO(){
		
	}
	
	public FacturaDesgloseDTO(String strSubTotal,  Locacion locacion, CuentaContable cuentaContable , String concepto, Aid aid, CategoriaMayor categoriaMayor, CategoriaMenor categoriaMenor,
			boolean trackAsset, String par, Compania libroContable){
		this.strSubTotal = strSubTotal;
		this.locacion = locacion;
		this.cuentaContable = cuentaContable;
		this.concepto = concepto;
		this.aid = aid;
		this.categoriaMayor = categoriaMayor;
		this.categoriaMenor = categoriaMenor;
		this.trackAsset = trackAsset;
		this.par = par;
		this.libroContable = libroContable;
	}
	
	
	public String getStrSubTotal() {
		return strSubTotal;
	}

	public void setStrSubTotal(String strSubTotal) {
		this.strSubTotal = strSubTotal;
	}

	public Locacion getLocacion() {
		return locacion;
	}
	public void setLocacion(Locacion locacion) {
		this.locacion = locacion;
	}
	public CuentaContable getCuentaContable() {
		return cuentaContable;
	}
	public void setCuentaContable(CuentaContable cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Aid getAid() {
		return aid;
	}
	public void setAid(Aid aid) {
		this.aid = aid;
	}
	public CategoriaMayor getCategoriaMayor() {
		return categoriaMayor;
	}
	public void setCategoriaMayor(CategoriaMayor categoriaMayor) {
		this.categoriaMayor = categoriaMayor;
	}
	public CategoriaMenor getCategoriaMenor() {
		return categoriaMenor;
	}
	public void setCategoriaMenor(CategoriaMenor categoriaMenor) {
		this.categoriaMenor = categoriaMenor;
	}

	public Integer getIndexInput() {
		return indexInput;
	}
	public void setIndexInput(Integer indexInput) {
		this.indexInput = indexInput;
	}

	public Integer getIndexFactura() {
		return indexFactura;
	}

	public void setIndexFactura(Integer indexFactura) {
		this.indexFactura = indexFactura;
	}

	public Integer getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	public void setUsuarioSolicitante(Integer usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	public Integer getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(Integer tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getNum_recibo() {
		return num_recibo;
	}

	public void setNum_recibo(String num_recibo) {
		this.num_recibo = num_recibo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getDescripcion_articulo() {
		return descripcion_articulo;
	}

	public void setDescripcion_articulo(String descripcion_articulo) {
		this.descripcion_articulo = descripcion_articulo;
	}

	public String getPrecio_unitario() {
		return precio_unitario;
	}

	public void setPrecio_unitario(String precio_unitario) {
		this.precio_unitario = precio_unitario;
	}

	public Integer getId_tabla_po() {
		return id_tabla_po;
	}

	public void setId_tabla_po(Integer id_tabla_po) {
		this.id_tabla_po = id_tabla_po;
	}

	public List<CuentaContable> getCuentasContables() {
		return cuentasContables;
	}

	public void setCuentasContables(List<CuentaContable> cuentasContables) {
		this.cuentasContables = cuentasContables;
	}
	
	public boolean isTrackAsset() {
		return trackAsset;
	}
	
	public boolean getTrackAsset() {
		return trackAsset;
	}
	
	public void setTrackAsset(boolean trackAsset) {
		this.trackAsset = trackAsset;
	}

	public String getPar() {
		return par;
	}

	public void setPar(String par) {
		this.par = par;
	}
	
	public Compania getLibroContable() {
		return libroContable;
	}

	public void setLibroContable(Compania libroContable) {
		this.libroContable = libroContable;
	}
	
}
