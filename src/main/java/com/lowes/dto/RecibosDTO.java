package com.lowes.dto;

public class RecibosDTO {
	
	private String recipt_num;
	private Integer receipt_num_integer;
	private Double montoTotal;
	private String montoTotalString;
	public String po;
	
	public String getPo() {
		return po;
	}
	public void setPo(String po) {
		this.po = po;
	}
	public String getRecipt_num() {
		return recipt_num;
	}
	public void setRecipt_num(String recipt_num) {
		this.recipt_num = recipt_num;
	}
	public String getMontoTotalString() {
		return montoTotalString;
	}
	public void setMontoTotalString(String montoTotalString) {
		this.montoTotalString = montoTotalString;
	}
	public Double getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}
	public Integer getReceipt_num_integer() {
		return receipt_num_integer;
	}
	public void setReceipt_num_integer(Integer receipt_num_integer) {
		this.receipt_num_integer = receipt_num_integer;
	}
	
	
}
