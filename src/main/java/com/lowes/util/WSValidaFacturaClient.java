package com.lowes.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.axis.AxisFault;
import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.buzonfiscal.www.ns.bf.conector._1.MessageType;
import com.buzonfiscal.www.ns.ws.CorpValidaCFDenLineaWS.BuzonFiscalCorpValidaCFDenLineaBindingStub;
import com.buzonfiscal.www.ns.xsd.bf.RequestEnviaCFD.DocumentoType;
import com.buzonfiscal.www.ns.xsd.bf.RequestEnviaCFD.DocumentoTypeTipo;
import com.buzonfiscal.www.ns.xsd.bf.RequestEnviaCFD.InfoBasicaType;
import com.buzonfiscal.www.ns.xsd.bf.bfenviacfd._1.RequestEnviaCfdType;

@Repository
public class WSValidaFacturaClient {

	private static final Logger log = Logger.getLogger(WSValidaFacturaClient.class);
	
	private BuzonFiscalCorpValidaCFDenLineaBindingStub stub;
	
	private static final DocumentoTypeTipo DOCUMENTO_TYPE_TIPO = DocumentoTypeTipo.XML; //Valor fijo XML
	private static final String SERVICE_VERSION = "3.2";
	
	public WSValidaFacturaClient() {
		
	}
	
	public List<WSFacturaMessageResponse> validarFactura (byte [] archivo, String nombreArchivo, String rfcEmisor, String rfcReceptor, String importeTotal, String endpoint, String token) {
		List<WSFacturaMessageResponse> messages = new ArrayList<WSFacturaMessageResponse>();
		
		try {
			validarParametros(archivo, nombreArchivo, rfcEmisor, rfcReceptor, importeTotal);
		} catch (IllegalStateException e) {
			messages.add(new WSFacturaMessageResponse(WSMessageType.PLATFORM_ERROR, -100, e.getMessage()));
			log.error(e.getMessage());
			return messages;
		}
		
		try {
			validarEndpoint(endpoint);
		} catch (IllegalStateException e) {
			messages.add(new WSFacturaMessageResponse(WSMessageType.PLATFORM_ERROR, -101, e.getMessage()));
			log.error(e.getMessage());
			return messages;
		}
		
		try {
			stub = getStub(endpoint);
		} catch (IllegalStateException e) {
			messages.add(new WSFacturaMessageResponse(WSMessageType.PLATFORM_ERROR, -102, e.getMessage()));
			log.error(e.getMessage());
			return messages;
		}
		
		StringBuilder parameters = new StringBuilder();
		parameters.append("\n").append("archivo (lenght)=")	.append(archivo.length);
		parameters.append("\n").append("nombreArchivo	=")	.append(nombreArchivo);
		parameters.append("\n").append("rfcEmisor		=")	.append(rfcEmisor);
		parameters.append("\n").append("rfcReceptor		=")	.append(rfcReceptor);
		parameters.append("\n").append("importeTotal	=")	.append(importeTotal);
		parameters.append("\n").append("token			=")	.append(token);
		parameters.append("\n").append("endpoint		=")	.append(endpoint);
		log.info(parameters);

		byte [] base64Encode = archivo;//convertirArchivoBase64(archivo);
		
		DocumentoType documento = getDocumento(base64Encode, nombreArchivo, DOCUMENTO_TYPE_TIPO, SERVICE_VERSION);
		
		InfoBasicaType infoBasica = getInfoBasicaType(rfcEmisor, rfcReceptor, importeTotal);
		
		RequestEnviaCfdType request = getRequest(documento, infoBasica, token);
		
		try {
			com.buzonfiscal.www.ns.bf.conector._1.MessageType[] response = stub.validaCFD(request);
			
			if (response != null && response.length > 0) {
				
				for (MessageType messageType : response) {
					messages.add(new WSFacturaMessageResponse(WSMessageType.WS_CONNECTION, messageType.getCode(), messageType.getMessage()));
				}
			} else {
				messages.add(new WSFacturaMessageResponse(WSMessageType.WS_CONNECTION, -103, "Respuesta vacia de Web Service"));
			}
		} catch (RemoteException e) {
			messages.add(new WSFacturaMessageResponse(WSMessageType.WS_CONNECTION, -104, e.getMessage()));
			log.error("Error en llamado a Web Service", e);
			return messages;
		}
		
		return messages;
	}

	private BuzonFiscalCorpValidaCFDenLineaBindingStub getStub(String endpoint) {
		try {
			stub = new BuzonFiscalCorpValidaCFDenLineaBindingStub( new URL(endpoint), null);
		} catch (AxisFault e) {
			log.error("Error en la creacion de Servicio Web", e);
		} catch (MalformedURLException e) {
			log.error("Error en la creacion de Servicio Web", e);
		}
		return stub;
	}
	
	private void validarEndpoint(String endpoint) throws IllegalStateException {
		if (endpoint == null || endpoint.isEmpty()) {
			throw new IllegalStateException("Endpoint no configurado");
		}
	}

	private RequestEnviaCfdType getRequest(DocumentoType documento,
			InfoBasicaType infoBasica, String token) {
		RequestEnviaCfdType request = new RequestEnviaCfdType();
		request.setDocumento(documento);
		request.setInfoBasica(infoBasica);
		request.setToken(token);
		return request;
	}

	private InfoBasicaType getInfoBasicaType(String rfcEmisor,
			String rfcReceptor, String importeTotal) {
		String serie = null;
		String folio = null;
		BigDecimal importe = new BigDecimal(importeTotal);
		String documentoId = null;
		String claveArea = null;
		String unidadNegocio = null; 
		
		return new InfoBasicaType(rfcEmisor, rfcReceptor, serie, folio, importe, documentoId, claveArea, unidadNegocio);
	}

	private DocumentoType getDocumento(byte[] archivo,
			String nombreArchivo, DocumentoTypeTipo tipo,
			String version) {
		return new DocumentoType(archivo, nombreArchivo, tipo, version); 
	}

	private byte[] convertirArchivoBase64(byte[] archivo) {
		//Archivo en Base64
		return Base64.encodeBase64(archivo); 
	}

	private void validarParametros(byte[] archivo, String nombreArchivo, String rfcEmisor, String rfcReceptor, String importeTotal) throws IllegalStateException {
		if (archivo == null 
				|| (nombreArchivo == null || nombreArchivo.isEmpty()) 
				|| (rfcEmisor == null || rfcEmisor.isEmpty()) 
				|| (rfcReceptor == null || rfcReceptor.isEmpty())
				|| (importeTotal == null || importeTotal.isEmpty())) {
			throw new IllegalStateException("Parametros obligatorios");
		}
	}
	

	//::::::::::::::::::::::::::::::::: metodos para conectar con konesh y enviar y recibir respuesta::::::::::::::::::::::::::::::.
	public HashMap<String,String> validaFacturaKonesh(String archivo, String CuentaTimbrado, String TokenTimbrado, String UsuarioTimbrado, String PasswordTimbrado, String endpoint, String rfcReceptor) {
	  HashMap<String,String> result=null;
      SOAPConnection soapConnection = null;
	  try { 
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				
	             public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                 return null;
	             }
	             public void checkClientTrusted(X509Certificate[] certs, String authType) {
	             }
	             public void checkServerTrusted(X509Certificate[] certs, String authType) {
	             }
	         }
	     };

	     // Install the all-trusting trust manager
	     SSLContext sc = SSLContext.getInstance("SSL");
	    sc.init(null, trustAllCerts, new java.security.SecureRandom());
	    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	     HostnameVerifier allHostsValid = new HostnameVerifier() {
	        public boolean verify(String hostname, SSLSession session) {
	             return true;
	         }
	     };
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	  
	String contenido="<![CDATA["+archivo+"]]>";
	
	 
	  try {
          SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
          soapConnection = soapConnectionFactory.createConnection();
          
          boolean validatos = validaDatosKonesh(CuentaTimbrado, TokenTimbrado, UsuarioTimbrado, PasswordTimbrado, endpoint);
          if(validatos) {
          	SOAPMessage response = soapConnection.call(publishWS(contenido, CuentaTimbrado,TokenTimbrado,UsuarioTimbrado,PasswordTimbrado,rfcReceptor), endpoint);
              //Print Response
              ByteArrayOutputStream out = new ByteArrayOutputStream();
              response.writeTo(out);
             //Process Result
             result = processResponse(out.toString());
          }
          else {
            result=new HashMap<String,String>();
            result.put("007", "Fallo un dato en el metodo validaDatosKenesh()");
            return result;
          }  
      } catch (Exception e) {
      	System.out.println(e.getMessage());
    
      }
	  return result;
   }
  
	public static boolean validaDatosKonesh(String CuentaTimbrado,String TokenTimbrado,String UsuarioTimbrado,String PasswordTimbrado, String endpoint) {
		boolean bandera=true;
		
		if(CuentaTimbrado==null || CuentaTimbrado.equals("") || CuentaTimbrado.isEmpty()) {
			return false;
		}else if(TokenTimbrado==null || TokenTimbrado.equals("")|| TokenTimbrado.isEmpty()){
			return false;
		}else if(UsuarioTimbrado==null || UsuarioTimbrado.isEmpty() || UsuarioTimbrado.equals("")) {
			return false;
		}else if(PasswordTimbrado==null || PasswordTimbrado.isEmpty() || PasswordTimbrado.equals("")) {
			return false;
		}else if(endpoint==null || endpoint.isEmpty() || endpoint.equals("")) {
			return false;
		}
		return bandera;
	}
	
	private static HashMap<String,String> processResponse(String respuesta) {
		 String xmlresponse =respuesta;
		 LinkedHashMap<String,String> atributos=new LinkedHashMap<String,String>();
		 String estatus="";
		 String codigo="";
		 String descripcion="";
		 Boolean estatus_info=false;
		 String DescripcionError=null;
		try {
	        
			String info=xmlresponse.replace("&lt;", "<");
		    //System.out.println("Replace-->"+info);
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         InputSource is = new InputSource(new StringReader(info));
	         Document doc = dBuilder.parse(is);
	         doc.getDocumentElement().normalize();
	        // System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("UUID");
	         //System.out.println("----------------------------");
	         
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            //System.out.println("\nCurrent Element :" + nNode.getNodeName());
	            
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	             //  System.out.println("folio : " 
	                //  + eElement.getAttribute("folio"));
	              // System.out.println("status : " 
	 	              //    + eElement.getAttribute("status"));
	               ///resultado=eElement.getAttribute("folio")+eElement.getAttribute("status");
	              // resultado.put(eElement.getAttribute("folio"), eElement.getAttribute("status"));
	               estatus=eElement.getAttribute("status");
	        
	            }
	         }
	        estatus_info = Boolean.valueOf(estatus);
	         if(estatus_info) {
	        	 atributos.put("0", "Proceso realizado con éxito, CFDi válido");
	         }else {
	        	 NodeList nListDescripcion = doc.getElementsByTagName("ERROR");
		         for (int i = 0; i < nListDescripcion.getLength(); i++) {
		        	 Node nNodes = nListDescripcion.item(i);
			      //      System.out.println("\nCurrent Element :" + nNodes.getNodeName());
			            
			            if (nNodes.getNodeType() == Node.ELEMENT_NODE) {
			               Element eElement = (Element) nNodes;
			              /// System.out.println("codigo : " 
			              //    + eElement.getAttribute("codigo"));
			              // System.out.println("mensaje : " 
			 	           //       + eElement.getAttribute("mensaje")); 
			               
			           // resultado= eElement.getAttribute("codigo")+"||"+eElement.getAttribute("mensaje");
			            //resultado.put(eElement.getAttribute("codigo"), eElement.getAttribute("mensaje"));
			            codigo=eElement.getAttribute("codigo");
			            descripcion=eElement.getAttribute("mensaje");
			            
			            DescripcionError=Errores(codigo.trim());
			            atributos.put(codigo,DescripcionError);
			            
			            }
					
				}
		         
		        // atributos.put(folio, estatus); 
		        
	         }
	         
	         
	        
	         
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
		
		return  atributos;
		
	}
	
	private static SOAPMessage publishWS(String contenido, String cuentaTimbrado, String tokenTimbrado,
			String usuarioTimbrado, String passwordTimbrado,String rfcReceptor) throws SOAPException, IOException {
		MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage request = messageFactory.createMessage();
        
        SOAPPart soaPart = request.getSOAPPart();
        SOAPEnvelope envelope = soaPart.getEnvelope();
        //Agregando el namespace
        envelope.addNamespaceDeclaration("val", "http://validadorws.cfdi.mx.konesh.com");
        //creando el body
        SOAPBody bodyConsultaDocumento = envelope.getBody();
        //Declarando el metodo a usar
        SOAPElement lowConsulta = bodyConsultaDocumento.addChildElement("val:validarDocumentos");
     
        SOAPElement ContenidoInfo =lowConsulta.addChildElement("cad");//IdCad
        ContenidoInfo.addTextNode(contenido);
        
        SOAPElement TokenTimb = lowConsulta.addChildElement("token");
        TokenTimb.addTextNode(tokenTimbrado);
        
        SOAPElement pass = lowConsulta.addChildElement("password");
        pass.addTextNode(passwordTimbrado);
        
        SOAPElement User = lowConsulta.addChildElement("user");
        User.addTextNode(usuarioTimbrado);
        
        SOAPElement CuentaTimb = lowConsulta.addChildElement("cuenta");
        CuentaTimb.addTextNode(cuentaTimbrado);
        
        SOAPElement rfcReceptorTimb = lowConsulta.addChildElement("rfcReceptor");
        rfcReceptorTimb.addTextNode(rfcReceptor);
        
        request.saveChanges();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        request.writeTo(out);
		return request;
	}
	
	 public static String convertToUTF8(String s) {
	        String out = null;
	        try {
	            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
	        } catch (java.io.UnsupportedEncodingException e) {
	            return null;
	        }
	        return out;
	    }
	 
	 public static String Errores(String codigo) {
		 String descripcionError="";
		 if(codigo.equals("302"))
		 {
		   descripcionError="Sello mal formado o invalido";
		 }else if(codigo.equals("301")) {
			 descripcionError="Errores de Estructura XML"; 
		 }else if(codigo.equals("303")) {
			 descripcionError="Sello no corresponde a emisor"; 
		 }
		 else if(codigo.equals("304")) {
			 descripcionError="Certificado revocado o caduco"; 
		 }
		 else if(codigo.equals("305")) {
			 descripcionError="La fecha de emisión no está dentro\n" + 
			 		"de la vigencia del CSD del emisor"; 
		 }
		 else if(codigo.equals("306")) {
			 descripcionError="El certificado no es de tipo CSD"; 
		 }
		 else if(codigo.equals("308")) {
			 descripcionError="Certificado no expedido por el SAT"; 
		 }
		 else if(codigo.equals("456")) {
			 descripcionError="El documento fué recibido\n" + 
			 		"anteriormente."; 
		 }
		 else if(codigo.equals("458")) {
			 descripcionError="El RFC receptor no coincide con\n" + 
			 		"ninguno del sistema."; 
		 }
		 else if(codigo.equals("512")) {
			 descripcionError="El certificado no cumple con las\n" + 
			 		"obligaciones en la lista del SAT."; 
		 }
		 else if(codigo.equals("513")) {
			 descripcionError="El certificado no fué emitido para la\n" + 
			 		"empresa emisora."; 
		 }
		 else if(codigo.equals("516")) {
			 descripcionError="El atributo \"noCertificado\" no\n" + 
			 		"coincide con el Número de Serie del\n" + 
			 		"Certificado"; 
		 }
		 else if(codigo.equals("517")) {
			 descripcionError="La cadena del certificado incluido en\n" + 
			 		"el cfdi no es correcta"; 
		 }
		 else if(codigo.equals("518")) {
			 descripcionError="El certificado incluido en el xml es\n" + 
			 		"invalido."; 
		 }
		 else if(codigo.equals("901")) {
			 descripcionError="NO SE HA PODIDO AUTENTICAR LA\n" + 
			 		"TRANSACCION."; 
		 }
		 else if(codigo.equals("902")) {
			 descripcionError="Declaración de namespace default\n" + 
			 		"no permitida."; 
		 }
		 else if(codigo.equals("903")) {
			 descripcionError="Declaración de namespace xsi\n" + 
			 		"incorrecta"; 
		 }
		 else if(codigo.equals("904")) {
			 descripcionError="Declaración de namespace cfdi\n" + 
			 		"incorrecta"; 
		 }
		 else if(codigo.equals("905")) {
			 descripcionError="Se ha declarado el namespace cfdi\n" + 
			 		"pero no se declaro la localización del\n" + 
			 		"esquema."; 
		 }
		 else if(codigo.equals("906")) {
			 descripcionError="Declaración de namespace ecc\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("907")) {
			 descripcionError="Se ha declarado el namespace ecc\n" + 
			 		"pero no se declaro la localización del\n" + 
			 		"esquema."; 
		 }
		 else if(codigo.equals("908")) {
			 descripcionError="Declaración de namespace detallista\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("909")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"detallista pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("910")) {
			 descripcionError="Declaración de namespace divisas\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("911")) {
			 descripcionError="Se ha declarado el namespace divisas\n" + 
			 		"pero no se declaro la localización del\n" + 
			 		"esquema."; 
		 }
		 else if(codigo.equals("912")) {
			 descripcionError="Declaración de namespace implocal\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("913")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"implocal pero no se declaro la\n" + 
			 		"localización del esquema"; 
		 }
		 else if(codigo.equals("914")) {
			 descripcionError="Declaración de namespace psgecfd\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("915")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"psgecfd pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("918")) {
			 descripcionError="Declaración de namespace terceros\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("919")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"terceros pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("920")) {
			 descripcionError="Declaración de namespace donat\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("921")) {
			 descripcionError="Se ha declarado el namespace donat\n" + 
			 		"pero no se declaro la localización del\n" + 
			 		"esquema."; 
		 }
		 else if(codigo.equals("922")) {
			 descripcionError="Declaración de namespace tpe\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("923")) {
			 descripcionError="Se ha declarado el namespace tpe\n" + 
			 		"pero no se declaro la localización del\n" + 
			 		"esquema."; 
		 }
		 else if(codigo.equals("924")) {
			 descripcionError="Declaración de namespace pfic\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("925")) {
			 descripcionError="Se ha declarado el namespace pfic\n" + 
			 		"pero no se declaro la localización del\n" + 
			 		"esquema."; 
		 }
		 else if(codigo.equals("926")) {
			 descripcionError="Declaración de namespace\n" + 
			 		"ventavehiculos incorrecta"; 
		 }
		 else if(codigo.equals("927")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"ventavehiculos pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("928")) {
			 descripcionError="Declaración de namespace leyendas\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("929")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"leyendas pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("930")) {
			 descripcionError="Declaración de namespace psgcfdsp\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("931")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"psgcfdsp pero no se declaro la\n" + 
			 		"localización del esquema"; 
		 }
		 else if(codigo.equals("932")) {
			 descripcionError="Declaración de namespace nomina\n" + 
			 		"incorrecta."; 
		 }
		 else if(codigo.equals("933")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"nomina pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("934")) {
			 descripcionError="Declaración de namespace pago en\n" + 
			 		"especie incorrecta."; 
		 }
		 else if(codigo.equals("935")) {
			 descripcionError="Se ha declarado el namespace pago\n" + 
			 		"en especie pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("936")) {
			 descripcionError="Declaración de namespace consumo\n" + 
			 		"de combustible incorrecta."; 
		 }
		 else if(codigo.equals("937")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"consumo de combustible pero no se\n" + 
			 		"declaro la localización del esquema."; 
		 }
		 else if(codigo.equals("938")) {
			 descripcionError="Declaración de namespace vales de\n" + 
			 		"despensa incorrecta"; 
		 }
		 else if(codigo.equals("939")) {
			 descripcionError="Se ha declarado el namespace vales\n" + 
			 		"de despensa pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("940")) {
			 descripcionError="Declaración de namespace\n" + 
			 		"aerolineas incorrecta."; 
		 }
		 else if(codigo.equals("941")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"aerolineas pero no se declaro la\n" + 
			 		"localización del esquema."; 
		 }
		 else if(codigo.equals("942")) {
			 descripcionError="Declaración de namespace notarios\n" + 
			 		"públicos incorrecta."; 
		 }
		 else if(codigo.equals("943")) {
			 descripcionError="Se ha declarado el namespace\n" + 
			 		"notarios públicos pero no se declaro\n" + 
			 		"la localización del esquema"; 
		 }
		 else if(codigo.equals("1000")) {
			 descripcionError="El parametro ip.socket.validador no\n" + 
			 		"está correctamente configurado."; 
		 }
		 else if(codigo.equals("-1")) {
			 descripcionError="Documento no validable (¿no es un\n" + 
			 		"cfdi?)"; 
		 }
		 else if(codigo.equals("3000")) {
			 descripcionError="El documento recibido no es un xml\n" + 
			 		"válido"; 
		 }
		 else if(codigo.equals("4001")) {
			 descripcionError="Faltan datos para autenticar la\n" + 
			 		"transaccion."; 
		 }
		 else if(codigo.equals("4002")) {
			 descripcionError="El usuario o password son\n" + 
			 		"incorrectos."; 
		 }
		 else if(codigo.equals("4004")) {
			 descripcionError="Hubo un problema al procesar la\n" + 
			 		"petición, motivo:<Exception>"; 
		 }
		 else if(codigo.equals("1210")) {
			 descripcionError=" El contribuyente aparece en la lista negra del artículo 69 (NO LOCALIZADOS)"; 
		 }
		 else {
			 descripcionError="Descripcion no encontrada" ;
		 }
		 
		
		 
		 return descripcionError;
	 }
	 
	
	
}
