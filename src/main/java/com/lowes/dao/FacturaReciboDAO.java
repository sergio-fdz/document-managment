package com.lowes.dao;

import java.util.List;

import com.lowes.entity.FacturaRecibo;


public interface FacturaReciboDAO {
	
	public Integer createFacturaRecibo(FacturaRecibo recibo);
    public FacturaRecibo updateFacturaRecibo(FacturaRecibo recibo);
    public void deleteFacturaRecibo(FacturaRecibo recibo);
    public List<FacturaRecibo> getAllFacturaRecibo();
    public FacturaRecibo getFacturaRecibo(Integer id);
    public List<FacturaRecibo> getAllFacturaRecibobyFactura(Integer idFacura);

}
