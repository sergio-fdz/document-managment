package com.lowes.dao;

import java.util.List;

import com.lowes.entity.Usuario;
import com.lowes.entity.XxloRequisitionPo;


public interface XxloRequisitionPoDAO {
	
	public Integer createXxloRequisitionPo(XxloRequisitionPo xxloRequisitionPo);
    public XxloRequisitionPo updateViajeConcepto(XxloRequisitionPo xxloRequisitionPo);
    public void deleteXxloRequisitionPo(Integer id);
    public List<XxloRequisitionPo> getAllXxloRequisitionPo();
    public XxloRequisitionPo getXxloRequisitionPo(Integer id);
    public List<XxloRequisitionPo> getOrdenesCompraRestricciones(Usuario empleado, Integer tipoFactura);
    public boolean tieneRecibosNoFacturados(List<XxloRequisitionPo> ordenesCompra);

}
