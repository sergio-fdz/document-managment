package com.lowes.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowes.dao.XxloDataSourcePoDAO;
import com.lowes.entity.Compania;
import com.lowes.entity.Solicitud;
import com.lowes.entity.Usuario;
import com.lowes.entity.XxloDataSourcePo;
import com.lowes.service.ParametroService;
import com.lowes.util.HibernateUtil;

@Repository
public class XxloDataSourcePoDAOImpl implements XxloDataSourcePoDAO {
	
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Autowired
	private ParametroService parametroService;

	@Override
	public Integer createXxloDataSourcePo(XxloDataSourcePo xxloDataSourcePo) {
		return (Integer) hibernateUtil.create(xxloDataSourcePo);
	}

	@Override
	public XxloDataSourcePo updateViajeConcepto(XxloDataSourcePo xxloDataSourcePo) {
		return hibernateUtil.update(xxloDataSourcePo);
	}

	@Override
	public void deleteXxloDataSourcePo(Integer id) {
		hibernateUtil.delete(new XxloDataSourcePo(id));
	}

	@Override
	public List<XxloDataSourcePo> getAllXxloDataSourcePo() {
		return hibernateUtil.fetchAll(XxloDataSourcePo.class);
	}

	@Override
	public XxloDataSourcePo getXxloDataSourcePo(Integer id) {
		return hibernateUtil.fetchById(id, XxloDataSourcePo.class);
	}

	@Override
	public List<XxloDataSourcePo> getOrdenesCompraRestricciones(Usuario empleado, Integer tipoFactura) {
		
		String queryString ="FROM " + XxloDataSourcePo.class.getName()
				+ " WHERE EMPLOYEE_NUM = :numero_empleado AND TRANSACTION_TYPE = :transaction_type";
		
		/*para tomar el tipo de factura*/
		String transaction_type = "";
		String idTipoFactura = parametroService.getParametroByName("idTipoFactura").getValor();
		if(tipoFactura == Integer.parseInt(idTipoFactura)){
			transaction_type = "RECEIVE";
		}else{
			transaction_type = "RETURN TO VENDOR";
		}
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("numero_empleado", String.valueOf(empleado.getNumeroEmpleado()));
		parameters.put("transaction_type",transaction_type);
		
		List<XxloDataSourcePo> ordenesCompra = hibernateUtil.fetchAllHqlObject(queryString, parameters);
		
		if(tieneRecibosNoFacturados(ordenesCompra)){
			return ordenesCompra;
		}else{
			List<XxloDataSourcePo> listaVacia = new ArrayList<>();
			return listaVacia;
		}
				
	}

	@Override
	public boolean tieneRecibosNoFacturados(List<XxloDataSourcePo> ordenesCompra) {
	   return true;
	}

	@Override
	public List<XxloDataSourcePo> getOrdenCompra(String idOrdenCompra, Integer tipoFactura, Integer check_association) {
		
		
		String queryString ="FROM " + XxloDataSourcePo.class.getName()
				+ " WHERE PO = :po AND TRANSACTION_TYPE = :transaction_type ";
			//	+ "AND CHECK_ASSOCIATION = :check_association";
		
		/*para tomar el tipo de factura*/
		String transaction_type = "";
		String idTipoFactura = parametroService.getParametroByName("idTipoFactura").getValor();
		if(tipoFactura == Integer.parseInt(idTipoFactura)){
			transaction_type = "RECEIVE";
		}else{
			transaction_type = "RETURN TO VENDOR";
		}
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("po", idOrdenCompra);
		parameters.put("transaction_type",transaction_type);
		//parameters.put("check_association",check_association);
		
		
		List<XxloDataSourcePo> ordenesCompra = hibernateUtil.fetchAllHqlObject(queryString, parameters);
		
		
		
		/*REVISAR QUE LOS RECIBOS DEPENDIENTES DE ESTA ORDEN DE COMPRA TENGAN POR LO MENOS UN NO USADO*/
		
		
		
		return ordenesCompra;
	}

	@Override
	public List<XxloDataSourcePo> getPOsByIDPO(String po) {
		
		String queryString = "FROM " + XxloDataSourcePo.class.getName()
				+ " WHERE PO = :po";
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("po", po);
		
		List<XxloDataSourcePo> ordenes = hibernateUtil.fetchAllHql(queryString, parameters);
		
		return ordenes;
		
	}

	@Override
	public List<XxloDataSourcePo> getSKUsByRecibos(String num_recibo, Integer tipoRecibo, String ordenCompra , Boolean ignorarCheckAssociation) {
		
		/*para tomar el tipo de factura*/
		String transaction_type = "";
		String idTipoFactura = parametroService.getParametroByName("idTipoFactura").getValor();
		if(tipoRecibo == Integer.parseInt(idTipoFactura)){
			transaction_type = "RECEIVE";
		}else{
			transaction_type = "RETURN TO VENDOR";
		}
		
		String queryString = "";
		
		if(ignorarCheckAssociation){
			
			 queryString = "FROM " + XxloDataSourcePo.class.getName()
					+ " WHERE PO = :ordenCompra AND TRANSACTION_TYPE = "
					+ ":transaction_type AND RECEIPT_NUM = :num_recibo";
			
		}else{
			
			 queryString = "FROM " + XxloDataSourcePo.class.getName()
					+ " WHERE PO = :ordenCompra AND TRANSACTION_TYPE = "
					+ ":transaction_type AND RECEIPT_NUM = :num_recibo AND CHECK_ASSOCIATION = 0 ";
			
		}

		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("num_recibo", num_recibo);
		parameters.put("transaction_type", transaction_type);
		parameters.put("ordenCompra", ordenCompra);
		
		List<XxloDataSourcePo> ordenes = hibernateUtil.fetchAllHql(queryString, parameters);
		
		return ordenes;
		
	}

	@Override
	public boolean updateRecibosEnFactura(String numero_recibo, Boolean reservado, Integer tipoFactura, String ordenCompra) {
		
		/*para tomar el tipo de factura*/
		String transaction_type = "";
		String idTipoFactura = parametroService.getParametroByName("idTipoFactura").getValor();
		if(tipoFactura == Integer.parseInt(idTipoFactura)){
			transaction_type = "RECEIVE";
		}else{
			transaction_type = "RETURN TO VENDOR";
		}
		
		String queryString = "UPDATE " +XxloDataSourcePo.class.getName() + " SET CHECK_ASSOCIATION = :estadoRecibo"
				+ "	WHERE RECEIPT_NUM = :numero_recibo AND TRANSACTION_TYPE = :transaction_type AND PO = :ordenCompra";
		
		Map<String, String> parameters = new HashMap<String, String>();
		
		if(reservado){
			parameters.put("estadoRecibo", "1");
		}else{
			parameters.put("estadoRecibo", "0");
		}
		
		parameters.put("numero_recibo", String.valueOf(numero_recibo));
		parameters.put("transaction_type", String.valueOf(transaction_type));
		parameters.put("ordenCompra", String.valueOf(ordenCompra));
		
		hibernateUtil.fetchDeleteAndUpdateQuerys(queryString, parameters);
		
		return true;
		
	}


	

}
