package com.lowes.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowes.dao.XxloRequisitionPoDAO;
import com.lowes.entity.Usuario;
import com.lowes.entity.XxloRequisitionPo;
import com.lowes.service.ParametroService;
import com.lowes.util.HibernateUtil;

@Repository
public class XxloRequisitionPoDAOImpl implements XxloRequisitionPoDAO
{
	
	@Autowired
	private HibernateUtil hibernateUtil;
	
	@Autowired
	private ParametroService parametroService;
	

	@Override
	public Integer createXxloRequisitionPo(XxloRequisitionPo xxloRequisitionPo) {
		return (Integer) hibernateUtil.create(xxloRequisitionPo);
	}

	@Override
	public XxloRequisitionPo updateViajeConcepto(XxloRequisitionPo xxloRequisitionPo) {
		return hibernateUtil.update(xxloRequisitionPo);
	}

	@Override
	public void deleteXxloRequisitionPo(Integer id) {
		hibernateUtil.delete(new XxloRequisitionPo(id));
	}

	@Override
	public List<XxloRequisitionPo> getAllXxloRequisitionPo() {
		return hibernateUtil.fetchAll(XxloRequisitionPo.class);
	}

	@Override
	public XxloRequisitionPo getXxloRequisitionPo(Integer id) {
		return hibernateUtil.fetchById(id, XxloRequisitionPo.class);
	}

	@Override
	public List<XxloRequisitionPo> getOrdenesCompraRestricciones(Usuario empleado, Integer tipoFactura) {
		
		String queryString ="FROM " + XxloRequisitionPo.class.getName()
				+ " WHERE NRO_REQUESTOR = :numero_empleado";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("numero_empleado", empleado.getNumeroEmpleado());
		
		List<XxloRequisitionPo> ordenesCompra = hibernateUtil.fetchAllHqlObject(queryString, parameters);
		
		if(tieneRecibosNoFacturados(ordenesCompra)){
			return ordenesCompra;
		}else{
			List<XxloRequisitionPo> listaVacia = new ArrayList<>();
			return listaVacia;
		}
		
	}

	@Override
	public boolean tieneRecibosNoFacturados(List<XxloRequisitionPo> ordenesCompra) {
		return true;
	}
	
}
