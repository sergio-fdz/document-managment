package com.lowes.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowes.dao.AidConfiguracionDAO;
import com.lowes.entity.AidConfiguracion;
import com.lowes.entity.Locacion;
import com.lowes.util.HibernateUtil;

@Repository
public class AidConfiguracionDAOImpl implements AidConfiguracionDAO{
	
	public AidConfiguracionDAOImpl(){
		System.out.println("AidConfiguracionDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;

	@Override
	public Integer createAidConfiguracion(AidConfiguracion aidConfiguracion) {
		return (Integer) hibernateUtil.create(aidConfiguracion);
	}

	@Override
	public AidConfiguracion updateAidConfiguracion(AidConfiguracion aidConfiguracion) {
		return hibernateUtil.update(aidConfiguracion);
	}

	@Override
	public void deleteAidConfiguracion(Integer id) {
		AidConfiguracion aidConfiguracion = getAidConfiguracion(id); // Obtener compa�ia por id y enviar el objeto para eliminar
		hibernateUtil.delete(aidConfiguracion);
	}

	@Override
	public List<AidConfiguracion> getAllAidConfiguracion() {
		return hibernateUtil.fetchAll(AidConfiguracion.class);
	}

	@Override
	public AidConfiguracion getAidConfiguracion(Integer id) {
		return hibernateUtil.fetchById(id, AidConfiguracion.class);
	}

	@Override
	public List<AidConfiguracion> getAidConfigurationByAID(Integer aid) {
		
		String queryString = "FROM " + AidConfiguracion.class.getName()
				+ " WHERE ID_AID = :aid";
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("aid", aid.toString());
		
		List<AidConfiguracion> config = hibernateUtil.fetchAllHql(queryString, parameters);
		
		return config;
		
	}
	
	
	

}