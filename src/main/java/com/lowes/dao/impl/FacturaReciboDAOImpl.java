package com.lowes.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowes.dao.FacturaReciboDAO;
import com.lowes.entity.FacturaRecibo;
import com.lowes.entity.XxloDataSourcePo;
import com.lowes.util.HibernateUtil;

@Repository
public class FacturaReciboDAOImpl implements FacturaReciboDAO {
	
	@Autowired
    private HibernateUtil hibernateUtil;

	@Override
	public Integer createFacturaRecibo(FacturaRecibo recibo) {
		return (Integer) hibernateUtil.create(recibo);
	}

	@Override
	public FacturaRecibo updateFacturaRecibo(FacturaRecibo recibo) {
		return hibernateUtil.update(recibo);
	}

	@Override
	public void deleteFacturaRecibo(FacturaRecibo recibo) {
		hibernateUtil.delete(recibo);
	}

	@Override
	public List<FacturaRecibo> getAllFacturaRecibo() {
		return hibernateUtil.fetchAll(FacturaRecibo.class);
	}

	@Override
	public FacturaRecibo getFacturaRecibo(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FacturaRecibo> getAllFacturaRecibobyFactura(Integer idFactura) {
		
		String queryString = "FROM " + FacturaRecibo.class.getName()
				+ " WHERE ID_FACTURA = :idFactura";
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("idFactura", idFactura.toString());
		
		List<FacturaRecibo> recibos = hibernateUtil.fetchAllHql(queryString, parameters);
		
		return recibos;
		
	}

}
