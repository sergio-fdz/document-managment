package com.lowes.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.StringEncoder;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.lowes.dto.FacturaConXML;
import com.lowes.dto.FacturaDesgloseDTO;
import com.lowes.dto.OrdenCompraDTO;
import com.lowes.dto.RecibosDTO;
import com.lowes.entity.Aid;
import com.lowes.entity.AidConfiguracion;
import com.lowes.entity.CategoriaMayor;
import com.lowes.entity.CategoriaMenor;
import com.lowes.entity.Compania;
import com.lowes.entity.CuentaContable;
import com.lowes.entity.EstadoSolicitud;
import com.lowes.entity.Factura;
import com.lowes.entity.FacturaArchivo;
import com.lowes.entity.FacturaDesglose;
import com.lowes.entity.FacturaRecibo;
import com.lowes.entity.FormaPago;
import com.lowes.entity.Locacion;
import com.lowes.entity.Moneda;
import com.lowes.entity.Proveedor;
import com.lowes.entity.Solicitud;
import com.lowes.entity.SolicitudArchivo;
import com.lowes.entity.SolicitudAutorizacion;
import com.lowes.entity.TipoDocumento;
import com.lowes.entity.TipoFactura;
import com.lowes.entity.TipoSolicitud;
import com.lowes.entity.Usuario;
import com.lowes.entity.UsuarioConfSolicitante;
import com.lowes.entity.XxloDataSourcePo;
import com.lowes.entity.XxloRequisitionPo;
import com.lowes.service.AidConfiguracionService;
import com.lowes.service.AidService;
import com.lowes.service.CategoriaMayorService;
import com.lowes.service.CategoriaMenorService;
import com.lowes.service.CompaniaService;
import com.lowes.service.CuentaContableService;
import com.lowes.service.FacturaArchivoService;
import com.lowes.service.FacturaDesgloseService;
import com.lowes.service.FacturaReciboService;
import com.lowes.service.FacturaService;
import com.lowes.service.LocacionService;
import com.lowes.service.MonedaService;
import com.lowes.service.ParametroService;
import com.lowes.service.ProveedorService;
import com.lowes.service.SolicitudArchivoService;
import com.lowes.service.SolicitudAutorizacionService;
import com.lowes.service.SolicitudService;
import com.lowes.service.TipoDocumentoService;
import com.lowes.service.UsuarioConfSolicitanteService;
import com.lowes.service.UsuarioService;
import com.lowes.service.XxloDataSourcePoService;
import com.lowes.service.XxloRequisitionPoService;
import com.lowes.util.Etiquetas;
import com.lowes.util.Utilerias;

import antlr.StringUtils;

import java.net.InetAddress;

@Scope("session")
@Controller
@PreAuthorize("hasRole('ROLE_USER')")
public class NoMercanciasController {
	
	@Value("${initBinder.autoGrowCollectionLimit}")
	private int autoGrowCollectionLimit;
	
	@Autowired
	private LocacionService locacionService;
	@Autowired
	private CuentaContableService cuentaContableService;
	@Autowired
	private AidService aidService;
	@Autowired
	private CategoriaMayorService categoriaMayor;
	@Autowired
	private CategoriaMenorService categoriaMenor;
	@Autowired
	private UsuarioConfSolicitanteService uConfsolicitanteService;
	@Autowired
	private CompaniaService companiaService;
	@Autowired
	private ProveedorService proveedorService;
	@Autowired
	private MonedaService monedaService;
	@Autowired
	private SolicitudService solicitudService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private FacturaService facturaService;
	@Autowired
	private FacturaDesgloseService facturaDesgloseService;
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	@Autowired
	private FacturaArchivoService facturaArchivoService;
	@Autowired
	private SolicitudArchivoService solicitudArchivoService;
	@Autowired
	private ParametroService parametroService;
	@Autowired
	private AidConfiguracionService aidConfiguracionService;
	@Autowired
	private SolicitudAutorizacionService solicitudAutorizacionService;
	@Autowired
	private XxloDataSourcePoService xxloDataSourcePoService;
	@Autowired
	private XxloRequisitionPoService xxloRequisitionPoService;
	@Autowired
	private FacturaReciboService facturaReciboService;

	private static final Logger logger = Logger.getLogger(NoMercanciasController.class);
	Etiquetas etiqueta = new Etiquetas("es");
	private Integer idUsuario = null;
	private Integer solicitanteSeleccion = null;
	public Integer idLibroSesion = 0;
	public List<FacturaRecibo> recibosSession = new ArrayList<>();
	public List<FacturaRecibo> recibosSessionPrimeraVez = new ArrayList<>();
	/*<begin jgh > */
	public List<FacturaDesgloseDTO> facturaDesglose=new ArrayList<>();
	/*</end jgh>*/
	
	@InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.setAutoGrowCollectionLimit(autoGrowCollectionLimit);
    }

	@RequestMapping(value = "/conXML", method = RequestMethod.GET)
	public ModelAndView inicioConXML(HttpSession session, Integer id) {
		this.idUsuario = usuarioService.getUsuarioSesion().getIdUsuario();
		Integer usuarioSolicitud = this.idUsuario;
		this.idLibroSesion = 0;
		List<XxloDataSourcePo> ordenesCompra = new ArrayList<>();
		recibosSession = new ArrayList<>();
		recibosSessionPrimeraVez = new ArrayList<>();

		HashMap<String, Object> model = new HashMap<String, Object>();
		FacturaConXML facturaConXML = new FacturaConXML();
		Integer idEstadoSolicitud = 0;

		if (id == null) {
			// destruir la sesion, creada por parametro
			session.setAttribute("solicitud", null);
		}

		// configuraciones permitidas por usuario
		List<UsuarioConfSolicitante> uconfigSol = new ArrayList<>();
		if (id != null && id > Etiquetas.CERO) {
			Solicitud solicitudSolicitante = solicitudService.getSolicitud(id);
			if (solicitudSolicitante != null) {
				uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(
						solicitudSolicitante.getUsuarioByIdUsuarioSolicita().getIdUsuario());
				usuarioSolicitud = solicitudSolicitante.getUsuarioByIdUsuarioSolicita().getIdUsuario();
			} else {
				uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
			}
		} else {
			uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
		}

		// Obtener combos filtrador por configuracion de solicitante.
		List<Locacion> lcPermitidas = UtilController.getLocacionesPermitidasPorUsuario(uconfigSol,
				Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()),
				locacionService.getAllLocaciones(), usuarioSolicitud);
		List<CuentaContable> ccontable = cuentaContableService.getAllCuentaContable();
		List<CuentaContable> ccPermitidas = UtilController.getCuentasContablesPermitidasPorUsuario(uconfigSol,
				ccontable);

		// Obtener Companias, AID, Categoria Mayor, Categoria Menor, Proveedores y Monedas
		List<Compania> lstCompanias = companiaService.getAllCompania();
		List<Aid> listAids = aidService.getAllAid();
		List<CategoriaMayor> listCatMayor = categoriaMayor.getAllCategoriaMayor();
		List<CategoriaMenor> listCatMenor = categoriaMenor.getAllCategoriaMenor();
		List<Proveedor> lstProveedores = proveedorService.getAllProveedoresActivos();
		List<Moneda> lstMoneda = monedaService.getAllMoneda();

		// obtener los jefe para opcion de usuario solicitante.
		List<Usuario> lstUsuariosJefe = new ArrayList<>();
		Usuario usuario = usuarioService.getUsuario(idUsuario);
		if (usuario.getUsuario() != null) {
			lstUsuariosJefe.add(usuario.getUsuario());
		}

		Solicitud solicitud = null;
		Factura factura = null;
		List<FacturaDesglose> facturasDesglose = new ArrayList<>();
		List<FacturaDesgloseDTO> facturasDesgloseDTO = new ArrayList<>();

		// Obtener solicitud,factura y su desglose si se requiere:
		if (id != null && id > Etiquetas.CERO) {
			facturaConXML.setIdSolicitudSession(id);
			solicitud = solicitudService.getSolicitud(facturaConXML.getIdSolicitudSession());
			if (solicitud != null) {
				idEstadoSolicitud = solicitud.getEstadoSolicitud().getIdEstadoSolicitud();

				// validacion de propietario de la solicitud
				// se manda la lista de autorizadores, if null : no aplica este
				// criterio
				// List<SolicitudAutorizacion> autorizadores =
				// solicitudAutorizacionService.getAllAutorizadoresByCriterio(solicitud.getIdSolicitud(),
				// Etiquetas.TIPO_CRITERIO_CUENTA_CONTABLE);
				List<SolicitudAutorizacion> autorizadores = solicitudAutorizacionService
						.getAllSolicitudAutorizacionBySolicitud(solicitud.getIdSolicitud());
				// resolver el acceso a la solicitud actual.
				Integer puestoAP = Integer
						.parseInt(parametroService.getParametroByName("puestoAutorizacionAP").getValor());
				Integer puestoConfirmacionAP = Integer
						.parseInt(parametroService.getParametroByName("puestoConfirmacionAP").getValor());
				Integer acceso = Utilerias.validaAcceso(solicitud,
						Integer.parseInt(
								parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()),
						usuarioService.getUsuarioSesion(), autorizadores, puestoAP, puestoConfirmacionAP);
				if (acceso == Etiquetas.NO_VISUALIZAR) {
					return new ModelAndView("redirect:/");
				} else if (acceso == Etiquetas.VISUALIZAR) {
					idEstadoSolicitud = Etiquetas.SOLO_VISUALIZACION;
				}
			}
		}

		if (solicitud != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

			// si la solicitud es v๏ฟฝlida entonces crear una variable de
			// sesi๏ฟฝn
			// para el update.
			session.setAttribute("solicitud", solicitud);

			factura = solicitud.getFacturas().get(Etiquetas.CERO);
			if (factura != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

				if (factura.getCompaniaByIdCompania() != null)
					this.idLibroSesion = factura.getCompaniaByIdCompania().getIdcompania();
					facturaConXML.setCompania(factura.getCompaniaByIdCompania());
				if (factura.getProveedor() != null) {
					facturaConXML.setProveedor(factura.getProveedor());
					facturaConXML.setRfcEmisor(factura.getProveedor().getRfc());
				}
				if (factura.getFolioFiscal() != null)
					facturaConXML.setFolioFiscal(factura.getFolioFiscal());
				facturaConXML.setFolio(factura.getFactura()); //
				if (factura.getSerieFactura() != null)
					facturaConXML.setSerie(factura.getSerieFactura());
				if (factura.getTipoFactura() != null)
					facturaConXML.setTipoFactura(factura.getTipoFactura().getIdTipoFactura());

				// retenciones
				if (factura.getConRetenciones() > Etiquetas.CERO_S) {
					facturaConXML.setConRetenciones(Etiquetas.TRUE);
					facturaConXML.setStrIsr_retenido(
							factura.getIsrRetenido() != null ? factura.getIsrRetenido().toString() : "");
					facturaConXML.setStrIva_retenido(
							factura.getIvaRetenido() != null ? factura.getIvaRetenido().toString() : "");
				} else {
					facturaConXML.setConRetenciones(Etiquetas.FALSE);
				}

				String fechaString = null;
				SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdfOut = new SimpleDateFormat("dd/MM/yyyy");

				try {
					fechaString = sdfOut.format(sdfIn.parse(factura.getFechaFactura().toString()));
					facturaConXML.setFecha_factura(fechaString);
				} catch (ParseException e) {

				}

				// el concepto se des-escapa para mandar a vista los caracteres
				// especiales correctamente.
				facturaConXML.setConcepto(StringEscapeUtils.unescapeJava(solicitud.getConceptoGasto()));

				/*if (factura.getTrackAsset() > Etiquetas.CERO) {
					facturaConXML.setTrack_asset(Etiquetas.TRUE);
				} else {
					facturaConXML.setTrack_asset(Etiquetas.FALSE);
				}

				facturaConXML.setPar(factura.getPar());
				facturaConXML.setId_compania_libro_contable(factura.getCompaniaByIdCompaniaLibroContable());*/

				facturaConXML.setStrIva(factura.getIva() != null ? factura.getIva().toString() : "");
				facturaConXML.setStrIeps(factura.getIeps() != null ? factura.getIeps().toString() : "");
				facturaConXML.setStrTotal(factura.getTotal() != null ? factura.getTotal().toString() : "");
				facturaConXML.setStrSubTotal(factura.getSubtotal() != null ? factura.getSubtotal().toString() : "");
				facturaConXML.setTasaIVA(factura.getPorcentajeIva());
				
				/* Forma y Metodo de Pago */
				facturaConXML.setFormaPago(factura.getFormaPago() != null ? factura.getFormaPago() : "");
				facturaConXML.setMetodoPago(factura.getMetodoPago() != null ? factura.getMetodoPago() : "");

				facturaConXML.setMoneda(factura.getMoneda());
				if (solicitud.getUsuarioByIdUsuario().getIdUsuario() != solicitud.getUsuarioByIdUsuarioSolicita()
						.getIdUsuario()) {
					facturaConXML.setSolicitante(true);
					facturaConXML.setIdSolicitanteJefe(solicitud.getUsuarioByIdUsuarioSolicita().getIdUsuario());
				} else {
					facturaConXML.setSolicitante(false);
				}

				/* si es orden de compra */
				if (factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S) {

					facturaConXML.setOrden_compra(true);
					facturaConXML.setIdOrdenCompra(factura.getOrdCompra());

					this.recibosSessionPrimeraVez = factura.getFacturaRecibos();
					for (int i = 0; i < this.recibosSessionPrimeraVez.size(); i++) {
						this.recibosSessionPrimeraVez.get(i).setIncluido(true);
					}

				}

				facturasDesglose = factura.getFacturaDesgloses();

				if (facturasDesglose != null && facturasDesglose.size() > Etiquetas.CERO) {
					for (FacturaDesglose fd : facturasDesglose) {
						FacturaDesgloseDTO fdd = new FacturaDesgloseDTO();
						
						fdd.setId_tabla_po(fd.getIdxxloDataSourceID());
						
						if(fd.getTrackAsset() != null && fd.getTrackAsset() > Etiquetas.CERO) {
							fdd.setTrackAsset(Etiquetas.TRUE);
						}else {
							fdd.setTrackAsset(Etiquetas.FALSE);
						}
						
						fdd.setLibroContable(fd.getLibroContable());
						
						fdd.setPar(fd.getPar());
						
						fdd.setAid(fd.getAid());
						
						fdd.setCategoriaMayor(fd.getCategoriaMayor());
						
						fdd.setCategoriaMenor(fd.getCategoriaMenor());
						
						fdd.setConcepto(StringEscapeUtils.unescapeJava(fd.getConcepto()));
						fdd.setCuentaContable(fd.getCuentaContable());
						fdd.setLocacion(fd.getLocacion());
						fdd.setStrSubTotal(fd.getSubtotal().toString());

						/* orden compra */
						if (factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S) {

							fdd.setNum_recibo(fd.getNumRecibo());
							fdd.setCantidad(fd.getCantidad());
							fdd.setSku(fd.getSku());
							fdd.setDescripcion_articulo(fd.getDescripcionSku());
							fdd.setPrecio_unitario(fd.getPrecioUnitarioSku().toString());
							fdd.setStrSubTotal(fd.getSubtotalSku().toString());
						}
						
						fdd.setCuentasContables(ccPermitidas);
						facturasDesgloseDTO.add(fdd);
					}
				}
			}
		} else {
			// mensaje de solicitud no valida a la vista.
			facturaConXML.setIdSolicitudSession(Etiquetas.CERO);
		}

		facturaConXML.setFacturaDesgloseList(facturasDesgloseDTO);

		// revisar si se trata de una modificaci๏ฟฝn para activar mensaje en la
		// vista
		if (session.getAttribute("actualizacion") != null) {
			facturaConXML.setModificacion(Etiquetas.TRUE);
			session.setAttribute("actualizacion", null);
		} else {
			facturaConXML.setModificacion(Etiquetas.FALSE);
		}

		// revisar si se trata de un nuevo registro para activar mensaje en la
		// vista
		if (session.getAttribute("creacion") != null) {
			facturaConXML.setCreacion(Etiquetas.TRUE);
			session.setAttribute("creacion", null);
		} else {
			facturaConXML.setCreacion(Etiquetas.FALSE);
		}

		// Enviar archivos anexados por solicitud:

		List<SolicitudArchivo> solicitudArchivoList = new ArrayList<>();
		if (facturaConXML.getIdSolicitudSession() > Etiquetas.CERO) {
			solicitudArchivoList = solicitudArchivoService
					.getAllSolicitudArchivoBySolicitud(facturaConXML.getIdSolicitudSession());
		}
		
		// valor mas menos para enviar conxml ordenes de compra.
		String valorMasMenos = parametroService.getParametroByName("masMenosTotal").getValor();

		/*
		 * ORDENES DE COMPRA
		 */
		// Usuario usr = usuarioService.getUsuario(usuario.getidusua);
		// getOrdenesCompraPO(usr,1);

		Usuario usuarioSession = usuarioService.getUsuarioSesion();

		model.put("facturaConXML", facturaConXML);
		model.put("lcPermitidas", lcPermitidas);
		model.put("ccPermitidas", ccPermitidas);
		model.put("lstCompanias", lstCompanias);
		model.put("listAids", listAids);
		model.put("listCatMayor", listCatMayor);
		model.put("listCatMenor", listCatMenor);
		model.put("lstProveedores", lstProveedores);
		model.put("lstMoneda", lstMoneda);
		if (facturaConXML.isSolicitante() == true
				&& facturaConXML.getIdSolicitanteJefe() == usuarioSession.getIdUsuario()) {
			lstUsuariosJefe = new ArrayList<>();
			lstUsuariosJefe.add(usuarioSession);
			model.put("lstUsuariosJefe", lstUsuariosJefe);
		} else {
			model.put("lstUsuariosJefe", lstUsuariosJefe);

		}

		model.put("estadoSolicitud", idEstadoSolicitud);
		model.put("solicitudArchivoList", solicitudArchivoList);
		model.put("tipoSolicitud",
				Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()));
		model.put("isSolicitante", usuario.getEspecificaSolicitante());
		model.put("usuarioSession", usuarioSession);
		model.put("valorMasMenos", valorMasMenos);

		/* Ordenes compra */
		model.put("ordenesCompra", ordenesCompra);

		return new ModelAndView("conXML", model);
	}

	@RequestMapping(value = "/sinXML", method = RequestMethod.GET)
	public ModelAndView inicioSinXML(HttpSession session, Integer id) {

		HashMap<String, Object> model = new HashMap<String, Object>();
		FacturaConXML facturaConXML = new FacturaConXML();
		Integer idEstadoSolicitud = 0;
		this.idUsuario = usuarioService.getUsuarioSesion().getIdUsuario();
		Integer usuarioSolicitud = this.idUsuario;
		CuentaContable ccNoComprobante = new CuentaContable();
		this.idLibroSesion = 0;
		List<XxloDataSourcePo> ordenesCompra = new ArrayList<>();
		recibosSession = new ArrayList<>();
		recibosSessionPrimeraVez = new ArrayList<>();

		if (id == null) {
			// destruir la sesion, creada por parametro
			session.setAttribute("solicitud", null);
		}

		// configuraciones permitidas por usuario
		List<UsuarioConfSolicitante> uconfigSol = new ArrayList<>();
		if (id != null && id > Etiquetas.CERO) {
			Solicitud solicitudSolicitante = solicitudService.getSolicitud(id);
			if (solicitudSolicitante != null) {
				uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(
						solicitudSolicitante.getUsuarioByIdUsuarioSolicita().getIdUsuario());
				usuarioSolicitud = solicitudSolicitante.getUsuarioByIdUsuarioSolicita().getIdUsuario();
			} else {
				uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
			}
		} else {
			uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
		}

		// Obtener combos filtrador por configuracion de solicitante.
		List<Locacion> lcPermitidas = UtilController.getLocacionesPermitidasPorUsuario(uconfigSol,
				Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor()),
				locacionService.getAllLocaciones(), usuarioSolicitud);
		List<CuentaContable> ccontable = cuentaContableService.getAllCuentaContable();
		List<CuentaContable> ccPermitidas = UtilController.getCuentasContablesPermitidasPorUsuario(uconfigSol,
				ccontable);
		
		// Obtener Companias, AID, Categoria Mayor, Categoria Menor, Proveedores y Monedas
		List<Compania> lstCompanias = companiaService.getAllCompania();
		List<Aid> listAids = aidService.getAllAid();
		List<CategoriaMayor> listCatMayor = categoriaMayor.getAllCategoriaMayor();
		List<CategoriaMenor> listCatMenor = categoriaMenor.getAllCategoriaMenor();
		List<Proveedor> lstProveedores = proveedorService.getAllProveedoresActivos();
		List<Moneda> lstMoneda = monedaService.getAllMoneda();

		String idMoneda = parametroService.getParametroByName("idPesos").getValor();
		Moneda moneda = new Moneda();
		moneda.setIdMoneda(Integer.parseInt(idMoneda));
		facturaConXML.setMoneda(moneda);
		// obtener los jefe para opcion de usuario solicitante.

		List<Usuario> lstUsuariosJefe = new ArrayList<>();
		Usuario usuario = usuarioService.getUsuario(idUsuario);
		if (usuario.getUsuario() != null) {
			lstUsuariosJefe.add(usuario.getUsuario());
		}

		Solicitud solicitud = null;
		Factura factura = null;
		List<FacturaDesglose> facturasDesglose = new ArrayList<>();
		List<FacturaDesgloseDTO> facturasDesgloseDTO = new ArrayList<>();

		// Obtener solicitud,factura y su desglose si se requiere:
		if (id != null && id > Etiquetas.CERO) {
			facturaConXML.setIdSolicitudSession(id);
			solicitud = solicitudService.getSolicitud(facturaConXML.getIdSolicitudSession());
			if (solicitud != null) {
				idEstadoSolicitud = solicitud.getEstadoSolicitud().getIdEstadoSolicitud();
				facturaConXML.setIdSolicitudSession(id);
			}
		}
		
		/*parametros configurados para sin xml*/
		CuentaContable ccparametro = cuentaContableService.getCCByNumeroCuenta(parametroService.getParametroByName("ccPAR").getValor());

		if (solicitud != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

			// si la solicitud es valida entonces crear una variable de sesion
			// para el update.
			session.setAttribute("solicitud", solicitud);

			factura = solicitud.getFacturas().get(Etiquetas.CERO);
			if (factura != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

				facturaConXML.setCompania(factura.getCompaniaByIdCompania());
				this.idLibroSesion = factura.getCompaniaByIdCompania().getIdcompania();
				facturaConXML.setProveedor(factura.getProveedor());
				facturaConXML.setRfcEmisor(factura.getProveedor().getRfc());
				facturaConXML.setFolioFiscal(factura.getFolioFiscal());
				facturaConXML.setFolio(factura.getFactura());
				facturaConXML.setSerie(factura.getSerieFactura());

				// retenciones
				if (factura.getConRetenciones() > Etiquetas.CERO_S) {
					facturaConXML.setConRetenciones(Etiquetas.TRUE);
					facturaConXML.setStrIsr_retenido(
							factura.getIsrRetenido() != null ? factura.getIsrRetenido().toString() : "");
					facturaConXML.setStrIva_retenido(
							factura.getIvaRetenido() != null ? factura.getIvaRetenido().toString() : "");
				} else {
					facturaConXML.setConRetenciones(Etiquetas.FALSE);
				}

				String fechaString = null;
				SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdfOut = new SimpleDateFormat("dd/MM/yyyy");

				try {
					fechaString = sdfOut.format(sdfIn.parse(factura.getFechaFactura().toString()));
					facturaConXML.setFecha_factura(fechaString);
				} catch (ParseException e) {

				}

				facturaConXML.setConcepto(StringEscapeUtils.unescapeJava(solicitud.getConceptoGasto()));

				/*if (factura.getTrackAsset() > Etiquetas.CERO) {
					facturaConXML.setTrack_asset(Etiquetas.TRUE);
				} else {
					facturaConXML.setTrack_asset(Etiquetas.FALSE);
				}

				facturaConXML.setPar(factura.getPar());
				facturaConXML.setId_compania_libro_contable(factura.getCompaniaByIdCompaniaLibroContable());*/

				facturaConXML.setStrIva(factura.getIva() != null ? factura.getIva().toString() : "");
				facturaConXML.setStrIeps(factura.getIeps() != null ? factura.getIeps().toString() : "");
				facturaConXML.setStrTotal(factura.getTotal() != null ? factura.getTotal().toString() : "");
				facturaConXML.setStrSubTotal(factura.getSubtotal() != null ? factura.getSubtotal().toString() : "");
				facturaConXML.setTasaIVA(factura.getPorcentajeIva());

				facturaConXML.setMoneda(factura.getMoneda());
				if (solicitud.getUsuarioByIdUsuario().getIdUsuario() != solicitud.getUsuarioByIdUsuarioSolicita()
						.getIdUsuario()) {
					facturaConXML.setSolicitante(true);
					facturaConXML.setIdSolicitanteJefe(solicitud.getUsuarioByIdUsuarioSolicita().getIdUsuario());
				} else {
					facturaConXML.setSolicitante(false);
				}

				facturasDesglose = factura.getFacturaDesgloses();
				facturasDesglose = Utilerias.getSortFacturasDesglose(facturasDesglose);
				
				
				/* si es orden de compra */
				if (factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S) {

					facturaConXML.setOrden_compra(true);
					facturaConXML.setIdOrdenCompra(factura.getOrdCompra());

					this.recibosSessionPrimeraVez = factura.getFacturaRecibos();
					for (int i = 0; i < this.recibosSessionPrimeraVez.size(); i++) {
						this.recibosSessionPrimeraVez.get(i).setIncluido(true);
					}

				}
				

				if (facturasDesglose != null && facturasDesglose.size() > Etiquetas.CERO) {
					for (FacturaDesglose fd : facturasDesglose) {
						FacturaDesgloseDTO fdd = new FacturaDesgloseDTO();
						
						fdd.setId_tabla_po(fd.getIdxxloDataSourceID());
						
						fdd.setId_tabla_po(fd.getIdxxloDataSourceID());
						
						fdd.setTrackAsset(Etiquetas.TRUE);
						
						fdd.setLibroContable(fd.getLibroContable());
						
						fdd.setPar(fd.getPar());
						
						fdd.setAid(fd.getAid());
						
						fdd.setCategoriaMayor(fd.getCategoriaMayor());
						
						fdd.setCategoriaMenor(fd.getCategoriaMenor());
						
						fdd.setConcepto(StringEscapeUtils.unescapeJava(fd.getConcepto()));
						fdd.setCuentaContable(fd.getCuentaContable());
						fdd.setLocacion(fd.getLocacion());
						fdd.setStrSubTotal(fd.getSubtotal().toString());
						
						/* orden compra */
						if (factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S) {

							fdd.setNum_recibo(fd.getNumRecibo());
							fdd.setCantidad(fd.getCantidad());
							fdd.setSku(fd.getSku());
							fdd.setDescripcion_articulo(fd.getDescripcionSku());
							fdd.setPrecio_unitario(fd.getPrecioUnitarioSku().toString());
							fdd.setStrSubTotal(fd.getSubtotalSku().toString());

							/* Agregar cuenta contable seleccionada en recibo */
							if (factura.getIsOrdCompra() == Etiquetas.UNO_S) {
								CuentaContable ccRecibo = cuentaContableService
										.getCuentaContable(fdd.getCuentaContable().getIdCuentaContable());
								ccPermitidas.add(ccRecibo);
							}

						}
						
						
					    List<CuentaContable> ccPermitidasComprobacion = UtilController.getCuentasContablesPermitidasPorLocacion(fd.getLocacion().getIdLocacion(), uconfigSol, 2);
					    if(!ccPermitidasComprobacion.contains(ccparametro)){
					    	ccPermitidasComprobacion.add(ccparametro);
					    }
                        fdd.setCuentasContables(ccPermitidasComprobacion);
						
						
						facturasDesgloseDTO.add(fdd);
					}
				}
			}
			// get cuenta contable from table
			if (factura.getFacturaDesgloses().isEmpty() == false)
				ccNoComprobante = cuentaContableService.getCuentaContable(
						factura.getFacturaDesgloses().get(0).getCuentaContable().getIdCuentaContable());
		} else {
			// mensaje de solicitud no valida a la vista.
			facturaConXML.setIdSolicitudSession(Etiquetas.CERO);

			// Set cuenta contable no deducible para documentos sin comprobante
			// fiscal
			String cuentaContable = parametroService.getParametroByName("ccNoDeducible").getValor();
			Integer ccID = UtilController.getIDByCuentaContable(cuentaContable,
					cuentaContableService.getAllCuentaContable());
			if (ccID != null) {
				ccNoComprobante = cuentaContableService.getCuentaContable(ccID);
			}
		}

		facturaConXML.setFacturaDesgloseList(facturasDesgloseDTO);

		// revisar si se trata de una modificaci๏ฟฝn para activar mensaje en la
		// vista
		if (session.getAttribute("actualizacion") != null) {
			facturaConXML.setModificacion(Etiquetas.TRUE);
			session.setAttribute("actualizacion", null);
		} else {
			facturaConXML.setModificacion(Etiquetas.FALSE);
		}

		// revisar si se trata de un nuevo registro para activar mensaje en la
		// vista
		if (session.getAttribute("creacion") != null) {
			facturaConXML.setCreacion(Etiquetas.TRUE);
			session.setAttribute("creacion", null);
		} else {
			facturaConXML.setCreacion(Etiquetas.FALSE);
		}

		// Enviar archivos anexados por solicitud:

		List<SolicitudArchivo> solicitudArchivoList = new ArrayList<>();
		if (facturaConXML.getIdSolicitudSession() > Etiquetas.CERO) {
			solicitudArchivoList = solicitudArchivoService
					.getAllSolicitudArchivoBySolicitud(facturaConXML.getIdSolicitudSession());
		}

		Usuario usuarioSession = usuarioService.getUsuarioSesion();
		
		
		String  parametroCContable = String.valueOf(ccparametro.getIdCuentaContable());
		String  parametroCmayor = parametroService.getParametroByName("catMayExpense").getValor();
		String  parametroCmaenor = parametroService.getParametroByName("catMenExpense").getValor();
		
		// valor mas menos para enviar conxml ordenes de compra.
				String valorMasMenos = parametroService.getParametroByName("masMenosTotal").getValor();
		

		model.put("ccNoComprobante", ccNoComprobante);
		model.put("facturaConXML", facturaConXML);
		model.put("lcPermitidas", lcPermitidas);
		model.put("ccPermitidas", ccPermitidas);
		model.put("listAids", listAids);
		model.put("listCatMayor", listCatMayor);
		model.put("listCatMenor", listCatMenor);
		model.put("lstCompanias", lstCompanias);
		model.put("lstProveedores", lstProveedores);
		model.put("lstMoneda", lstMoneda);
		model.put("lstUsuariosJefe", lstUsuariosJefe);
		model.put("estadoSolicitud", idEstadoSolicitud);
		model.put("solicitudArchivoList", solicitudArchivoList);
		model.put("tipoSolicitud",
				Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor()));
		model.put("isSolicitante", usuario.getEspecificaSolicitante());
		model.put("usuarioSession", usuarioSession);
		model.put("parametroCContable", parametroCContable);
		model.put("parametroCmayor", parametroCmayor);
		model.put("parametroCmaenor", parametroCmaenor);
		model.put("valorMasMenos", valorMasMenos);
		
		/* Ordenes compra */
		model.put("ordenesCompra", ordenesCompra);

		return new ModelAndView("conXML", model);
	}

	@RequestMapping(value = "/saveFacturaXML", method = RequestMethod.POST)
	public ModelAndView saveFacturaXML(@ModelAttribute("facturaConXML") FacturaConXML facturaConXML,
			HttpSession session, @RequestParam("file") MultipartFile[] files, HttpServletRequest request) {
		
		facturaDesglose.clear();
		
		//Audited IP Address SF
		StringBuilder audited = new StringBuilder();
		
		audited.append("\n").append("*** IP Audited ***");
		try {
			String remoteAddress = request.getRemoteAddr();
			InetAddress remote = InetAddress.getByName(remoteAddress);
			audited.append("\n").append("ID Usuario: ").append(idUsuario);
			audited.append("\n").append("Hostname: ").append(remote.getHostName());
			audited.append("\n").append("IP Address: ").append(remote.getHostAddress());
			
		}catch(Exception e){
			audited.append("\n").append(e);
			
		}
		audited.append("\n").append("******************");
		logger.info(audited);
		
		// se crea el objeto de solicitud, factura y desglose
		Solicitud solicitudSaveOrUpdate = new Solicitud();
		Factura facturaSaveOrUpdate = new Factura();
		FacturaDesglose fdesgloseSaveOrUpdate = new FacturaDesglose();
		Integer idSolicitudNueva = null;
		
		// saber si hay en session una solicitud
		Solicitud solicitudActual = (Solicitud) session.getAttribute("solicitud");

		if (solicitudActual != null && solicitudActual.getIdSolicitud() > Etiquetas.CERO) {

			// 1 - ACTUALIZAR la solicitud
			logger.info("Actualizacion de Solicitud / Factura, No. Solicitud:" + solicitudActual.getIdSolicitud());
			
			/* TODO quitar el llamado a bd */
			Solicitud solicitudSaveOrUpdateAux = solicitudService.getSolicitud(solicitudActual.getIdSolicitud());

			String ordenFacturaOld = solicitudSaveOrUpdateAux.getFacturas().get(0).getOrdCompra();

			// se cargan los datos de la solicitud
			solicitudSaveOrUpdate = loadSolicitud(facturaConXML);
			solicitudSaveOrUpdate.setCreacionFecha(solicitudSaveOrUpdateAux.getCreacionFecha());
			solicitudSaveOrUpdate.setCreacionUsuario(solicitudSaveOrUpdateAux.getCreacionUsuario());
			solicitudSaveOrUpdate.setIdSolicitud(solicitudActual.getIdSolicitud());
			solicitudSaveOrUpdate.setModificacionFecha(new Date());
			solicitudSaveOrUpdate.setModificacionUsuario(idUsuario);
			// solicitud service actualizar.
			solicitudSaveOrUpdate.setEstadoSolicitud(solicitudSaveOrUpdateAux.getEstadoSolicitud());
			// solicitudSaveOrUpdate.setConceptoGasto("");
			solicitudSaveOrUpdate = solicitudService.updateSolicitud(solicitudSaveOrUpdate);
			idSolicitudNueva = solicitudSaveOrUpdate.getIdSolicitud();

			// si se guardo y por lo tanto el id' que regreso es mayor a cero
			// entonces cargamos los datos de factura.
			if (idSolicitudNueva > Etiquetas.CERO) {
				facturaSaveOrUpdate = loadFactura(idSolicitudNueva, facturaConXML);
				facturaSaveOrUpdate.setActivo(Etiquetas.UNO_S);
				facturaSaveOrUpdate.setCreacionUsuario(
						solicitudSaveOrUpdateAux.getFacturas().get(Etiquetas.CERO).getCreacionUsuario());
				facturaSaveOrUpdate.setCreacionFecha(
						solicitudSaveOrUpdateAux.getFacturas().get(Etiquetas.CERO).getCreacionFecha());
				facturaSaveOrUpdate.setModificacionFecha(new Date());
				facturaSaveOrUpdate.setModificacionUsuario(idUsuario);
				Integer idFactura = solicitudSaveOrUpdateAux.getFacturas().get(Etiquetas.CERO).getIdFactura();
				facturaSaveOrUpdate.setIdFactura(idFactura);

				// ACTUALIZAR: llamada al servicio con el objeto cargado.
				facturaSaveOrUpdate = facturaService.updateFactura(facturaSaveOrUpdate);

				// actualizar ARCHIVOS
				if (files != null && files.length > Etiquetas.CERO) {
					facturaArchivoService.deleteAllFacturaArchivoByIdFactura(idFactura);
					guardarArchivosFactura(files, idSolicitudNueva, idFactura);
				}

				// eliminar desgloses guardados y reemplazarlos.
				facturaDesgloseService.deleteAllByIdFactura(idFactura);

				/* Eliminar recibos y volverlos a guardar */
				List<FacturaRecibo> listFacturasRecibos = facturaReciboService
						.getAllFacturaRecibobyFactura(facturaSaveOrUpdate.getIdFactura());
				for (FacturaRecibo t : listFacturasRecibos) {
					facturaReciboService.deleteFacturaRecibo(t);
					xxloDataSourcePoService.updateRecibosEnFactura(t.getNumRecibo(), false,
							facturaSaveOrUpdate.getTipoFactura().getIdTipoFactura(), ordenFacturaOld);
				}
				
				/*<begin-jgh 22.02.2018> */
				 
				//System.out.println("***TAMAÑO D ELA LISTA***");
				//System.out.println(facturaConXML.getFacturaDesgloseList().size());
				//facturaDesglose=facturaConXML.getFacturaDesgloseList();
				
				for (FacturaDesgloseDTO fdto : facturaConXML.getFacturaDesgloseList()) {
				  //    System.out.println("SubTitotal-->"+fdto.getStrSubTotal());
				      if(fdto.getStrSubTotal()!=null) {
				    	  facturaDesglose.add(fdto);
				      }
					
				}
				
				//System.out.println("*TAMAÑO DESPUES DE AGREGAR");
				//System.out.println(facturaDesglose.size());
				
				/*for (FacturaDesgloseDTO facturaNormal : facturaDesglose) {
					System.out.println("Nueva lista de subtotales-->"+facturaNormal.getStrSubTotal());
				}*/
				
				
				/*<end-jgh 22.02.2018> */
				
				/* Guardar recibos de facturas para ordenes de compra */
				if (facturaConXML.isOrden_compra()) {

					for (FacturaRecibo recibo : this.recibosSession) {

						if (recibo.isIncluido()) {
							recibo.setFactura(new Factura(idFactura));
							recibo.setImporte(Utilerias.convertStringToBigDecimal(recibo.getImporteString()));
							facturaReciboService.createFacturaRecibo(recibo);
							xxloDataSourcePoService.updateRecibosEnFactura(recibo.getNumRecibo(), true,
									facturaSaveOrUpdate.getTipoFactura().getIdTipoFactura(),
									facturaSaveOrUpdate.getOrdCompra());
						} else {

							xxloDataSourcePoService.updateRecibosEnFactura(recibo.getNumRecibo(), false,
									facturaSaveOrUpdate.getTipoFactura().getIdTipoFactura(),
									facturaSaveOrUpdate.getOrdCompra());

						}
					}

				}

				// si la factura tiene desgloses se guardan.
				//if (idFactura > Etiquetas.CERO && facturaConXML.getFacturaDesgloseList() != null && facturaConXML.getFacturaDesgloseList().size() > Etiquetas.CERO) {
				//<begin-jgh 22.02.2018>
				 if(idFactura >Etiquetas.CERO && facturaDesglose!=null && facturaDesglose.size()> Etiquetas.CERO) {
				//</end-jgh 22.02.2018>
					 
					

					 //<begin-jgh 22.02.2018>
					//for (FacturaDesgloseDTO fdto : facturaConXML.getFacturaDesgloseList()) {
					 for(FacturaDesgloseDTO fdto:facturaDesglose) {
					 //</end-jgh 22.02.2018>
						 		if (facturaConXML.isOrden_compra() == false) {

							// cargar datos del desglose si tienen info: evitar
							// nulos
							if (fdto.getStrSubTotal() != null && fdto.getStrSubTotal().isEmpty() == false&& fdto.getCuentaContable() != null) {
								fdesgloseSaveOrUpdate = loadFacturaDesglose(idFactura, fdto, facturaConXML);
								// guardar desglose
								facturaDesgloseService.createFacturaDesglose(fdesgloseSaveOrUpdate);
							}

						} else {

							// evitar insertar desgloses nulos para deslgoses
							// con ordenes de compra
							if (fdto != null && fdto.getSku() != null && fdto.getDescripcion_articulo() != null && fdto.getPrecio_unitario() != null) {
								fdesgloseSaveOrUpdate = loadFacturaDesglose(idFactura, fdto, facturaConXML);
								// guardar desglose
								facturaDesgloseService.createFacturaDesglose(fdesgloseSaveOrUpdate);
								
							}

						}

					}

				}

			}

			// crear variable de sesion para el mensaje de actualzaci๏ฟฝn
			Integer updt = Etiquetas.UNO;
			session.setAttribute("actualizacion", updt);

		} else {
			/* ******* CREAR INFO POR PRIMERA VEZ ********* */
			logger.info("Creación de Solicitud / Factura");
			
			// <BEGIN 04/06/2018 JGH>
			//facturaDesglose=facturaConXML.getFacturaDesgloseList();
			for(FacturaDesgloseDTO fdto: facturaConXML.getFacturaDesgloseList()) {
		      if(fdto.getStrSubTotal()!=null) {
		    	  facturaDesglose.add(fdto);
		      }
			}
			// <END 04/06/2018 JGH>
						
			// se cargan los datos de la solicitud
			solicitudSaveOrUpdate = loadSolicitud(facturaConXML);

			// campos control
			solicitudSaveOrUpdate.setCreacionFecha(new Date());
			solicitudSaveOrUpdate.setCreacionUsuario(idUsuario);
			solicitudSaveOrUpdate.setEstadoSolicitud(new EstadoSolicitud(
					Integer.parseInt(parametroService.getParametroByName("idEstadoSolicitudCapturada").getValor())));

			// guardar factura y desglose por primera vez.
			idSolicitudNueva = solicitudService.createSolicitud(solicitudSaveOrUpdate);

			// si se guardo y por lo tanto el id' que regreso es mayor a cero
			// entonces cargamos los datos de factura.
			if (idSolicitudNueva > Etiquetas.CERO) {
				facturaSaveOrUpdate = loadFactura(idSolicitudNueva, facturaConXML);
				facturaSaveOrUpdate.setActivo(Etiquetas.UNO_S);
				facturaSaveOrUpdate.setCreacionUsuario(idUsuario);
				facturaSaveOrUpdate.setCreacionFecha(new Date());
			}

			// CREAR factura
			Integer idFactura = facturaService.createFactura(facturaSaveOrUpdate);
			logger.info("ID Factura: " + idFactura);

			// GUARDAR ARCHIVOS
			guardarArchivosFactura(files, idSolicitudNueva, idFactura);
			
			
			/* Guardar recibos de facturas para ordenes de compra */
			if (facturaConXML.isOrden_compra()) {

				for (FacturaRecibo recibo : this.recibosSession) {
					if (recibo.isIncluido()) {
						recibo.setFactura(new Factura(idFactura));
						recibo.setImporte(Utilerias.convertStringToBigDecimal(recibo.getImporteString()));
						facturaReciboService.createFacturaRecibo(recibo);

						// reservar recibo check_association = 1
						xxloDataSourcePoService.updateRecibosEnFactura(recibo.getNumRecibo(), true,
								facturaSaveOrUpdate.getTipoFactura().getIdTipoFactura(),
								facturaSaveOrUpdate.getOrdCompra());
					}

				}

			}

			// si la factura tiene desgloses se guardan.
			 //<begin-jgh 22.02.2018 >
			
			//if (idFactura > Etiquetas.CERO && facturaConXML.getFacturaDesgloseList() != null
			//&& facturaConXML.getFacturaDesgloseList().size() > Etiquetas.CERO) {
		    //for (FacturaDesgloseDTO fdto : facturaConXML.getFacturaDesgloseList()) {
			 if(idFactura> Etiquetas.CERO && facturaDesglose!=null && facturaDesglose.size()>Etiquetas.CERO) {
				for(FacturaDesgloseDTO fdto :facturaDesglose) {
			  
			  
			//</end-jgh 22.02.2018>		
					// evitar insertar desgloses nulos para deslgoses sin
					// ordenes de compra
					if (facturaConXML.isOrden_compra() == false) {
						if (fdto != null && fdto.getStrSubTotal() != null && fdto.getConcepto() != null
								&& fdto.getLocacion() != null) {
							// cargar datos del desglose
							fdesgloseSaveOrUpdate = loadFacturaDesglose(idFactura, fdto, facturaConXML);
							// guardar desglose
							facturaDesgloseService.createFacturaDesglose(fdesgloseSaveOrUpdate);
						}
					} else {
						// evitar insertar desgloses nulos para deslgoses con
						// ordenes de compra
						if (fdto != null && fdto.getSku() != null && fdto.getDescripcion_articulo() != null
								&& fdto.getPrecio_unitario() != null) {
							// cargar datos del desglose
							fdesgloseSaveOrUpdate = loadFacturaDesglose(idFactura, fdto, facturaConXML);
							// guardar desglose
							facturaDesgloseService.createFacturaDesglose(fdesgloseSaveOrUpdate);
						}
					}

				}
			}

			// crear variable de sesion para el mensaje de actualzacion
			Integer newrow = Etiquetas.UNO;
			session.setAttribute("creacion", newrow);

		} // fin.else
		
		if (facturaConXML.getTipoSolicitud() == Integer
				.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor())) {
			return new ModelAndView("redirect:conXML?id=" + idSolicitudNueva);
		} else {
			return new ModelAndView("redirect:sinXML?id=" + idSolicitudNueva);
		}
	}

	private Solicitud loadSolicitud(FacturaConXML facturaConXML) {

		Solicitud sol = new Solicitud();

		sol.setActivo(Etiquetas.UNO_S);

		sol.setMoneda(facturaConXML.getMoneda());
		sol.setCompania(facturaConXML.getCompania());

		sol.setFormaPago(new FormaPago(
				Integer.parseInt(parametroService.getParametroByName("formaPagoTransferencia").getValor())));

		// definir tipo de solicitud
		sol.setTipoSolicitud(new TipoSolicitud(facturaConXML.getTipoSolicitud()));
		sol.setMontoTotal(Utilerias.convertStringToBigDecimal(facturaConXML.getStrTotal()));
		sol.setUsuarioByIdUsuario(new Usuario(idUsuario));

		if (facturaConXML.isSolicitante()) {
			sol.setUsuarioByIdUsuarioSolicita(new Usuario(facturaConXML.getIdSolicitanteJefe()));
			sol.setLocacion(usuarioService.getUsuario(facturaConXML.getIdSolicitanteJefe()).getLocacion());
		} else {
			sol.setUsuarioByIdUsuarioSolicita(new Usuario(idUsuario));
			sol.setLocacion(usuarioService.getUsuario(idUsuario).getLocacion());
		}

		if (facturaConXML.getConcepto().length() >= 500) {
			sol.setConceptoGasto(StringEscapeUtils.escapeJava(facturaConXML.getConcepto()).substring(0, 500));

		} else {
			sol.setConceptoGasto(facturaConXML.getConcepto());
		}

		/*if (facturaConXML.isTrack_asset()) {
			sol.setTrackAsset(Etiquetas.UNO_S);
		} else {
			sol.setTrackAsset(Etiquetas.CERO_S);
		}*/

		return sol;
	}

	private Factura loadFactura(Integer idSolicitud, FacturaConXML facturaConXML) {

		Factura factura = new Factura();
		String folio = facturaConXML.getFolio() == null || facturaConXML.getFolio().isEmpty() ? facturaConXML.getFolioFiscal() : facturaConXML.getFolio();
		
		factura.setSolicitud(new Solicitud(idSolicitud));
		factura.setMoneda(facturaConXML.getMoneda());
		factura.setFactura(folio);
		factura.setCompaniaByIdCompania(facturaConXML.getCompania());
		/*factura.setCuentaContable(facturaConXML.getCuentaContable());*/

		if (facturaConXML.getConcepto().length() >= 500) {
			factura.setConceptoGasto(StringEscapeUtils.escapeJava(facturaConXML.getConcepto()).substring(0, 500));

		} else {
			factura.setConceptoGasto(facturaConXML.getConcepto());
		}
		
		/* if (facturaConXML.isTrack_asset()) {
			factura.setCompaniaByIdCompaniaLibroContable(facturaConXML.getId_compania_libro_contable());
			factura.setPar(facturaConXML.getPar());
			factura.setTrackAsset(Etiquetas.UNO_S);
		} else {
			factura.setTrackAsset(Etiquetas.CERO_S);
		}*/

		factura.setProveedor(facturaConXML.getProveedor());
		factura.setSerieFactura(facturaConXML.getSerie());
		factura.setFolioFiscal(facturaConXML.getFolioFiscal());
		if (facturaConXML.getTipoFactura() == null) {
			// tipo de factura standar
			factura.setTipoFactura(
					new TipoFactura(Integer.parseInt(parametroService.getParametroByName("idTipoFactura").getValor())));
		} else {
			factura.setTipoFactura(new TipoFactura(facturaConXML.getTipoFactura()));
		}

		try {
			factura.setFechaFactura(Utilerias.parseDate(facturaConXML.getFecha_factura(), "dd/MM/yyyy"));
		} catch (ParseException e) {

		}
		
		factura.setSubtotal(Utilerias.convertStringToBigDecimal(facturaConXML.getStrSubTotal()));
		factura.setTotal(Utilerias.convertStringToBigDecimal(facturaConXML.getStrTotal()));

		if (facturaConXML.getStrIeps() != null) {
			factura.setIeps(Utilerias.convertStringToBigDecimal(facturaConXML.getStrIeps()));
		}
		if (facturaConXML.getStrIva() != null) {
			factura.setIva(Utilerias.convertStringToBigDecimal(facturaConXML.getStrIva()));
		}

		factura.setPorcentajeIva(facturaConXML.getTasaIVA());

		// retenciones
		if (facturaConXML.isConRetenciones()) {
			factura.setConRetenciones(Etiquetas.UNO_S);
			factura.setIvaRetenido(Utilerias.convertStringToBigDecimal(facturaConXML.getStrIva_retenido()));
			factura.setIsrRetenido(Utilerias.convertStringToBigDecimal(facturaConXML.getStrIsr_retenido()));
		} else {
			factura.setConRetenciones(Etiquetas.CERO_S);
		}

		/* si es orden de compra marcar y guardar valores a nivel factura */
		if (facturaConXML.isOrden_compra()) {

			
			if(facturaConXML.getIdOrdenCompra() != null && !facturaConXML.getIdOrdenCompra().equals("-1")){
				factura.setIsOrdCompra(Etiquetas.UNO_S);
			factura.setOrdCompra(facturaConXML.getIdOrdenCompra());
			// obtener el vendor site code
			List<XxloDataSourcePo> ordCompra = xxloDataSourcePoService.getPOsByIDPO(facturaConXML.getIdOrdenCompra());
			String vendorName = ordCompra.get(0).getVendorSiteCode();
			factura.setVendorSiteCode(vendorName);
			}


		}
		
		/* Forma y Medodo de Pago */
		factura.setMetodoPago(facturaConXML.getMetodoPago());
		factura.setFormaPago(facturaConXML.getFormaPago());

		return factura;
	}

	private FacturaDesglose loadFacturaDesglose(Integer idFactura, FacturaDesgloseDTO fdto, FacturaConXML facturaConXML) {

		FacturaDesglose fdesglose = new FacturaDesglose();

		fdesglose.setFactura(new Factura(idFactura));
		fdesglose.setLocacion(fdto.getLocacion());
		fdesglose.setCuentaContable(fdto.getCuentaContable());

		if (fdto.getConcepto() != null) {

			if (fdto.getConcepto().length() >= 500) {
				fdesglose.setConcepto(fdto.getConcepto().substring(0, 500));
			} else {
				fdesglose.setConcepto(fdto.getConcepto());
			}
		} else {
			fdesglose.setConcepto("orden de compra.");
		}
		
		fdesglose.setSubtotal(Utilerias.convertStringToBigDecimal(fdto.getStrSubTotal()));
		fdesglose.setCreacionFecha(new Date());
		fdesglose.setActivo(Etiquetas.UNO_S);
		
		/* Track Asset */
		if (fdto.isTrackAsset()) {
			
			fdesglose.setTrackAsset(Etiquetas.UNO_S);
			
			if (fdto.getLibroContable() != null && fdto.getLibroContable().getIdcompania() > Etiquetas.CERO) {
				fdesglose.setLibroContable(fdto.getLibroContable());
			}
			
			if (fdto.getPar() != null) {
				fdesglose.setPar(fdto.getPar());
			}
			
			if (fdto.getAid() != null && fdto.getAid().getIdAid() > Etiquetas.CERO) {
				fdesglose.setAid(fdto.getAid());
			}
			
			if (fdto.getCategoriaMayor() != null && fdto.getCategoriaMayor().getIdCategoriaMayor() > Etiquetas.CERO) {
				fdesglose.setCategoriaMayor(fdto.getCategoriaMayor());
			}

			if (fdto.getCategoriaMenor() != null && fdto.getCategoriaMenor().getIdCategoriaMenor() > Etiquetas.CERO) {
				fdesglose.setCategoriaMenor(fdto.getCategoriaMenor());
			}
		}

		/* para orden de compra */
		if (facturaConXML.isOrden_compra()) {

			fdesglose.setNumRecibo(fdto.getNum_recibo());
			fdesglose.setCantidad(fdto.getCantidad());
			fdesglose.setSku(fdto.getSku());
			fdesglose.setDescripcionSku(fdto.getDescripcion_articulo());
			fdesglose.setPrecioUnitarioSku(Utilerias.convertStringToBigDecimal(fdto.getPrecio_unitario()));
			fdesglose.setSubtotalSku(Utilerias.convertStringToBigDecimal(fdto.getStrSubTotal()));
			fdesglose.setIdxxloDataSourceID(fdto.getId_tabla_po());

		}

		return fdesglose;
	}

	@RequestMapping(value = "/addRowConXML", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> addRow(HttpSession session, @RequestParam Integer numrows,
			@RequestParam Integer tipoSolicitud, @RequestParam Integer idSolicitud, @RequestParam Integer idSolicitante,
			@RequestParam String concepto,  HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		StringBuilder row2 = new StringBuilder();

		List<UsuarioConfSolicitante> uconfigSol = new ArrayList<>();
		Integer idLocacionUsuario = null;
		if (idSolicitante != null && idSolicitante != -1) {
			// configuraciones permitidas por solicitante seleccionado
			uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idSolicitante);
			idLocacionUsuario = usuarioService.getUsuario(idSolicitante).getLocacion().getIdLocacion();
			solicitanteSeleccion = idSolicitante;
		} else {
			// configuraciones permitidas por usuario en sesion
			uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
			idLocacionUsuario = usuarioService.getUsuario(idUsuario).getLocacion().getIdLocacion();
			solicitanteSeleccion = this.idUsuario;
		}

		// filtrar locaciones por tipo de solicitud.
		// Obtener combos filtrador por configuracion de solicitante.
		List<Locacion> lcPermitidas = UtilController.getLocacionesPermitidasPorUsuario(uconfigSol, tipoSolicitud,
				locacionService.getAllLocaciones(), solicitanteSeleccion);

		// OBTENER AID'S
		//List<Aid> listaAID = new ArrayList<>();
		//if (this.idLibroSesion != null && this.idLibroSesion > 0) {
		//	List<AidConfiguracion> configuration = aidConfiguracionService.getAllAidConfiguracion();
		//	for (AidConfiguracion conf : configuration) {
		//		if (conf.getCompania().getIdcompania() == idLibroSesion) {
		//			if (listaAID.contains(conf.getAid()) == false) {
		//				listaAID.add(conf.getAid());
		//			}
		//		}
		//	}
		//}

		// String num = String.valueOf(numrows);
		// numero aleatoreo para serializar los inputs.
		String num = Utilerias.getRandomNum();

		Integer numLinea = numrows + 1;

		row2.append("<tr class=\"odd gradeX\">");
		row2.append("<td align=\"center\"><div class=\"linea\">" + numLinea + "</div></td>");

		// subtotal
		row2.append("<td><input step=\"any\" onblur=\"sumSbt()\" id=\"facturaDesgloseList" + num
				+ ".strSubTotal\" name=\"facturaDesgloseList[" + num
				+ "].strSubTotal\" class=\"form-control subtotales currencyFormat sbtGrid\" value=\"\" type=\"text\"></td>");

		// locaciones
		String accionSeleccion = "onChange=\"actualizarCuentas(this.id)\"";
		row2.append("<td><select " + accionSeleccion + " id=\"facturaDesgloseList" + num
				+ ".locacion.idLocacion\" name=\"facturaDesgloseList[" + num
				+ "].locacion.idLocacion\" class=\"form-control locaciones\">");
		row2.append("<option value=\"-1\">Seleccione:</option>");
		for (Locacion loc : lcPermitidas) {

			if (idSolicitud == Integer
					.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor())
					&& loc.getIdLocacion() == idLocacionUsuario) {
				row2.append("<option selected  value=\"");
			} else {
				row2.append("<option value=\"");
			}
			row2.append(String.valueOf(loc.getIdLocacion()));
			row2.append("\"> " + loc.getNumeroDescripcionLocacion() + " </option>");
		}
		row2.append("</select>");
		row2.append("</td>");

		// si el tipo de solicitud es sin xml no mercancias, se cargan las
		// cuentas contables por
		// locacion de usuario seleccionada automatica.
		List<CuentaContable> ccPermitidasPorUsuario = null;
//		if (idSolicitud == Integer 
//				.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor())) {
//			ccPermitidasPorUsuario = UtilController.getCuentasContablesPermitidasPorLocacion(idLocacionUsuario,
//					uConfsolicitanteService.getUsuarioConfSolByIdUsuario(solicitanteSeleccion), idSolicitud);
//		}
		// cuenta contables
		
		String ccOnChange = "";
		if(idSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor())){
		   ccOnChange = "onChange=\"actualizarCategorias(this)\"";
		}else{
			   ccOnChange = "";
		}
		
		row2.append("<td><select "+ccOnChange+" id=\"facturaDesgloseList" + num
				+ ".cuentaContable.idCuentaContable\" name=\"facturaDesgloseList[" + num
				+ "].cuentaContable.idCuentaContable\" class=\"form-control ccontable\">");
		//row2.append("<option value=\"-1\">Seleccione:</option>");

		// sin xml cuentas precargadas.
		if (idSolicitud == Integer
				.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor())) {
			
			//cuenta contable configurada
			Integer idCuentaContableNoND = UtilController.getIDByCuentaContable(
					parametroService.getParametroByName("ccPAR").getValor(),
					cuentaContableService.getAllCuentaContable());
			
			CuentaContable cc = new CuentaContable();
			if (idCuentaContableNoND != null) {
				
				
				
				cc = cuentaContableService.getCuentaContable(idCuentaContableNoND);
//				if(track_asset){
//					row2.append("<option selected value=\"" + cc.getIdCuentaContable() + "\"> "	+ cc.getNumeroDescripcionCuentaContable() + " </option>");
//				}else{
//					row2.append("<option value=\"" + cc.getIdCuentaContable() + "\"> "	+ cc.getNumeroDescripcionCuentaContable() + " </option>");
//				}
//				
//				
				
				row2.append(UtilController.getCuentasContablesHTML(idLocacionUsuario, uconfigSol, idCuentaContableNoND,tipoSolicitud,cc));				
				
				
			} else {
				row2.append("<option selected value=\"-1\"> " + etiqueta.CCNODED_NO_CONFIGURADA + " </option>");
			}
			
			
			
		}
		row2.append("</select>");
		row2.append("</td>");

		// concepto
		row2.append("<td><input maxlength=\"240\" id=\"facturaDesgloseList" + num
				+ ".concepto\" name=\"facturaDesgloseList[" + num
				+ "].concepto\" class=\"form-control conceptogrid\" value=\"" + concepto + "\" type=\"text\"></td>");

		//if (idSolicitud == 1) {
			
			//Track Asset
			row2.append("<td style=\"text-align: center;\"><input type=\"checkbox\" id=\"facturaDesgloseList" + num
					+ ".trackAsset\" name=\"facturaDesgloseList[" + num + "].trackAsset\" onclick=\"activeTrackAssetOptions(" + num + ");\"></td>");
			
			//ID Compania Libro Contable
			List<Compania> lstCompanias = companiaService.getAllCompania();
			row2.append("<td><select id=\"facturaDesgloseList" + num
					+ ".libroContable.idcompania\" name=\"facturaDesgloseList[" + num
					+ "].libroContable.idcompania\" class=\"form-control\" onchange=\"selectLibroContable(this.value, " + num + ")\" disabled>");
			row2.append("<option value=\"-1\">Seleccione:</option>");
			for (Compania compania : lstCompanias) {
				if (compania.getEsContable() != null && compania.getEsContable() == 1) {
					row2.append("<option value=\"");
					row2.append(String.valueOf(compania.getIdcompania()));
					row2.append("\" > " + compania.getDescripcion() + " </option>");
				}
			}
			row2.append("</select>");
			
			//PAR
			row2.append("<td><input type=\"text\" id=\"facturaDesgloseList" + num
					+ ".par\" name=\"facturaDesgloseList[" + num
					+ "].par\" class=\"form-control\" disabled></td>");
			
			// aids
			List<Aid> listaAID = new ArrayList<>();
			row2.append("<td><select id=\"facturaDesgloseList" + num
					+ ".aid.idAid\" name=\"facturaDesgloseList[" + num
					+ "].aid.idAid\" class=\"form-control\" onChange=\"alSeleccionarAID(this.value, " + num + ")\" disabled>");
			row2.append("<option value=\"-1\">Seleccione:</option>");
			for (Aid aid : listaAID) {
				row2.append("<option value=\"");
				row2.append(String.valueOf(aid.getIdAid()));
				row2.append("\" > " + aid.getNumeroDescripcionAid() + " </option>");
			}
			row2.append("</select>");
			row2.append("</td>");
			
			// categoria mayor
			row2.append("<td><select onChange=\"alSeleccionarCatMayor(this.value, " + num + ")\" id=\"facturaDesgloseList" + num
					+ ".categoriaMayor.idCategoriaMayor\" name=\"facturaDesgloseList[" + num
					+ "].categoriaMayor.idCategoriaMayor\" class=\"form-control\" disabled>");
			row2.append("<option value=\"-1\">Seleccione:</option>");
			row2.append("</select>");
			row2.append("</td>");

			// categoria menor
			row2.append("<td><select id=\"facturaDesgloseList" + num
					+ ".categoriaMenor.idCategoriaMenor\" name=\"facturaDesgloseList[" + num
					+ "].categoriaMenor.idCategoriaMenor\" class=\"form-control\" disabled>");
			row2.append("<option value=\"-1\">Seleccione:</option>");
			row2.append("</select>");
			row2.append("</td>");

		//}
		// remover
		row2.append("<td><button type=\"button\" class=\"btn btn-danger removerFila\">Remover</button></td>");
		row2.append("</tr>");

		ObjectMapper map = new ObjectMapper();
		if (!row2.toString().isEmpty()) {
			try {
				json = map.writeValueAsString(row2.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/getRFC", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> getRFC(HttpSession session, @RequestParam Integer idProveedor,
			HttpServletRequest request, HttpServletResponse response) {

		String result = null;
		String json = null;
		String rfc = "false";

		Proveedor proveedor = proveedorService.getProveedor(idProveedor);
		if (proveedor != null) {
			rfc = proveedor.getRfc();
		}
		result = rfc;

		ObjectMapper map = new ObjectMapper();
		if (!result.toString().isEmpty()) {
			try {
				json = map.writeValueAsString(result.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	// metodo ajax para el cambio de estatus a cancelada.
	@RequestMapping(value = "/cancelarSolicitud", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> cancelarSolicitud(HttpSession session,
			@RequestParam Integer idSolicitud, HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();

		if (idSolicitud != null && idSolicitud > Etiquetas.CERO) {
			Solicitud solicitud = solicitudService.getSolicitud(idSolicitud);

			solicitud.setIdEstadoSolicitud(
					Integer.parseInt(parametroService.getParametroByName("idEstadoSolicitudCancelada").getValor()));
			solicitud.setEstadoSolicitud(new EstadoSolicitud(
					Integer.parseInt(parametroService.getParametroByName("idEstadoSolicitudCancelada").getValor())));

			Solicitud solicitudAux = null;
			solicitudAux = solicitudService.updateSolicitud(solicitud);
			
			if (solicitudAux != null && solicitudAux.getFacturas().size() > Etiquetas.CERO) {
				if(solicitudAux.getFacturas().get(0).getIsOrdCompra() != null && solicitudAux.getFacturas().get(0).getIsOrdCompra().equals("1") == true) {
					Factura factura = solicitudAux.getFacturas().get(0);
					if (factura != null && factura.getFacturaRecibos() != null && factura.getFacturaRecibos().isEmpty() == false) {
						for (FacturaRecibo recibo : factura.getFacturaRecibos()) {
							xxloDataSourcePoService.updateRecibosEnFactura(recibo.getNumRecibo(), false,
									factura.getTipoFactura().getIdTipoFactura(), factura.getOrdCompra());
						}
					}
				}

			}

			if (solicitudAux != null) {
				result.put("resultado", "true");
			} else {
				result.put("resultado", "false");
			}

		} else {
			result.put("resultado", "false");
		}

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	private boolean guardarArchivosFactura(MultipartFile[] files, Integer idSolicitud, Integer idFactura) {

		// ruta de guardado.
		boolean guardado = false;

		File archivo = null;
		String UID = UUID.randomUUID().toString();
		String fileName = null;
		FacturaArchivo facturaArchivo = new FacturaArchivo();
		String ruta = Utilerias.getFilesPath() + idSolicitud + "/";
		List<TipoDocumento> tipos = tipoDocumentoService.getAllTipoDocumento();

		// revisar si existe el folder si no: se crea.
		if (files != null && files.length > Etiquetas.CERO) {
			for (MultipartFile file : files) {

				File folder = new File(ruta);
				if (!folder.exists()) {
					folder.mkdirs();
				}

				String descripcion = file.getOriginalFilename();
				String extencion = FilenameUtils.getExtension(descripcion);
				fileName = UID + "." + extencion;
				extencion = extencion.toUpperCase();
				archivo = new File(ruta + fileName);

				// validar tipo de archivo valido
				Integer idTipo = null;
				for (TipoDocumento tipo : tipos) {
					if (tipo.getDescripcion().equals(extencion)) {
						idTipo = tipo.getTipoDocumento();
						break;
					}
				}

				// si el tipo de archivo no es valido
				if (idTipo != null) {
					try {
						// para escribir en directorio
						byte[] bytes = file.getBytes();
						BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(archivo));
						buffStream.write(bytes);
						buffStream.close();

						// carga del objeto para guardar en tabla.
						facturaArchivo.setActivo(Etiquetas.UNO_S);
						facturaArchivo.setArchivo(fileName);
						facturaArchivo.setCreacionFecha(new Date());
						facturaArchivo.setCreacionUsuario(-1);
						facturaArchivo.setDescripcion(descripcion);
						facturaArchivo.setFactura(new Factura(idFactura));
						facturaArchivo.setTipoDocumento(new TipoDocumento(idTipo));

						// guardar.
						facturaArchivoService.createFacturaArchivo(facturaArchivo);

					} catch (IOException e) {
						logger.error(e);
					}
				}

			}
			guardado = true;
		}

		return guardado;
	}

	// metodo ajax para el cambio de estatus a cancelada.
	@RequestMapping(value = "/seleccionLocacion", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> seleccionLocacion(HttpSession session, @RequestParam Integer idLocacion,
			@RequestParam String idElemento, @RequestParam Integer tipoSolicitud, HttpServletRequest request,
			HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();

		// para evirar nulo cuando no sea establecido un solicitante ya sea por
		// session o por seleccion
		if (solicitanteSeleccion == null) {
			solicitanteSeleccion = usuarioService.getUsuarioSesion().getIdUsuario();
		}

		List<UsuarioConfSolicitante> uconfigSol = uConfsolicitanteService
				.getUsuarioConfSolByIdUsuario(solicitanteSeleccion);

		String str = idElemento;
		str = str.replaceAll("[^-?0-9]+", " ");
		String idCCElementDOM = "facturaDesgloseList@.cuentaContable.idCuentaContable";
		if (tipoSolicitud == Integer
				.parseInt(parametroService.getParametroByName("idTipoSolicitudComprobacionAnticipoViaje").getValor()))
			idCCElementDOM = "comprobacionAntDesglose@.cuentaContable.idCuentaContable";

		idCCElementDOM = idCCElementDOM.replace("@", str);
		idCCElementDOM = idCCElementDOM.replaceAll("\\s+", "");
		
		  CuentaContable ccPAR = new CuentaContable();
		  if(tipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor())){
		    	
		    	/*tomar cuenta contable automatica*/
		    	Integer idCuentaContableNoND = UtilController.getIDByCuentaContable(parametroService.getParametroByName("ccPAR").getValor(),cuentaContableService.getAllCuentaContable());
		    	ccPAR = cuentaContableService.getCuentaContable(idCuentaContableNoND);	    	
		    	
		    }
		

		result.put("options", UtilController.getCuentasContablesHTML(idLocacion, uconfigSol, tipoSolicitud,ccPAR));
		result.put("idElemento", idCCElementDOM);

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	// metodo ajax para el cambio de estatus a cancelada.
	@RequestMapping(value = "/seleccionAID", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> seleccionAID(HttpSession session, @RequestParam Integer idAid,
			@RequestParam Integer idLibro, HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();
		StringBuilder row = new StringBuilder();

		// lista de conficuraciones
		List<AidConfiguracion> configs = aidConfiguracionService.getAllAidConfiguracion();
		List<AidConfiguracion> configsAux = new ArrayList<>();
		List<CategoriaMayor> catMayor = new ArrayList<>();

		// filtrar por el AID seleccionado
		if (configs != null) {
			for (AidConfiguracion conf : configs) {
				if (conf.getAid().getIdAid() == idAid && conf.getCompania().getIdcompania() == idLibro && catMayor.contains(conf.getCategoriaMayor()) == false) {
					configsAux.add(conf);
					catMayor.add(conf.getCategoriaMayor());
				}
			}
		}

		// para identificar el target del elemento en DOM para agregar las
		// opciones
		// de categoria mayor y menor dependiento de la seleccion del AID
		

		row.append("<option value=\"-1\">Seleccione:</option>");
		// generacion de HTML para desplegar en los selects.
		//if (configsAux != null && configsAux.isEmpty() == false && configsAux.size() > 1) {

			// CAT Mayor
		//	for (AidConfiguracion conf : configsAux) {
		//		row.append("<option value=\"" + conf.getCategoriaMayor().getIdCategoriaMayor() + "\">"
		//				+ conf.getCategoriaMayor().getDescripcion() + "</option>");
		//	}
		//}else if(configsAux != null && configsAux.isEmpty() == false && configsAux.size() == 1){
			
			for (AidConfiguracion conf : configsAux) {
				row.append("<option value=\"" + conf.getCategoriaMayor().getIdCategoriaMayor() + "\">"
						+ conf.getCategoriaMayor().getDescripcion() + "</option>");
			}
			
		//}

		result.put("options", row.toString());
		//result.put("idCatMayor", idCatMayor);
		//result.put("size",String.valueOf(configsAux.size()));

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	public StringBuilder getAIDSelectHTML() {

		StringBuilder row = new StringBuilder();

		return row;

	}

	// metodo ajax para el cambio de estatus a cancelada.
	@RequestMapping(value = "/seleccionCatMayor", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> seleccionCatMayor(HttpSession session, @RequestParam Integer idCatMayor,
			@RequestParam Integer idLibro, @RequestParam Integer idAid, HttpServletRequest request, HttpServletResponse response) {
		
		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();
		StringBuilder row = new StringBuilder();

		// lista de conficuraciones
		List<AidConfiguracion> configs = aidConfiguracionService.getAllAidConfiguracion();
		List<AidConfiguracion> configsAuxAID = new ArrayList<>();
		List<AidConfiguracion> configsAux = new ArrayList<>();
		List<Integer> catMenor = new ArrayList<>();

		/* solo configuraciones de ese AID */
		for (AidConfiguracion conf : configs) {
			if (conf.getAid().getIdAid() == idAid) {
				configsAuxAID.add(conf);
			}
		}
		
		// filtrar por el AID seleccionado
		if (configs != null) {
			for (AidConfiguracion conf : configsAuxAID) {
				if (conf.getCategoriaMayor().getIdCategoriaMayor() == idCatMayor && conf.getCompania().getIdcompania() == idLibro && catMenor.contains(conf.getCategoriaMenor().getIdCategoriaMenor()) == false) {
					configsAux.add(conf);
					catMenor.add(conf.getCategoriaMenor().getIdCategoriaMenor());
				}
			}
		}
				
		// para identificar el target del elemento en DOM para agregar las
		// opciones
		// de categoria mayor y menor dependiento de la seleccion del AID
		
		row.append("<option value=\"-1\">Seleccione:</option>");
		for (AidConfiguracion conf : configsAux) {
			row.append("<option value=\"" + conf.getCategoriaMenor().getIdCategoriaMenor() + "\">"
					+ conf.getCategoriaMenor().getDescripcion() + "</option>");
		}
		result.put("options", row.toString());

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	// ----------------------------------------------------------------------------------------

	// metodo ajax para el cambio de estatus a cancelada.
	@RequestMapping(value = "/seleccionaLibroContable", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> seleccionaLibroContable(HttpSession session,
			@RequestParam Integer idLibro, HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();
		StringBuilder row = new StringBuilder();

		this.idLibroSesion = idLibro;

		// OBTENER AID'S
		List<Aid> listaAID = new ArrayList<>();
		if (this.idLibroSesion != null && this.idLibroSesion > 0) {
			List<AidConfiguracion> configuration = aidConfiguracionService.getAllAidConfiguracion();
			for (AidConfiguracion conf : configuration) {
				if (conf.getCompania().getIdcompania() == idLibroSesion) {
					if (listaAID.contains(conf.getAid()) == false) {
						listaAID.add(conf.getAid());
					}
				}
			}
		}

		row.append("<option value=\"-1\">Seleccione:</option>");
		for (Aid aid : listaAID) {
			row.append("<option value=\"");
			row.append(String.valueOf(aid.getIdAid()));
			row.append("\"> " + aid.getNumeroDescripcionAid() + " </option>");
		}

		result.put("options", row.toString());

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/modalNoMercancias", method = RequestMethod.GET)
	public ModelAndView modalNoMercancias(HttpSession session, Integer id) {
		this.idUsuario = usuarioService.getUsuarioSesion().getIdUsuario();

		HashMap<String, Object> model = new HashMap<String, Object>();
		FacturaConXML facturaConXML = new FacturaConXML();
		Integer idEstadoSolicitud = 0;

		if (id == null) {
			// destruir la sesion, creada por parametro
			session.setAttribute("solicitud", null);
		}

		// configuraciones permitidas por usuario
		List<UsuarioConfSolicitante> uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
		// Obtener combos filtrador por configuracion de solicitante.
		List<Locacion> lcPermitidas = UtilController.getLocacionesPermitidasPorUsuario(uconfigSol,
				Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()),
				locacionService.getAllLocaciones(), this.idUsuario);
		List<CuentaContable> ccontable = cuentaContableService.getAllCuentaContable();
		List<CuentaContable> ccPermitidas = UtilController.getCuentasContablesPermitidasPorUsuario(uconfigSol,
				ccontable);

		// OBTENER AID'S
		List<Aid> listAids = aidService.getAllAid();

		// Obtener categoria mayor y menor
		List<CategoriaMayor> listCatMayor = categoriaMayor.getAllCategoriaMayor();
		List<CategoriaMenor> listCatMenor = categoriaMenor.getAllCategoriaMenor();

		// Obtener Companias, Proveedores y Monedas
		List<Compania> lstCompanias = companiaService.getAllCompania();
		List<Proveedor> lstProveedores = proveedorService.getAllProveedoresActivos();
		List<Moneda> lstMoneda = monedaService.getAllMoneda();

		// obtener los jefe para opcion de usuario solicitante.
		List<Usuario> lstUsuariosJefe = new ArrayList<>();
		Usuario usuario = usuarioService.getUsuario(idUsuario);
		if (usuario.getUsuario() != null) {
			lstUsuariosJefe.add(usuario.getUsuario());
		}

		Solicitud solicitud = null;
		Factura factura = null;
		List<FacturaDesglose> facturasDesglose = new ArrayList<>();
		List<FacturaDesgloseDTO> facturasDesgloseDTO = new ArrayList<>();

		// Obtener solicitud,factura y su desglose si se requiere:
		if (id != null && id > Etiquetas.CERO) {
			facturaConXML.setIdSolicitudSession(id);
			solicitud = solicitudService.getSolicitud(facturaConXML.getIdSolicitudSession());
			if (solicitud != null) {
				idEstadoSolicitud = solicitud.getEstadoSolicitud().getIdEstadoSolicitud();

				// validacion de propietario de la solicitud
				// se manda la lista de autorizadores, if null : no aplica este
				// criterio
				List<SolicitudAutorizacion> autorizadores = solicitudAutorizacionService.getAllAutorizadoresByCriterio(
						solicitud.getIdSolicitud(), Etiquetas.TIPO_CRITERIO_CUENTA_CONTABLE);
				// resolver el acceso a la solicitud actual.
				Integer puestoAP = Integer
						.parseInt(parametroService.getParametroByName("puestoAutorizacionAP").getValor());
				Integer puestoConfirmacionAP = Integer
						.parseInt(parametroService.getParametroByName("puestoConfirmacionAP").getValor());
				Integer acceso = Utilerias.validaAcceso(solicitud,
						Integer.parseInt(
								parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()),
						usuarioService.getUsuarioSesion(), autorizadores, puestoAP, puestoConfirmacionAP);
				if (acceso == Etiquetas.NO_VISUALIZAR) {
					return new ModelAndView("redirect:/");
				} else if (acceso == Etiquetas.VISUALIZAR) {
					idEstadoSolicitud = Etiquetas.SOLO_VISUALIZACION;
				}

			}
		}

		if (solicitud != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

			// si la solicitud es v๏ฟฝlida entonces crear una variable de
			// sesi๏ฟฝn
			// para el update.
			session.setAttribute("solicitud", solicitud);

			factura = solicitud.getFacturas().get(Etiquetas.CERO);
			if (factura != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

				if (factura.getCompaniaByIdCompania() != null)
					facturaConXML.setCompania(factura.getCompaniaByIdCompania());
				if (factura.getProveedor() != null) {
					facturaConXML.setProveedor(factura.getProveedor());
					facturaConXML.setRfcEmisor(factura.getProveedor().getRfc());
				}
				if (factura.getFolioFiscal() != null)
					facturaConXML.setFolioFiscal(factura.getFolioFiscal());
				facturaConXML.setFolio(factura.getFactura()); //
				if (factura.getSerieFactura() != null)
					facturaConXML.setSerie(factura.getSerieFactura());
				if (factura.getTipoFactura() != null)
					facturaConXML.setTipoFactura(factura.getTipoFactura().getIdTipoFactura());

				// retenciones
				if (factura.getConRetenciones() > Etiquetas.CERO_S) {
					facturaConXML.setConRetenciones(Etiquetas.TRUE);
					facturaConXML.setStrIsr_retenido(
							factura.getIsrRetenido() != null ? factura.getIsrRetenido().toString() : "");
					facturaConXML.setStrIva_retenido(
							factura.getIvaRetenido() != null ? factura.getIvaRetenido().toString() : "");
				} else {
					facturaConXML.setConRetenciones(Etiquetas.FALSE);
				}

				String fechaString = null;
				SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdfOut = new SimpleDateFormat("MM/dd/yyyy");

				try {
					fechaString = sdfOut.format(sdfIn.parse(factura.getFechaFactura().toString()));
					facturaConXML.setFecha_factura(fechaString);
				} catch (ParseException e) {

				}

				facturaConXML.setConcepto(solicitud.getConceptoGasto());
				
				//** Se Comenta. SF. Track Asset se Habilito en Desglose **
				//if (factura.getTrackAsset() > Etiquetas.CERO) {
				//	facturaConXML.setTrack_asset(Etiquetas.TRUE);
				//} else {
				//	facturaConXML.setTrack_asset(Etiquetas.FALSE);
				//}
				//facturaConXML.setPar(factura.getPar());
				//facturaConXML.setId_compania_libro_contable(factura.getCompaniaByIdCompaniaLibroContable());

				facturaConXML.setStrIva(factura.getIva() != null ? factura.getIva().toString() : "");
				facturaConXML.setStrIeps(factura.getIeps() != null ? factura.getIeps().toString() : "");
				facturaConXML.setStrTotal(factura.getTotal() != null ? factura.getTotal().toString() : "");
				facturaConXML.setStrSubTotal(factura.getSubtotal() != null ? factura.getSubtotal().toString() : "");

				facturaConXML.setMoneda(factura.getMoneda());
				if (solicitud.getUsuarioByIdUsuario().getIdUsuario() != solicitud.getUsuarioByIdUsuarioSolicita()
						.getIdUsuario()) {
					facturaConXML.setSolicitante(true);
					facturaConXML.setIdSolicitanteJefe(solicitud.getUsuarioByIdUsuarioSolicita().getIdUsuario());
				} else {
					facturaConXML.setSolicitante(false);
				}

				facturasDesglose = factura.getFacturaDesgloses();
				facturasDesglose = Utilerias.getSortFacturasDesglose(facturasDesglose);

				if (facturasDesglose != null && facturasDesglose.size() > Etiquetas.CERO) {
					for (FacturaDesglose fd : facturasDesglose) {
						FacturaDesgloseDTO fdd = new FacturaDesgloseDTO();
						if (fd.getAid() != null && fd.getAid().getIdAid() > Etiquetas.CERO) {
							fdd.setAid(fd.getAid());
						}
						if (fd.getCategoriaMayor() != null
								&& fd.getCategoriaMayor().getIdCategoriaMayor() > Etiquetas.CERO) {
							fdd.setCategoriaMayor(fd.getCategoriaMayor());
						}
						if (fd.getCategoriaMenor() != null
								&& fd.getCategoriaMenor().getIdCategoriaMenor() > Etiquetas.CERO) {
							fdd.setCategoriaMenor(fd.getCategoriaMenor());
						}
						fdd.setConcepto(StringEscapeUtils.unescapeJava(fd.getConcepto()));
						fdd.setCuentaContable(fd.getCuentaContable());
						fdd.setLocacion(fd.getLocacion());
						fdd.setStrSubTotal(fd.getSubtotal().toString());
						facturasDesgloseDTO.add(fdd);
					}
				}
			}
		} else {
			// mensaje de solicitud no valida a la vista.
			facturaConXML.setIdSolicitudSession(Etiquetas.CERO);
		}

		facturaConXML.setFacturaDesgloseList(facturasDesgloseDTO);

		// revisar si se trata de una modificaci๏ฟฝn para activar mensaje en la
		// vista
		if (session.getAttribute("actualizacion") != null) {
			facturaConXML.setModificacion(Etiquetas.TRUE);
			session.setAttribute("actualizacion", null);
		} else {
			facturaConXML.setModificacion(Etiquetas.FALSE);
		}

		// revisar si se trata de un nuevo registro para activar mensaje en la
		// vista
		if (session.getAttribute("creacion") != null) {
			facturaConXML.setCreacion(Etiquetas.TRUE);
			session.setAttribute("creacion", null);
		} else {
			facturaConXML.setCreacion(Etiquetas.FALSE);
		}

		// Enviar archivos anexados por solicitud:

		List<SolicitudArchivo> solicitudArchivoList = new ArrayList<>();
		if (facturaConXML.getIdSolicitudSession() > Etiquetas.CERO) {
			solicitudArchivoList = solicitudArchivoService
					.getAllSolicitudArchivoBySolicitud(facturaConXML.getIdSolicitudSession());
		}

		Usuario usuarioSession = usuarioService.getUsuarioSesion();

		model.put("facturaConXML", facturaConXML);
		model.put("lcPermitidas", lcPermitidas);
		model.put("ccPermitidas", ccPermitidas);
		model.put("listAids", listAids);
		model.put("listCatMayor", listCatMayor);
		model.put("listCatMenor", listCatMenor);
		model.put("lstCompanias", lstCompanias);
		model.put("lstProveedores", lstProveedores);
		model.put("lstMoneda", lstMoneda);
		if (facturaConXML.isSolicitante() == true
				&& facturaConXML.getIdSolicitanteJefe() == usuarioSession.getIdUsuario()) {
			lstUsuariosJefe = new ArrayList<>();
			lstUsuariosJefe.add(usuarioSession);
			model.put("lstUsuariosJefe", lstUsuariosJefe);
		} else {
			model.put("lstUsuariosJefe", lstUsuariosJefe);

		}

		model.put("estadoSolicitud", idEstadoSolicitud);
		model.put("solicitudArchivoList", solicitudArchivoList);
		model.put("tipoSolicitud",
				Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()));
		model.put("isSolicitante", usuario.getEspecificaSolicitante());
		model.put("usuarioSession", usuarioSession);

		return new ModelAndView("modalNoMercancias", model);
	}

	public List<XxloDataSourcePo> getOrdenesCompraPO(Usuario usr, Integer tipoFactura, Integer idSolicitud) {

		List<XxloDataSourcePo> ordenesCompra = new ArrayList<>();

		/* datos de XXLO_DATA_SOURCE_PO */
		List<XxloDataSourcePo> sourcePO = xxloDataSourcePoService.getOrdenesCompraRestricciones(usr, tipoFactura);

		/*
		 * TODOS LOS PO DE LA TABLA XXLO_REQUISITION_PO que corresponden al
		 * empleado
		 * 
		 */
		List<XxloRequisitionPo> listaPoRequisition = xxloRequisitionPoService.getOrdenesCompraRestricciones(usr,
				tipoFactura);

		/* Integrar los pos de la tabla XXLO_REQUISITION_PO */
		for (XxloRequisitionPo po : listaPoRequisition) {
			sourcePO.addAll(xxloDataSourcePoService.getOrdenCompra(po.getNroPo(), tipoFactura, 0));
		}

		/*
		 * Revisar si hay una solicitud para filtrar si esta solicitud es tipo
		 * orden de compra entonces revisar si tiene un recibo guardado y
		 * habilitar el listado de este.
		 */
		Factura factura = new Factura();
		if (idSolicitud != null && idSolicitud > 0) {
			factura = solicitudService.getSolicitud(idSolicitud).getFacturas().get(0);
		}

		/* Filtrar Ordenes de compra repeditos */
		List<XxloDataSourcePo> ordenesCompraFiltrados = new ArrayList<>();
		List<String> idPOs = new ArrayList<>();
		if (factura != null && factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S
				&& factura.getOrdCompra() != null) {

			// filtrar con factura orden compra
			for (XxloDataSourcePo spo : sourcePO) {
				if (!idPOs.contains(spo.getPo()) && factura.getOrdCompra().equals(spo.getPo())) {
					idPOs.add(spo.getPo());
					ordenesCompraFiltrados.add(spo);
				} else if (!idPOs.contains(spo.getPo()) && Integer.parseInt(spo.getCheckAssociation()) == 0) {
					idPOs.add(spo.getPo());
					ordenesCompraFiltrados.add(spo);
				}

			}
		} else {

			// filtrar sin factura
			for (XxloDataSourcePo spo : sourcePO) {
				if (!idPOs.contains(spo.getPo()) && Integer.parseInt(spo.getCheckAssociation()) == 0) {
					idPOs.add(spo.getPo());
					ordenesCompraFiltrados.add(spo);
				}

			}

		}

		return ordenesCompraFiltrados;

	}

	public StringBuilder construyeOptionsOrdCompra(List<XxloDataSourcePo> ordenesCompra) {

		StringBuilder ordenesC = new StringBuilder();

		List<Integer> ordenesCompraInteger = new ArrayList<>();
		for (XxloDataSourcePo po : ordenesCompra) {
			ordenesCompraInteger.add(Integer.parseInt(po.getPo()));
		}

		Collections.sort(ordenesCompraInteger);

		ordenesC.append("<option value=\"-1\">Seleccione:</option>");
		for (Integer ord : ordenesCompraInteger) {
			ordenesC.append("<option value=\"" + ord + "\">" + ord + "</option>");
		}

		return ordenesC;

	}

	// metodo ajax para listar ordenes de compra
	@RequestMapping(value = "/getPOs", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> getPOs(HttpSession session, @RequestParam Integer tipoFactura,
			@RequestParam Integer idSolicitante, @RequestParam Integer esSolicitante, @RequestParam Integer idSolicitud,
			HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();

		StringBuilder row = new StringBuilder();
		Usuario usr = new Usuario();
		if (esSolicitante == 1) {
			usr = usuarioService.getUsuario(idSolicitante);
		} else {
			Integer idUsuario = usuarioService.getUsuarioSesion().getIdUsuario();
			usr = usuarioService.getUsuario(idUsuario);
		}

		List<XxloDataSourcePo> ordenesCompraDelSolicitante = this.getOrdenesCompraPO(usr, tipoFactura, idSolicitud);
		row = this.construyeOptionsOrdCompra(ordenesCompraDelSolicitante);

		result.put("optionsOrdCompra", row.toString());

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	// checkPAR
	@RequestMapping(value = "/checkPAR", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> checkPAR(HttpSession session, @RequestParam Integer idPO,
			@RequestParam Integer idCompania, HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();

		if (idPO == -1) {
			idPO = 0;
		}

		List<XxloDataSourcePo> ordenesCompra = xxloDataSourcePoService.getPOsByIDPO(idPO.toString());
		String par = null;
		par = ordenesCompra.isEmpty() == false ? ordenesCompra.get(0).getPar() : null;

		if (par != null && par.isEmpty() == false && par.equals("NULL") == false) {
			result.put("par", par);
			result.put("compania", idCompania.toString());
		} else {
			result.put("par", "null");
			result.put("compania", "null");

		}

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	// metodo ajax para listar ordenes de compra
	@RequestMapping(value = "/getRecibos", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
		    public @ResponseBody
		    ResponseEntity<String> getRecibos(HttpSession session, @RequestParam Integer tipoFactura, @RequestParam Integer idSolicitante, @RequestParam Integer esSolicitante, @RequestParam String idPO,@RequestParam Integer idSolicitud, HttpServletRequest request, HttpServletResponse response) {
			    
		    	// limpiar lista en sesion
		    	recibosSession = new ArrayList<>();
		    	
			    String json = null;
		        HashMap<String, String> result = new HashMap<String, String>();
		        
		        StringBuilder row = new StringBuilder();
		        Usuario usr = new Usuario();
		        if(esSolicitante == 1){
			        usr = usuarioService.getUsuario(idSolicitante);
		        }else{
		        	Integer idUsuario = usuarioService.getUsuarioSesion().getIdUsuario();
		        	usr = usuarioService.getUsuario(idUsuario);
		        }
		        
		        
		        List<XxloDataSourcePo> ordenesCompraDelSolicitante = xxloDataSourcePoService.getOrdenCompra(idPO, tipoFactura, 0);
		        List<RecibosDTO> recibos = new ArrayList<>();
		        
		        /*
				   * Revisar si hay una solicitud para filtrar si esta solicitud es tipo orden de compra
				   * entonces revisar si tiene un recibo guardado y habilitar el listado de este.
				   * */
				  Factura factura = new Factura();
				  if(idSolicitud != null && idSolicitud > 0){
					  factura = solicitudService.getSolicitud(idSolicitud).getFacturas().get(0);
				  }
				  
				  if(factura != null && factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S && factura.getOrdCompra() != null){
					  
					    // filtrar recibos con factura
				        for(XxloDataSourcePo recibo : ordenesCompraDelSolicitante){
				        	if(recibo.getPo().equals(idPO) && factura.getOrdCompra().equals(idPO)){
				        		
				        		if(factura.getFacturaRecibos() != null && factura.getFacturaRecibos().isEmpty() == false){
					        		
				        			for(FacturaRecibo reciboEnFactura : factura.getFacturaRecibos()){
					        			if(reciboEnFactura.getNumRecibo().equals(recibo.getReceiptNum()) || recibo.getCheckAssociation().equals("0")){
						        		RecibosDTO dto = new RecibosDTO();
				                        dto.setRecipt_num(recibo.getReceiptNum());
				                        dto.setReceipt_num_integer(Integer.parseInt(recibo.getReceiptNum()));
				                        dto.setMontoTotal(recibo.getSubtotalIncDev().doubleValue());
				                        dto.setPo(recibo.getPo());
				                        recibos.add(dto);
					        			}
					        		}
				        			
				        		}else{
				        			if(recibo.getCheckAssociation().equals("0")){
				        				RecibosDTO dto = new RecibosDTO();
				        				dto.setRecipt_num(recibo.getReceiptNum());
				        				dto.setReceipt_num_integer(Integer.parseInt(recibo.getReceiptNum()));
				        				dto.setMontoTotal(recibo.getSubtotalIncDev().doubleValue());
				        				dto.setPo(recibo.getPo());
				        				recibos.add(dto);
				        			}
				        		}
	
				        		
				        		
		                        
				        	}else if(recibo.getPo().equals(idPO) && Integer.parseInt(recibo.getCheckAssociation()) == 0){
				        		RecibosDTO dto = new RecibosDTO();
		                        dto.setRecipt_num(recibo.getReceiptNum());
		                        dto.setReceipt_num_integer(Integer.parseInt(recibo.getReceiptNum()));
		                        dto.setMontoTotal(recibo.getSubtotalIncDev().doubleValue());
		                        dto.setPo(recibo.getPo());
		                        recibos.add(dto);
				        		
				        	}
				        }
				        
				  }else{
					    // filtrar recibos sin factura
				        for(XxloDataSourcePo recibo : ordenesCompraDelSolicitante){
				        	if(recibo.getPo().equals(idPO) && Integer.parseInt(recibo.getCheckAssociation()) == 0){
				        		RecibosDTO dto = new RecibosDTO();
		                        dto.setRecipt_num(recibo.getReceiptNum());
		                        dto.setReceipt_num_integer(Integer.parseInt(recibo.getReceiptNum()));
		                        dto.setMontoTotal(recibo.getSubtotalIncDev().doubleValue());
		                        dto.setPo(recibo.getPo());
		                        recibos.add(dto);
				        	}
				        }
				  }

		        /*recibos en factura*/
		        List<String> recibosEnFactura = new ArrayList<>();
		        if(idSolicitud != null && idSolicitud > 0){
		        	Solicitud sol = solicitudService.getSolicitud(idSolicitud);
		        	Factura fact = sol.getFacturas().get(0);
		        	if(fact.getIsOrdCompra() != null && fact.getIsOrdCompra() == Etiquetas.UNO_S){
		        		List<FacturaRecibo> recibosGuardados = facturaReciboService.getAllFacturaRecibo();
		        		for(FacturaRecibo r :recibosGuardados){
		        			if(r.getFactura().getIdFactura() == fact.getIdFactura() && recibosEnFactura.contains(r.getNumRecibo()) == false){
		        				recibosEnFactura.add(r.getNumRecibo());
		        			}
		        		}
		        	}
		        }

		        List<String> recibosPO = new ArrayList<>();	
		        
		        
		        
		        /* Filtrado de Recibos */
		        Collections.sort(recibos,(r1, r2) -> r1.getReceipt_num_integer() - r2.getReceipt_num_integer());
		        
		        List<FacturaRecibo> recibosSessionAux = new ArrayList<>();
		        recibosSessionAux.addAll(recibosSession);
		        boolean primeraVez = false;
		        
		        for(RecibosDTO recibo : recibos){
			        if(recibosPO.contains(recibo.getRecipt_num()) == false){
				        
			        	Double sumatoria = recibos.stream().filter(o -> o.getRecipt_num().equals(recibo.getRecipt_num())).mapToDouble(o -> o.getMontoTotal()).sum();
				        recibosPO.add(recibo.getRecipt_num());
				        
				        
				        // guardar en objeto en sesion los recibos actuales.
				        FacturaRecibo reciboSesion = new FacturaRecibo();
				        reciboSesion.setNumRecibo(recibo.getRecipt_num());
				        reciboSesion.setImporte(new BigDecimal(sumatoria).setScale(Etiquetas.DOS,BigDecimal.ROUND_HALF_UP));
				        reciboSesion.setImporteString(sumatoria.toString());
				        reciboSesion.setIncluido(false);
				        
				        if(this.recibosSessionPrimeraVez != null && this.recibosSessionPrimeraVez.size() > 0){
				        	
				        	for(FacturaRecibo reciboInicio : this.recibosSessionPrimeraVez){
				        		if(reciboSesion.getNumRecibo().equals(reciboInicio.getNumRecibo())){
				        			reciboSesion.setIncluido(true);
				        		 }
				        	}
				        	
				        	primeraVez = true;
		        			this.recibosSession.add(reciboSesion);

				        	
				        }else{
				        	
		        			this.recibosSession.add(reciboSesion);
				        	
				        }
				        
				        /*HTML Para Recibos */
				           String checked =  "";
				           if(recibosEnFactura.contains(recibo.getRecipt_num()) && primeraVez){
				        	   checked = "checked=\"checked\"";
				           }
				           
				           row.append("<tr>");
				           row.append(" <td style=\"text-align: center;\"><input ").append(checked).append(" class=\"recibosOrd\" value=\"").append(recibo.getRecipt_num()).append("\" onchange=\"accionRecibos(this,").append(idPO).append(")\" style=\"margin: 0px; float: left;\" type=\"checkbox\"> ").append(recibo.getRecipt_num()).append("</td>");
				           row.append(" <td style=\"text-align: center;\">$<span class=\"currencyFormatRecibo\"> ").append(sumatoria).append("</span></td>");
				           row.append("</tr>");
				        
			        }	
		        }
		        
		        if(primeraVez == true){
			        this.recibosSessionPrimeraVez = new ArrayList<>();
		        }else{
			        //this.recibosSession = new ArrayList<>();
			        //this.recibosSession.addAll(recibosSessionAux);		      
			     }

		        
			    result.put("recibos", row.toString());
			    
	          //bind json
		        ObjectMapper map = new ObjectMapper();
		        if (!result.isEmpty()) {
		            try {
		                json = map.writeValueAsString(result);
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		        
		        //respuesta
		        HttpHeaders responseHeaders = new HttpHeaders(); 
		        responseHeaders.add("Content-Type", "application/json; charset=utf-8"); 
		        return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
		    }

	// metodo ajax para listar ordenes de compra
	@RequestMapping(value = "/addRowRecibos", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> addRowRecibos(HttpSession session, @RequestParam String ordenCompra,
			@RequestParam String idRecibo, @RequestParam Integer numrows, @RequestParam Integer tipoFactura,
			@RequestParam Integer idCompania, @RequestParam Integer idSolicitud, @RequestParam Boolean track_asset, HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();
		StringBuilder row2 = new StringBuilder();
        
		List<XxloDataSourcePo> recibos = new ArrayList<>();
		
		/*hay un recibo guardado??*/
		List<String> recibosGuardadosFactura = new ArrayList<>();
		Solicitud solicitud = solicitudService.getSolicitud(idSolicitud);
		Factura factura = new Factura();
		if(solicitud != null && solicitud.getFacturas() != null && solicitud.getFacturas().isEmpty() == false){
			factura = solicitud.getFacturas().get(0);
			if(factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == 1 && factura.getFacturaRecibos() != null && factura.getFacturaRecibos().isEmpty() == false){
				for(FacturaRecibo recibo : factura.getFacturaRecibos()){
					recibosGuardadosFactura.add(recibo.getNumRecibo());
				}
			}
		}
		
		
		/*si este recibo esta guardado en esta solicitud ignorar check_association*/
		boolean ignorarReservado = false;
		if(recibosGuardadosFactura.contains(idRecibo)){
			 ignorarReservado = true;
		}
		recibos = xxloDataSourcePoService.getSKUsByRecibos(idRecibo, tipoFactura, ordenCompra, ignorarReservado);

		Integer rowcount = 1;
		for (XxloDataSourcePo recibo : recibos) {

			// String num = String.valueOf(numrows);
			// numero aleatoreo para serializar los inputs.
			int num = 0;
			/*<begin-jgh 26.02.2018> */
			int random=0;
			
			//Integer random1 = Integer.parseInt(Utilerias.getRandomNum());
			//Integer random2 = Integer.parseInt(Utilerias.getRandomNum());
			random = (int) (Math.random() * 1) + 1;
			String num_info=String.valueOf(random);
			String rowCount_info=String.valueOf(rowcount);
			String concat_random=rowCount_info+num_info;
			num=Integer.parseInt(concat_random);
			/*<end-jgh 26.02.2018> */
			
			//System.out.println("Numero Aleatorio nuevo;"+num);
			
			
			
		  //(random1 + random2);
	
			Integer numLinea = numrows + rowcount;
			String classrow = "aid_" + recibo.getReceiptNum();
			



			row2.append("<tr class=\"odd ").append(classrow).append(" gradeX\">");
			
			
			row2.append("<td align=\"center\"><div class=\"linea\">" + numLinea + "</div></td>");

			// campo oculto id
			row2.append("<td> <input id=\"facturaDesgloseList" +num+ ".id_tabla_po\" name=\"facturaDesgloseList[" +num+ "].id_tabla_po\" type=\"hidden\" value=\""+recibo.getIdXxloDataSourcePo()+"\" />");

			// numero_recibo
			row2.append("<input style=\"text-align: right;\" disabled maxlength=\"30\" id=\"facturaDesgloseList"
					+ num + ".num_recibo\" name=\"facturaDesgloseList[" + num
					+ "].num_recibo\" class=\"form-control ordCompraRow\" value=\"" + recibo.getReceiptNum()
					+ "\" type=\"text\"></td>");

			// cantidad
			row2.append("<td><input style=\"text-align: right;\" disabled maxlength=\"30\" id=\"facturaDesgloseList"
					+ num + ".cantidad\" name=\"facturaDesgloseList[" + num
					+ "].cantidad\" class=\"form-control ordCompraRow\" value=\"" + recibo.getQtyReceived()
					+ "\" type=\"text\"></td>");

			// item sku
			row2.append("<td><input disabled maxlength=\"40\" id=\"facturaDesgloseList" + num
					+ ".sku\" name=\"facturaDesgloseList[" + num
					+ "].sku\" class=\"form-control ordCompraRow\" value=\"" + recibo.getItem()
					+ "\" type=\"text\"></td>");

			// descripcion articulo
			row2.append("<td><input disabled maxlength=\"40\" id=\"facturaDesgloseList" + num
					+ ".descripcion_articulo\" name=\"facturaDesgloseList[" + num
					+ "].descripcion_articulo\" class=\"form-control ordCompraRow\" value=\"" + recibo.getDescription()
					+ "\" type=\"text\"></td>");

			// precio unitario
			row2.append("<td><input style=\"text-align: right;\" disabled maxlength=\"40\" id=\"facturaDesgloseList"
					+ num + ".precio_unitario\" name=\"facturaDesgloseList[" + num
					+ "].precio_unitario\" class=\"form-control ordCompraRow currencyFormatSKU\" value=\""
					+ recibo.getPoUnitPrice() + "\" type=\"text\"></td>");

			// subtotal
			
			String negativoPositivo = "";
			if(tipoFactura == 2){
				negativoPositivo = "-";
			}
			
			row2.append(
					"<td><input style=\"text-align: right;\" disabled step=\"any\" onblur=\"sumSbt()\" id=\"facturaDesgloseList"
							+ num + ".strSubTotal\" name=\"facturaDesgloseList[" + num
							+ "].strSubTotal\" class=\"form-control subtotales ordCompraRow currencyFormatSKU sbtGrid\" value=\""
							+negativoPositivo+ recibo.getSubtotalIncDev() + "\" type=\"text\"></td>");

			// locaciones
			Locacion locacion = locacionService.getLocacionByNum(Integer.parseInt(recibo.getLocation()));

			row2.append("<td><select disabled  id=\"facturaDesgloseList" + num
					+ ".locacion.idLocacion\" name=\"facturaDesgloseList[" + num
					+ "].locacion.idLocacion\" class=\"form-control ordCompraRow locaciones\">");
			row2.append("<option value=\"-1\">Seleccione:</option>");
			row2.append("<option selected value=\"");
			row2.append(String.valueOf(locacion.getIdLocacion()));
			row2.append("\"> " + locacion.getNumeroDescripcionLocacion() + " </option>");
			row2.append("</select>");
			row2.append("</td>");

			// cuenta contable
			CuentaContable cc = cuentaContableService.getCCByNumeroCuenta(recibo.getCuenta());

			row2.append("<td><select disabled  id=\"facturaDesgloseList" + num
					+ ".cuentaContable.idCuentaContable\" name=\"facturaDesgloseList[" + num
					+ "].cuentaContable.idCuentaContable\" class=\"form-control ccontable\">");
			row2.append("<option value=\"-1\">Seleccione:</option>");
			row2.append("<option selected value=\"");
			row2.append(String.valueOf(cc.getIdCuentaContable()));
			row2.append("\"> " + cc.getNumeroDescripcionCuentaContable() + " </option>");
			row2.append("</select>");
			row2.append("</td>");

			// AID
			Aid aid = aidService.getAidByNum(recibo.getAid());

			row2.append(
					"<td><select disabled  id=\"facturaDesgloseList" + num + ".aid.idAid\" name=\"facturaDesgloseList["
							+ num + "].aid.idAid\" class=\"form-control aidAll ordCompraRow trackAsset \">");
			row2.append("<option value=\"-1\">Seleccione:</option>");

			if (aid != null) {
				row2.append("<option selected value=\"");
				row2.append(String.valueOf(aid.getIdAid()));
				row2.append("\"> " + aid.getNumeroDescripcionAid() + " </option>");

				row2.append("</select>");
				row2.append("</td>");
			}

			String accionSeleccionCatMayor = "onChange=\"alSeleccionarCatMayor(this.id)\"";

			List<Integer> catMayorIds = new ArrayList<>();
			List<CategoriaMayor> catMayors = new ArrayList<>();

			List<Integer> catMenorIds = new ArrayList<>();
			List<CategoriaMenor> catMenors = new ArrayList<>();

			List<AidConfiguracion> config = new ArrayList<>();

			// si existe un aid
			if (aid != null) {
				config = aidConfiguracionService.getAidConfigurationByAID(aid.getIdAid());
			}

			// filtrar las configuraciones y guardar en istas las categorias
			// mayor y menos no repetidas
			for (AidConfiguracion cfg : config) {

				if (cfg.getCompania().getIdcompania() == idCompania) {

					if (!catMayorIds.contains(cfg.getCategoriaMayor().getIdCategoriaMayor())) {
						catMayorIds.add(cfg.getCategoriaMayor().getIdCategoriaMayor());
						catMayors.add(cfg.getCategoriaMayor());
					}
					if (!catMenorIds.contains(cfg.getCategoriaMenor().getIdCategoriaMenor())) {
						catMenorIds.add(cfg.getCategoriaMenor().getIdCategoriaMenor());
						catMenors.add(cfg.getCategoriaMenor());
					}

				}
			}

			boolean solo1CatMayor = false;
			CategoriaMayor catMayor = new CategoriaMayor();

			// si solo es una debe estar seleccionado y deshabilitado
			if (catMayors != null && catMayors.size() == 1) {

				solo1CatMayor = true;

				row2.append("<td><select disabled  " + accionSeleccionCatMayor + " id=\"facturaDesgloseList" + num
						+ ".categoriaMayor.idCategoriaMayor\" name=\"facturaDesgloseList[" + num
						+ "].categoriaMayor.idCategoriaMayor\" class=\"form-control ordCompraRow cMayor trackAsset\">");
				row2.append("<option value=\"-1\">Seleccione:</option>");

				for (CategoriaMayor cm : catMayors) {
					row2.append("<option  selected value=\"" + cm.getIdCategoriaMayor() + "\"> " + cm.getDescripcion()
							+ " </option>");
					catMayor = cm;
				}

			} else { // si son mas de una se queda abierto y seleccionable

				row2.append("<td><select " + accionSeleccionCatMayor + " id=\"facturaDesgloseList" + num
						+ ".categoriaMayor.idCategoriaMayor\" name=\"facturaDesgloseList[" + num
						+ "].categoriaMayor.idCategoriaMayor\" class=\"form-control ordCompraRow cMayor trackAsset\">");
				row2.append("<option value=\"-1\">Seleccione:</option>");

				for (CategoriaMayor cm : catMayors) {
					row2.append("<option  value=\"" + cm.getIdCategoriaMayor() + "\"> " + cm.getDescripcion()
							+ " </option>");
				}

			}

			row2.append("</select>");
			row2.append("</td>");

			/* CATEGORIA MENOR */

			// si es mas de 1 categoria mayor entonces se le deja para que el
			// usuario la seleccione si solo es una
			// se selecciona automaticamente y se llena el combo de categoria
			// menor.
			if (solo1CatMayor) {
				// si la lista de categorias menor solo es una se selecciona
				// automaticamente y se deshabilita
				if (catMenors != null && catMenors.size() == 1) {

					row2.append("<td><select disabled id=\"facturaDesgloseList" + num
							+ ".categoriaMenor.idCategoriaMenor\" name=\"facturaDesgloseList[" + num
							+ "].categoriaMenor.idCategoriaMenor\" class=\"form-control cMenor ordCompraRow trackAsset\">");
					row2.append("<option value=\"-1\">Seleccione:</option>");
					for (CategoriaMenor catmenor : catMenors) {
						row2.append("<option selected  value=\"" + catmenor.getIdCategoriaMenor() + "\"> "
								+ catmenor.getDescripcion() + " </option>");
					}
					row2.append("</select>");
					row2.append("</td>");

				} else {
					// si es mas de una categoria menor se deja para que se
					// seleccione.
					row2.append("<td><select  id=\"facturaDesgloseList" + num
							+ ".categoriaMenor.idCategoriaMenor\" name=\"facturaDesgloseList[" + num
							+ "].categoriaMenor.idCategoriaMenor\" class=\"form-control cMenor ordCompraRow trackAsset\">");
					row2.append("<option value=\"-1\">Seleccione:</option>");
					for (CategoriaMenor catmenor : catMenors) {
						row2.append("<option value=\"" + catmenor.getIdCategoriaMenor() + "\"> "
								+ catmenor.getDescripcion() + " </option>");
					}
					row2.append("</select>");
					row2.append("</td>");

				}

			} else {

				// si tiene mas de una se deja sin cargar cat menor ya que al
				// seleccionar se ejecuta un ajax que carga esto.

				row2.append("<td><select  id=\"facturaDesgloseList" + num
						+ ".categoriaMenor.idCategoriaMenor\" name=\"facturaDesgloseList[" + num
						+ "].categoriaMenor.idCategoriaMenor\" class=\"form-control ordCompraRow cMenor trackAsset\">");
				row2.append("<option value=\"-1\">Seleccione:</option>");
				row2.append("</select>");
				row2.append("</td>");

			}

			row2.append("</tr>");
			rowcount++;

			// seleccionar los recibos del objeto en sesion que se estan
			// agregando a la vista.
			for (FacturaRecibo recibosSession : this.recibosSession) {
				if (recibosSession.getNumRecibo().equals(recibo.getReceiptNum())) {
					recibosSession.setIncluido(true);
				}
			}

		} // for recibos

		result.put("recibos", row2.toString());

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/removeRecibo", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> removeRecibo(HttpSession session, @RequestParam String idRecibo,
			@RequestParam Integer idSolicitud, HttpServletRequest request, HttpServletResponse response) {

		// List<FacturaRecibo> recibosEnFactura = new ArrayList<>();
		// if(idSolicitud != null && idSolicitud > 0){
		// Solicitud sol = solicitudService.getSolicitud(idSolicitud);
		// recibosEnFactura =
		// facturaReciboService.getAllFacturaRecibobyFactura(sol.getFacturas().get(0).getIdFactura());
		// }
		
		facturaDesglose.clear();
		
		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();

		for (int i = 0; i < this.recibosSession.size(); i++) {
			if (this.recibosSession.get(i).getNumRecibo().equals(idRecibo)) {
				this.recibosSession.get(i).setIncluido(false);
			}
		}

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/tieneOrdenes", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> tieneOrdenes(HttpSession session, @RequestParam Integer tipoFactura,
			@RequestParam Integer idSolicitante, @RequestParam Integer esSolicitante, @RequestParam Integer idSolicitud, @RequestParam Integer idProveedor,
			HttpServletRequest request, HttpServletResponse response) {

		String json = null;
		HashMap<String, String> result = new HashMap<String, String>();

		StringBuilder row = new StringBuilder();
		Usuario usr = new Usuario();
		if (esSolicitante == 1) {
			usr = usuarioService.getUsuario(idSolicitante);
		} else {
			Integer idUsuario = usuarioService.getUsuarioSesion().getIdUsuario();
			usr = usuarioService.getUsuario(idUsuario);
		}
		
		String descripcionProveedor = proveedorService.getProveedor(idProveedor).getDescripcion();
		List<XxloDataSourcePo> ordenesCompraDelSolicitante = this.getOrdenesCompraPO(usr, tipoFactura, idSolicitud);
		boolean tieneProveedor = false;
		for(XxloDataSourcePo po : ordenesCompraDelSolicitante){
			List<XxloDataSourcePo> recibosporOrd = xxloDataSourcePoService.getPOsByIDPO(po.getPo());
			for(XxloDataSourcePo recibo : recibosporOrd){
				if(recibo.getVendorName().equals(descripcionProveedor)){
					tieneProveedor = true;
					break;
				}
			}
		}

		if (ordenesCompraDelSolicitante != null && ordenesCompraDelSolicitante.size() > 0 && tieneProveedor) {
			result.put("tieneOrdenCompra", "1");
		} else {
			result.put("tieneOrdenCompra", "0");
		}

		// bind json
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// respuesta
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	}
	
	//Validar Folio Unico (SINXML)
	@RequestMapping(value = "/validUniqFolio", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody ResponseEntity<String> validUniqFolio(HttpSession session, @RequestParam Integer id_solicitud, String folio, @RequestParam Integer proveedor_id,  HttpServletRequest request, HttpServletResponse response) {
		
		String is_valid = "true";
		
		if (Integer.parseInt(parametroService.getParametroByName("validaFacturaUnica").getValor()) == 1) {
			List<Factura> lstFactsFolio = facturaService.getFacturaByFolio(folio, proveedor_id.toString());
			if (lstFactsFolio.size() > 0) {
				for (Factura factura : lstFactsFolio) {
					if(id_solicitud != factura.getSolicitud().getIdSolicitud()) {
						is_valid = "false";
						break;
					}
				}
			}
		}
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(is_valid, responseHeaders, HttpStatus.CREATED);
	}

}
