package com.lowes.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.google.common.io.ByteStreams;
import com.lowes.dto.FacturaConXML;
import com.lowes.dto.FacturaDTO;
import com.lowes.dto.FacturaDesgloseDTO;
import com.lowes.entity.Aid;
import com.lowes.entity.CategoriaMayor;
import com.lowes.entity.CategoriaMenor;
import com.lowes.entity.Compania;
import com.lowes.entity.ComprobacionAnticipo;
import com.lowes.entity.ComprobacionDeposito;
import com.lowes.entity.CuentaContable;
import com.lowes.entity.EncabezadoEbs;
import com.lowes.entity.Factura;
import com.lowes.entity.FacturaDesglose;
import com.lowes.entity.FacturaGastoViaje;
import com.lowes.entity.FacturaKilometraje;
import com.lowes.entity.LineasEbs;
import com.lowes.entity.Locacion;
import com.lowes.entity.Moneda;
import com.lowes.entity.Proveedor;
import com.lowes.entity.ProveedorLibre;
import com.lowes.entity.Solicitud;
import com.lowes.entity.SolicitudAnticipoViaje;
import com.lowes.entity.SolicitudArchivo;
import com.lowes.entity.Usuario;
import com.lowes.entity.UsuarioConfSolicitante;
import com.lowes.entity.XxloDataSourcePo;
import com.lowes.service.AidService;
import com.lowes.service.CategoriaMayorService;
import com.lowes.service.CategoriaMenorService;
import com.lowes.service.CompaniaService;
import com.lowes.service.ComprobacionAnticipoService;
import com.lowes.service.ComprobacionDepositoService;
import com.lowes.service.CuentaContableService;
import com.lowes.service.EncabezadoEbsService;
import com.lowes.service.FacturaGastoViajeService;
import com.lowes.service.FacturaKilometrajeService;
import com.lowes.service.FacturaService;
import com.lowes.service.LineasEbsService;
import com.lowes.service.LocacionService;
import com.lowes.service.MonedaService;
import com.lowes.service.ParametroService;
import com.lowes.service.ProveedorLibreService;
import com.lowes.service.ProveedorService;
import com.lowes.service.SolicitudAnticipoViajeService;
import com.lowes.service.SolicitudArchivoService;
import com.lowes.service.SolicitudManagerService;
import com.lowes.service.SolicitudService;
import com.lowes.service.TipoFacturaService;
import com.lowes.service.UsuarioConfSolicitanteService;
import com.lowes.service.UsuarioService;
import com.lowes.service.WSValidaFacturaService;
import com.lowes.service.XxloDataSourcePoService;
import com.lowes.util.Etiquetas;
import com.lowes.util.ParseFacturaConXML;
import com.lowes.util.Utilerias;
import com.lowes.util.WSFacturaMessageResponse;


@Controller
@PreAuthorize("hasRole('ROLE_USER')")
public class UtilController {
	
	@Autowired
	private CompaniaService companiaService;
	@Autowired
	private ProveedorService proveedorService;
	@Autowired
	private MonedaService monedaService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private WSValidaFacturaService wsValidaFacturaService;
	@Autowired
	private FacturaService facturaService;
	@Autowired
	private SolicitudService solicitudService;
	@Autowired
	private EncabezadoEbsService encabezadoEbsService;
	@Autowired
	private LineasEbsService lineasEbsService;
	@Autowired
	private ParametroService parametroService;
	@Autowired
	private SolicitudManagerService solicitudManagerService;
	@Autowired
	private CategoriaMayorService categoriaMayor;
	@Autowired
	private CategoriaMenorService categoriaMenor;
	@Autowired
	private CuentaContableService cuentaContableService;
	@Autowired
	private AidService aidService;
	@Autowired
	private LocacionService locacionService;
	@Autowired
	private SolicitudArchivoService solicitudArchivoService;
	@Autowired
	private TipoFacturaService tipoFacturaService;
	@Autowired
	private FacturaKilometrajeService facturaKilometrajeService;
	@Autowired
	private ComprobacionAnticipoService comprobacionAnticipoService;
	@Autowired
	private ProveedorLibreService proveedorLibreService;
	@Autowired
	private SolicitudAnticipoViajeService solicitudAnticipoViajeService;
	@Autowired
	private ComprobacionDepositoService comprobacionDepositoService;
	@Autowired
	private FacturaGastoViajeService facturaGastoViajeService;
	@Autowired
	private XxloDataSourcePoService xxloDataSourcePoService;
	
	Etiquetas etiqueta = new Etiquetas("es");
	
	@Autowired
	ServletContext servletContext;
	
	
	private static final Logger logger = Logger.getLogger(UtilController.class);
	
	private String codigo;
	private String descripcion;
	
	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	//testing
	@RequestMapping("visorPDF")
	public ModelAndView usuarios() {
		 
		//ServletContext fullPath = servletContext.get("/com/lowes/scheduler/spring-quartz.xml");
		new ClassPathXmlApplicationContext("/com/lowes/scheduler/spring-quartz.xml");
		//new FileSystemXmlApplicationContext(fullPath);
		
		return new ModelAndView("visorPDF");
	}
	
	
	@RequestMapping("/blankx")
	public ModelAndView blank() {
		 
		
		return new ModelAndView("blank");
	}
	
	// testing
	@RequestMapping("filterSelect")
	public ModelAndView filterSelect() {
		List<Usuario> usuarios = usuarioService.getAllUsuarios();
		FacturaConXML facturaConXML = new FacturaConXML();
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		model.put("facturaConXML", facturaConXML);
		model.put("usuarios", usuarios);

		return new ModelAndView("filterSelect",model);
	}
	
	/**
	 * @author Adinfi
	 * @param MultipartHttpServletRequest request
	 * @param HttpServletResponse response
	 * @return String fecha en formato String
	 * METODO QUE EXTRAE LA INFORMACION DEL COMPROBANTE FISCAL EN FORMATO XML
	 */
	@RequestMapping(value = "/resolverXML", method = RequestMethod.POST, produces = "text/plain; charset=utf-8")
	public @ResponseBody ResponseEntity<String> readXML(MultipartHttpServletRequest request, HttpServletResponse response) {

		FacturaDTO factura = new FacturaDTO();
		HashMap<String, String> result = new HashMap<String, String>();
		String json = null;
		String responseMensaje = null;
		Integer responsecode = null;

		if (request != null) {

			Iterator<String> itr = request.getFileNames();
			MultipartFile mpf = request.getFile(itr.next());
			String contentType = mpf.getContentType();

			if (contentType.equals("text/xml")) {
				try {
					InputStream is = mpf.getInputStream();
					
					/*
					 * Clonar InputStream para no tener problemas de cerrado de "streams"
					 * 
					 * */
					
					ByteArrayOutputStream baos = Utilerias.getCloneInputStream(is);
					
					InputStream is1 = new ByteArrayInputStream(baos.toByteArray()); 
					InputStream is2 = new ByteArrayInputStream(baos.toByteArray()); 
					
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					byte [] archivo = ByteStreams.toByteArray(is1);
					Document doc = dBuilder.parse(is2);
					
					ParseFacturaConXML parse = new ParseFacturaConXML();
					factura = parse.getFactura(doc);
					
					String nombreArchivo = mpf.getOriginalFilename();
					String rfcEmisor = factura.getRfcEmisor();
					String rfcReceptor = factura.getRfcReceptor();
					String importeTotal = String.valueOf(factura.getTotal());
					String token = companiaService.getTokenCompania(rfcReceptor);
					String folio = factura.getFolio() == null || factura.getFolio().isEmpty() ? factura.getFolioFiscal() : factura.getFolio();
					String formaPago = factura.getFormaPago();
					//String metodoPago = factura.getMetodoPago();
					//BigDecimal descuento = factura.getDescuento();
					
					//Validacion CFDI
					if(Integer.parseInt(parametroService.getParametroByName("validacionCFDI").getValor()) != -1){
						//Diverza
						if(Integer.parseInt(parametroService.getParametroByName("validacionCFDI").getValor()) == 1) {
							System.out.println("Creando Conexion a Diverza");
							
							//Parametros
							String wsEndPointDiverza = parametroService.getParametroByName("wsEndPointDiverza") != null ? parametroService.getParametroByName("wsEndPointDiverza").getValor() : null;
						    
							List<WSFacturaMessageResponse> responseWS = wsValidaFacturaService.validaFactura(archivo, nombreArchivo, rfcEmisor, rfcReceptor, importeTotal, wsEndPointDiverza, token);
							
						// <BEGIN JGH 16/07/2018>
							// Se realizo la validacion cuando responseWS venga null
							if(responseWS!=null) {
								responseMensaje =  Utilerias.getMensajeWS(responseWS);
								logger.info("Mensaje de retorno Diverza: "+responseMensaje);

								
								if(responseMensaje != null){
									responsecode = responseWS.get(Etiquetas.CERO).getCode();
									logger.info("Codigo de retorno Diverza: "+responsecode);
								}
								
								if (responsecode < Etiquetas.CERO) {
									responseMensaje = "No fue posible conectarse al servicio de validacion  fiscal.";
								}
							}else {
								responseMensaje = "No fue posible conectarse al [WS].";
								responsecode=99;
							}
							
							// <END JGH 16/07/2018>
						
						}
						
						//Konesh
						if(Integer.parseInt(parametroService.getParametroByName("validacionCFDI").getValor()) == 2) {
							System.out.println("Creando Conexion a konesh");
							
							/*convertir UTFS*/
							byte ptext[] = baos.toString().getBytes("ISO-8859-1"); 
							String valuedata = new String(ptext, "UTF-8"); 
							
							//Parametros
							String wsEndPointKonesh = parametroService.getParametroByName("wsEndPointKonesh") != null ? parametroService.getParametroByName("wsEndPointKonesh").getValor() : null;
							String wsTokenKonesh = parametroService.getParametroByName("wsTokenKonesh") != null ? parametroService.getParametroByName("wsTokenKonesh").getValor() : null;
							String wsCuentaTimbradoKonesh = parametroService.getParametroByName("wsCuentaTimbradoKonesh") != null ? parametroService.getParametroByName("wsCuentaTimbradoKonesh").getValor() : null;
							String wsUsuarioTimbradoKonesh = parametroService.getParametroByName("wsUsuarioTimbradoKonesh") != null ? parametroService.getParametroByName("wsUsuarioTimbradoKonesh").getValor() : null;
							String wsPasswordTimbradoKonesh = parametroService.getParametroByName("wsPasswordTimbradoKonesh") != null ? parametroService.getParametroByName("wsPasswordTimbradoKonesh").getValor() : null;
						
							// <BEGIN 11/07/2018>
						try {
							HashMap<String,String> responseWSKonesh = wsValidaFacturaService.validaFacturaKonesh(valuedata, wsCuentaTimbradoKonesh, wsTokenKonesh, wsUsuarioTimbradoKonesh, wsPasswordTimbradoKonesh, wsEndPointKonesh, rfcReceptor);
						     if(responseWSKonesh==null) {
						    	 responsecode=-1;
						    	 responseMensaje="No se pudo conectar al WS Konesh.";
						     }
						     else {
						    		for(Map.Entry<String,String>itmaps:responseWSKonesh.entrySet()){
										responsecode=Integer.parseInt(itmaps.getKey());
										responseMensaje=itmaps.getValue();
										
										setCodigo(responsecode.toString());
										setDescripcion(responseMensaje);
										if(getCodigo().equals("0")) {
											responseMensaje ="\n" + getDescripcion() + "\n";
										}else {
											responseMensaje ="\n" +  getDescripcion() + " [" + getCodigo() + "] \n";
										}
										logger.info("Codigo de retorno Konesh: "+responseMensaje);
									} 
						     }
						
						} catch (Exception e) {
							System.out.println("No se pudo conectar al webservices:"+e.getMessage());
							
							responsecode=-1;
						}
							
						// <END 11/07/2018>
							
						
							
						}
					}else {
						responsecode = 00;
					}
					
					//Valid ?
					if(responsecode == 00 || responsecode == 34){
						
						logger.info("if L260");
						
						//Validar Factura Unica
						Factura f = new Factura();						
						f.setFolioFiscal(factura.getFolioFiscal());
						f.setFactura(folio);
						f.setSerieFactura(factura.getSerie());
						
						//Transformar rfc_emisor en proveedor
						Integer idProveedor = null;
						List<Proveedor> proveedores = proveedorService.getAllProveedoresActivos();
						for (Proveedor p : proveedores) {
							if (p.getRfc().equals(factura.getRfcEmisor())) {
								idProveedor = p.getIdProveedor();
								break;
							}
						}
						
						//Valida Factura Unica
						List<Factura> factDuplicadas = new ArrayList<>();
						if(idProveedor != null && Integer.parseInt(parametroService.getParametroByName("validaFacturaUnica").getValor()) == 1) {
							//1er Validacion: UUID
							if(f.getFolioFiscal() != null){
								List<Factura> lstFactsUUID = facturaService.getFacturaByUUID(f.getFolioFiscal());
								if(lstFactsUUID.size() > 0){
									for(Factura factItem : lstFactsUUID){
										if(solicitudService.getSolicitud(factItem.getSolicitud().getIdSolicitud()).getEstadoSolicitud().getIdEstadoSolicitud() != Integer.parseInt(parametroService.getParametroByName("idEstadoSolicitudCancelada").getValor())){
											factDuplicadas.add(factItem);
										}
									}
								}
							}
							//2da Validacion: Folio y Proveedor
							if(f.getFactura() != null){
								List<Factura> lstFactsFolio = facturaService.getFacturaByFolio(f.getFactura(), idProveedor.toString());
								if (lstFactsFolio.size() > 0) {
									for (Factura factDup : lstFactsFolio) {
										if(solicitudService.getSolicitud(factDup.getSolicitud().getIdSolicitud()).getEstadoSolicitud().getIdEstadoSolicitud() != Integer.parseInt(parametroService.getParametroByName("idEstadoSolicitudCancelada").getValor())){
											factDuplicadas.add(factDup);
										}
									}
								}
							}
						}
						
						if(factDuplicadas.size() == 0) {
							//Transformar rfc_receptor en compa๏ฟฝia
							Integer idCompania = null;
							List<Compania> companias = companiaService.getAllCompania();
							for (Compania c : companias) {
								if (c.getRfc().equals(factura.getRfcReceptor())) {
									idCompania = c.getIdcompania();
									break;
								}
							}
	
							//Transformar tipo_moneda en id moneda
							Integer idMoneda = null;
							String xmlMoneda = null;
							List<Moneda> monedas = monedaService.getAllMoneda();
							for (Moneda m : monedas) {
								String[] descMoneda = m.getDescripcionCortaVariantes().split(",");
								for(String variante : descMoneda){
									//System.out.println(variante+"->"+factura.getMoneda());
									
									// en caso de que no tenga variantes se busca el default.
									if(variante.toUpperCase().equals("DEFAULT") && factura.getMoneda() != null 
									&& m.getDescripcionCorta().toUpperCase().equals(factura.getMoneda().toUpperCase())){
										idMoneda = m.getIdMoneda();
										break;
									}
									
									if(factura.getMoneda() != null && variante.toUpperCase().equals(factura.getMoneda().toUpperCase())){
										idMoneda = m.getIdMoneda();
										break;
									}
								}
							}
							
							//Moneda es Valida ?
							if(idMoneda == null){
								result.put("xmlMoneda", xmlMoneda);
							}
							
							//Validar Concepto 240 Caracteres
							if(factura.getConceptos().length() > 240) {
								result.put("warningConceptos","true");
								factura.setConceptos(factura.getConceptos().substring(0,240));
							}
							
							String tipoFactura = Etiquetas.CERO.toString();
							if(factura.getTipoDeComprobante() != null){
								if(factura.getTipoDeComprobante().equals("ingreso") || factura.getTipoDeComprobante().equals("I")){
									tipoFactura = parametroService.getParametroByName("idTipoFactura").getValor();
								}else{
									tipoFactura = parametroService.getParametroByName("idTipoFacturaNotaCredito").getValor();
								}
							}
							
							//Si no existe el proveedor en base de datos
							if(idProveedor == null){
								result.put("validxml", "false");
								result.put("faltaProveedor", "true");
								result.put("proveedorLibre", String.valueOf(factura.getNombreEmisor()));
								
								ProveedorLibre existe = proveedorLibreService.existeProveedor(factura.getRfcEmisor());
								if(existe != null)
									result.put("idProveedorLibre", String.valueOf(existe.getIdProveedorLibre()));
								else
									result.put("idProveedorLibre", String.valueOf(agregaNuevoProveedor(factura.getNombreEmisor(),factura.getRfcEmisor()).getIdProveedorLibre()));
							}else{
								result.put("validxml", "true");
								result.put("faltaProveedor", "false");
								result.put("idProveedor", String.valueOf(idProveedor));
							}
							
						
							result.put("idCompania", String.valueOf(idCompania));
							result.put("rsFactura", idCompania != null ? String.valueOf(companiaService.getCompania(idCompania).getDescripcion()) : "0");
							result.put("idMoneda", String.valueOf(idMoneda));
							result.put("folioFiscal", factura.getFolioFiscal());
							result.put("total", String.valueOf(factura.getTotal()));
							result.put("subTotal", String.valueOf(factura.getSubTotal()));
							result.put("rfcEmisor", factura.getRfcEmisor());
							result.put("serie", factura.getSerie());
							result.put("folio", folio);
							result.put("concepto", factura.getConceptos());
							result.put("fechaEmision", factura.getFechaEmision());
							result.put("fechaPago", factura.getFechaPago());
							result.put("iva", String.valueOf(factura.getIva()));
							result.put("tua", String.valueOf(factura.getTua()));
							result.put("ish", String.valueOf(factura.getIsh()));
							result.put("yri", String.valueOf(factura.getYri()));
							result.put("otrosCargos", String.valueOf(factura.getOtrosCargos()));
							BigDecimal totalOtrosCargos = factura.getOtrosCargos().add(factura.getTua().add(factura.getIsh().add(factura.getYri())));
							result.put("otrosCargosImpuestos", String.valueOf(totalOtrosCargos));
							result.put("tasaIva", String.valueOf(factura.getTasaIva()));
							result.put("ieps", String.valueOf(factura.getIeps()));
							result.put("tasaIeps", String.valueOf(factura.getTasaIeps()));
							result.put("cantidadNeta", String.valueOf(factura.getCantidadNeta()));
							result.put("ivaRetenido", String.valueOf(factura.getIvaRetenido()));
							result.put("isrRetenido", String.valueOf(factura.getIsrRetenido()));
							// booleans
							result.put("incluyeIVA", String.valueOf(factura.isIncluyeIVA()));
							result.put("incluyeRetenciones", String.valueOf(factura.isIncluyeRetenciones()));
							// webservice
							result.put("wsmensaje",responseMensaje);
							result.put("wscode", responsecode.toString());
							//tipofactura
							result.put("tipoFactura", tipoFactura);
							
							result.put("formaPago", factura.getFormaPago());
							result.put("metodoPago", factura.getMetodoPago());
							result.put("descuento", String.valueOf(factura.getDescuento()));
							
							result.put("validxml", "true");
						}else {
							result.put("wsmensaje",etiqueta.XML_DUPLICADO);
							result.put("validxml", "false");
							result.put("faltaProveedor", "false");
						}
					}else{
						logger.info("else L398");
						result.put("validxml", "false");
						result.put("faltaProveedor", "false");
						result.put("wsmensaje",responseMensaje);
						result.put("wscode", responsecode.toString());
					}
				} catch (IOException | ParserConfigurationException | SAXException | DOMException | ParseException e) {
					e.printStackTrace();
				}

			} else {
				result.put("validxml", "false");
				result.put("faltaProveedor", "false");
				result.put("wsmensaje",etiqueta.NOXML);
			}

		}

		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	/**
	 * @author Adinfi
	 * @param Date fecha fecha en fotmato Date
	 * @return String fecha en formato String
	 */
	public String parseFecha(Date fecha){
		String fechaString = null;
		SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfOut = new SimpleDateFormat("MM/dd/yyyy");
		
		try {
			fechaString = sdfOut.format(sdfIn.parse(fecha.toString()));
		} catch (ParseException e) {
			
		}
		return fechaString;
	}
	
	@RequestMapping(value = "/validarPDF", method = RequestMethod.POST, produces = "text/plain; charset=utf-8")
	public @ResponseBody ResponseEntity<String> validarPDF(MultipartHttpServletRequest request, HttpServletResponse response) {
		
		String json = null;
		HashMap<String, Boolean> result = new HashMap<String, Boolean>();


		if (request != null) {

			Iterator<String> itr = request.getFileNames();
			MultipartFile mpf = request.getFile(itr.next());
			String contentType = mpf.getContentType();
			
			if(contentType.equals("application/pdf")){
			  result.put("valido", true);
			}else{
		      result.put("valido", false);
			}
			
		}
		
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/validarIMG", method = RequestMethod.POST, produces = "text/plain; charset=utf-8")
	public @ResponseBody ResponseEntity<String> validarIMG(MultipartHttpServletRequest request, HttpServletResponse response) {
		
		String json = null;
		HashMap<String, Boolean> result = new HashMap<String, Boolean>();


		if (request != null) {

			Iterator<String> itr = request.getFileNames();
			MultipartFile mpf = request.getFile(itr.next());
			int size = request.getContentLength();
			
			String contentType = mpf.getContentType();
			//
			if(contentType.equals("image/jpeg") || contentType.equals("image/png") ){
				if(size <= Etiquetas.MAX_FILE_SIZE){
					  result.put("valido", true);
				}else{
					  result.put("valido", false);
				}
			}else{
		      result.put("valido", false);
			}
			
		}
		
		ObjectMapper map = new ObjectMapper();
		if (!result.isEmpty()) {
			try {
				json = map.writeValueAsString(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	public boolean enviarEBS(Solicitud solicitud, Usuario usuario) {
		
		logger.debug("------------- INICIA EL ENVIO A EBS  --------------");
		
		int idTipoSolicitud = solicitud.getTipoSolicitud().getIdTipoSolicitud();
	
		// Solicitud tipo 1 -----------------------------------------------
		if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor())) {
			enviarNoMercancias(solicitud);
		}
		// Solicitud tipo 2 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor())) {
			enviarNoMercancias(solicitud);
		}
		// Solicitud tipo 3 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudReembolsos").getValor())) {
			enviarReembolsos(solicitud);	
		}	
		// Solicitud tipo 4 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudCajaChica").getValor())) {
			enviarReembolsos(solicitud);	
		}	
		// Solicitud tipo 5 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudKilometraje").getValor())) {
			enviarKilometraje(solicitud);				
		}			
		// Solicitud tipo 6 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudAnticipo").getValor())) {
			enviarAnticipo(solicitud);			
		}			
		// Solicitud tipo 7 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudComprobacionAnticipo").getValor())) {
			enviarComprobacionAnticipo(solicitud);		
		}		
		// Solicitud tipo 8 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudAnticipoGastosViaje").getValor())) {
			enviarAnticipoViaje(solicitud);			
		}
		// Solicitud tipo 9 -----------------------------------------------
		else if (idTipoSolicitud == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudComprobacionAnticipoViaje").getValor())) {
			enviarComprobacionAnticipoViaje(solicitud);			
		}
		
		return true;
	}
	
	
	// Funciones Enviar EBS -----------------------------------------------

	// [1] EBS SinXML ---------------------------------------------------------
	private void enviarNoMercancias(Solicitud solicitud) {
		
		Factura factura = solicitud.getFacturas().get(Etiquetas.CERO);
		EncabezadoEbs encabezado = new EncabezadoEbs();

		// se carga la solicitud
		encabezado.setSolicitudId(solicitud.getIdSolicitud());

		// se concatena la serie y el folio de la dactura.
		String serie = factura.getSerieFactura();
		String folio = factura.getFactura();
		StringBuilder invoiceNum = new StringBuilder();
		
		//Fecha para concatenar en INVOICE_NUM
		Date fecha = solicitud.getCreacionFecha();
		SimpleDateFormat sd1 = new SimpleDateFormat("ddMMyyyy");
		String fechaString = sd1.format(fecha);
		
		//Si Folio biene vacio se agrega UUID - Folio Fiscal
		
		//<BEGIN JGH-21.06.2018>
		
		// validacion cuando obtenga un null agregamos vacio al string
		serie = serie!= null ? serie : "";
		folio =folio!= null ? folio : "";
		String quito_espacios_serie=serie.trim();
		String quito_espacios_folio=folio.trim();
		
		if(quito_espacios_folio.isEmpty()) {
			quito_espacios_folio = factura.getFolioFiscal();
		}
		
		
		//cuando no tiene serie ni folio agrega vacio.
		if(quito_espacios_serie.isEmpty() && quito_espacios_folio.isEmpty()) {
			invoiceNum.append("");
			// cuando tiene serie y folio concatena
		}else if(quito_espacios_folio.length()>0 && quito_espacios_serie.length()>0) {
			invoiceNum.append(quito_espacios_serie).append(" - ").append(quito_espacios_folio);
			//cuando solamente tenga serie
		}else if(quito_espacios_serie.length()>0) {
			invoiceNum.append(quito_espacios_serie);
			//cuando solamente tenga folio
		}else if(quito_espacios_folio.length()>0){
			invoiceNum.append(quito_espacios_folio);
			
		}
		
		//<END JGH-21.06.2018>
		
		/*if(folio == null || folio.isEmpty()) {
			folio = factura.getFolioFiscal();
		}
		
		//Concatena
		if(serie != null) {
			invoiceNum.append(serie).append(" - ").append(folio);
		}else {
			invoiceNum.append(folio);
		}*/
		encabezado.setInvoiceNum(invoiceNum.toString());
		
        // descripcion EBS
		encabezado.setInvoiceTypeLookupCode(solicitud.getFacturas().get(Etiquetas.CERO).getTipoFactura().getDescripcionEbs());

		/* fecha de la factura unica de solicitud.*/
		encabezado.setInvoiceDate(factura.getFechaFactura());
					
		// numero del proveedor de la factura.
		encabezado.setVendorNum(String.valueOf(factura.getProveedor().getNumeroProveedor()));

		
		/*
		 * 
		 * Actualmente NO se tiene un criterio de validaci๏ฟฝn para el env๏ฟฝo
		 * del valor de Importe Total a la tabla EBS, es decir, el sistema
		 * env๏ฟฝa a EBS el valor de Importe Total = Total (6)
		 * 
		 * Se est๏ฟฝ solicitando incluir un criterio de validaci๏ฟฝn para
		 * evaluar si el importe Total se env๏ฟฝa a EBS calculado o como
		 * actualmente se env๏ฟฝa, dando como resultado los siguientes
		 * criterios: Criterio 1: Si en alguno de los campos de IVA Retenido
		 * (4) o ISR Retenido (5) > 0, el valor a enviar a EBS ser๏ฟฝ: Importe
		 * Total a enviar a EBS =๏ฟฝSubtotal (1) + IVA (2) + IEPS (3)
		 * 
		 * Criterio 2: Si en los campos de IVA Retenido (4) e ISR Retenido
		 * (5) = 0, el valor a enviar a EBS ser๏ฟฝ: Importe Total a enviar a
		 * EBS =๏ฟฝTotal (6)
		 * 
		 * Nota1: El c๏ฟฝlculo del segundo criterio NO se ve afectado, es
		 * decir, se env๏ฟฝa lo que hoy se est๏ฟฝ enviando. Nota2: Los n๏ฟฝmeros
		 * entre par๏ฟฝntesis hacen referencia a la Imagen 1 del documento
		 */
		
		// monto total de la solicitud
		if(factura.getTipoFactura().getIdTipoFactura() == Integer.parseInt(parametroService.getParametroByName("idTipoFacturaNotaCredito").getValor())){
		
			if(solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor())){
				if ((solicitud.getFacturas().get(0).getIvaRetenido() != null && solicitud.getFacturas().get(0).getIvaRetenido().compareTo(BigDecimal.ZERO) > 0)	|| (solicitud.getFacturas().get(0).getIsrRetenido() != null && solicitud.getFacturas().get(0).getIsrRetenido().compareTo(BigDecimal.ZERO) > 0)) {
					encabezado.setInvoiceAmount(solicitud.getFacturas().get(0).getSubtotal().add(solicitud.getFacturas().get(0).getIva()).add(solicitud.getFacturas().get(0).getIeps()).negate());
				}else{
					encabezado.setInvoiceAmount(solicitud.getMontoTotal().negate());
				}
			}else{
				encabezado.setInvoiceAmount(solicitud.getMontoTotal().negate());
			}
			
			
		}else{
			
			if(solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor())){
				if ((solicitud.getFacturas().get(0).getIvaRetenido() != null && solicitud.getFacturas().get(0).getIvaRetenido().compareTo(BigDecimal.ZERO) > 0)	|| (solicitud.getFacturas().get(0).getIsrRetenido() != null && solicitud.getFacturas().get(0).getIsrRetenido().compareTo(BigDecimal.ZERO) > 0)) {
					encabezado.setInvoiceAmount(solicitud.getFacturas().get(0).getSubtotal().add(solicitud.getFacturas().get(0).getIva()).add(solicitud.getFacturas().get(0).getIeps()));
				}else{
					encabezado.setInvoiceAmount(solicitud.getMontoTotal());
				}
			}else{
				encabezado.setInvoiceAmount(solicitud.getMontoTotal());
			}
			
		}

		// formato de moneda
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());
		
		// track asset Control de cambios # 166
		//if (factura.getTrackAsset() > Etiquetas.CERO_S) {
		//	/* Description Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		//	ArrayList<String> tokens = new ArrayList<String>();
		//	tokens.add(etiqueta.PAR + ": " + factura.getPar());
		//	if(solicitud.getConceptoGasto().length() > 240){
		//		tokens.add(solicitud.getConceptoGasto().substring(0,240));
		//	}else {
		//		tokens.add(solicitud.getConceptoGasto());
		//	}
		//	String descripcion = buildDescriptionEBS(tokens, 1);
		//	encabezado.setDescription(descripcion);
		//} else {
			// concepto de la solicitud
			if(solicitud.getConceptoGasto().length() > 240){
				encabezado.setDescription(solicitud.getConceptoGasto().substring(0,240));
			}else{
				encabezado.setDescription(solicitud.getConceptoGasto());
			}
		//}

		// folio fiscal de la factura
		encabezado.setAttribute15(factura.getFolioFiscal());

		// poner el status que es NEW para todas las solicitudes enviadas
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());

		// etiquetaOrigenEBS
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());
        
		// metodo de pago
		encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());

		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());
		
		
		/*SI ES UNA ORDEN DE COMPRA*/
		if(factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S){
			encabezado.setPo(factura.getOrdCompra());
			encabezado.setVendorSiteCode(factura.getVendorSiteCode());
		}
		

		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		
		// metodo para agregar las lineas
	    addLineasEBS(solicitud, id);
	}

	// [3] EBS Reembolsos -----------------------------------------------------
	private void enviarReembolsos(Solicitud solicitud) {
		
		// solicitudes: Reembolsos y caja chica.
		EncabezadoEbs encabezado = new EncabezadoEbs();

		// se carga la solicitud
		encabezado.setSolicitudId(solicitud.getIdSolicitud());

		// fecha de la solicitud en formato ddmmaaa
		Date fecha = solicitud.getCreacionFecha();
		SimpleDateFormat sd1 = new SimpleDateFormat("ddMMyyyy");
		
		String date = sd1.toString().replaceAll("-","");
		
		StringBuilder invoiceNum = new StringBuilder();
		invoiceNum.append(sd1.format(fecha));
		encabezado.setInvoiceNum(invoiceNum.toString()+" - "+solicitud.getIdSolicitud());

		//Descripcion EBS -  para reembolsos y caja chica es stadard default
		//encabezado.setInvoiceTypeLookupCode(tipoFacturaService.getTipoFactura(Etiquetas.TRES).getDescripcionEbs());
		encabezado.setInvoiceTypeLookupCode(solicitud.getFacturas().get(Etiquetas.CERO).getTipoFactura().getDescripcionEbs());


		// fecha de la factura unica de solicitud.
		encabezado.setInvoiceDate(solicitud.getCreacionFecha());
		
		// numero del usuario solicitante.
		Integer vendorNum = null;
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudReembolsos").getValor())) {
			vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
			}
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudCajaChica").getValor())) {
			vendorNum = solicitud.getUsuarioByIdUsuarioAsesor().getNumeroProveedor();
			}
		
		if(vendorNum!= null)
			encabezado.setVendorNum(vendorNum.toString());
		else
			encabezado.setVendorNum(null);

		// monto total de la solicitud
		encabezado.setInvoiceAmount(solicitud.getMontoTotal());

		// formato de moneda
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());

		// concepto de la solicitud
		if(solicitud.getConceptoGasto().length() > 240){
			encabezado.setDescription(solicitud.getConceptoGasto().substring(0,240));
		}else{
			encabezado.setDescription(solicitud.getConceptoGasto());
		}

		// folio fiscal de la factura (en este tipo de solicitudes va
		// vacio.)
		encabezado.setAttribute15(null);

		// poner el status que es NEW para todas las solicitudes enviadas
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());

		// etiquetaOrigenEBS
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());

		// metodo de pago.
		encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());

		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());

		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		
		// metodo para agregar las lineas
		addLineasEBS(solicitud, id);
	}	
	
	// [5] EBS Kilometraje ----------------------------------------------------
	private void enviarKilometraje(Solicitud solicitud) {
		
		String fecha = "";
		fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		
		// solicitudes: Reembolsos y caja chica.
		EncabezadoEbs encabezado = new EncabezadoEbs();
		/* 1: se carga la solicitud */
		encabezado.setSolicitudId(solicitud.getIdSolicitud());
		/* 2: InvoiceNum */
		String fechaNumber = fecha.replace("/", "");
		StringBuilder invoiceNum = new StringBuilder();
		invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" - ").append(fechaNumber).append(" - ").append(solicitud.getIdSolicitud());
		encabezado.setInvoiceNum(invoiceNum.toString());
		/* 3: TypeLookupCode */
		encabezado.setInvoiceTypeLookupCode(solicitud.getTipoSolicitud().getDescripcionInvoiceTypeEBS());
		/* 4: fecha de la factura unica de solicitud. */
		encabezado.setInvoiceDate(solicitud.getCreacionFecha());
		/* 5: numero de proveedor del empleado. */
		Integer vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
		if(vendorNum != null)
			encabezado.setVendorNum(vendorNum.toString());
		else
			encabezado.setVendorNum(null);
		/* 6: monto total de la solicitud */
		encabezado.setInvoiceAmount(solicitud.getMontoTotal());
		/* 7: formato de moneda */
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());
		/* 8: Descripci๏ฟฝn de la solicitud */
		Usuario usuario = solicitud.getUsuarioByIdUsuario();
		String name = usuario.getNombreCompletoUsuario();
		String descripcion = solicitud.getTipoSolicitud().getPrefijoDescriptionEBS()+" / "+name;
		encabezado.setDescription(descripcion);
		/* 9: folio fiscal de la factura (en este tipo de solicitudes va vacio.) en documento dice NULL*/
		encabezado.setAttribute15(null);
		/* 10: poner el status que es NEW para todas las solicitudes enviadas */
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());
		/* 11: etiquetaOrigenEBS */
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());
		/* 12: metodo de pago. */
		encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());
		/* 13: metodo de pago. */
		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());
		
		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		
		// metodo para agregar las lineas
		addLineasEBS(solicitud, id);
	}	
	
	// [6] EBS Anticipo -------------------------------------------------------
	private void enviarAnticipo(Solicitud solicitud) {
		String fecha = "";
		fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		// solicitudes: Anticipo.
		EncabezadoEbs encabezado = new EncabezadoEbs();

		/* 1: se carga la solicitud */
		encabezado.setSolicitudId(solicitud.getIdSolicitud());
		
		/* 2: InvoiceNum */
		//PR - DDMMYYYY - ID_SOLICITUD
		String fechaNumber = fecha.replace("/", "");
		StringBuilder invoiceNum = new StringBuilder();
		invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" - ").append(fechaNumber).append(" - ").append(solicitud.getIdSolicitud());
		encabezado.setInvoiceNum(invoiceNum.toString());
		/* 3: TypeLookupCode */
		encabezado.setInvoiceTypeLookupCode(solicitud.getTipoSolicitud().getDescripcionInvoiceTypeEBS());
		/* 4: Fecha de la factura unica de solicitud. */
		encabezado.setInvoiceDate(solicitud.getCreacionFecha());
		/* 5: Numero de asesor/proveedor */
		Integer vendorNum = null;
		String prooveedorDescripcion;
		
		
		/*
		 * ASESOR:
		   CONCEPTO COMPLETO / NOMBRE_ASESOR
		   
		   PROVEEDOR:
		   CONCEPTO_COMPLETO / NOMBRE_PROVEEDOR 
		*/
		
		if(solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_ASESOR){
			//vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
			Usuario usuario = solicitud.getUsuarioByIdUsuarioAsesor();
			prooveedorDescripcion = usuario.getNombreCompletoUsuario();
		}else{
			//vendorNum = solicitud.getProveedor().getNumeroProveedor();
			prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
		}
		
		vendorNum = solicitud.getProveedor().getNumeroProveedor();
				
		if(vendorNum != null && vendorNum > 0)
			encabezado.setVendorNum(vendorNum.toString());
		else
			encabezado.setVendorNum(null);
		
		/* 6: monto total de la solicitud */
		encabezado.setInvoiceAmount(solicitud.getMontoTotal());
		/* 7: Formato de moneda */
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());
		/* 8: Descripci๏ฟฝn de la solicitud		
		   Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getConceptoGasto());
		tokens.add(prooveedorDescripcion);
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 0);
		encabezado.setDescription(descripcion);
		/* 9: Folio fiscal de la factura (en este tipo de solicitudes va vacio.) */
		encabezado.setAttribute15(null);
		/* 10: Poner el status que es NEW para todas las solicitudes enviadas */
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());
		/* 11: etiquetaOrigenEBS */
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());
		/* 12: metodo de pago. */
		encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());
		/* 13: Org ID */
		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());
		
		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		// metodo para agregar las lineas
		addLineasEBS(solicitud, id);
	}

	// [7] EBS Comprobaci๏ฟฝn Anticipo ------------------------------------------
	private void enviarComprobacionAnticipo(Solicitud solicitud) {
		// Generar encabezado
		
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		
		Integer vendorNum;
		String prooveedorDescripcion;
		
		/*
		 * ASESOR:
		   CONCEPTO COMPLETO / NOMBRE_ASESOR
		   
		   PROVEEDOR:
		   CONCEPTO_COMPLETO / NOMBRE_PROVEEDOR 
		*/
		
		if(solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_ASESOR){
			//vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
			Usuario usuario = solicitud.getUsuarioByIdUsuarioAsesor();
			prooveedorDescripcion = usuario.getNombreCompletoUsuario();
		}else{
			//vendorNum = solicitud.getProveedor().getNumeroProveedor();
			prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
		}
		
		vendorNum = solicitud.getProveedor().getNumeroProveedor();
		
		// solicitudes: ComprobacionAnticipo.
		EncabezadoEbs encabezado = new EncabezadoEbs();
		
		/* 1: se carga la solicitud */
		encabezado.setSolicitudId(solicitud.getIdSolicitud());
		/* 2: InvoiceNum */
		//PR COMP - DDMMYYYY - ID_SOLICITUD
		String fechaNumber = fecha.replace("/", "");
		StringBuilder invoiceNum = new StringBuilder();
		invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" COMP - ").append(fechaNumber).append(" - ").append(solicitud.getIdSolicitud());
		encabezado.setInvoiceNum(invoiceNum.toString());

		/* 3: TypeLookupCode */
		encabezado.setInvoiceTypeLookupCode(solicitud.getTipoSolicitud().getDescripcionInvoiceTypeEBS());
		/* 4: fecha de la factura unica de solicitud. */
		encabezado.setInvoiceDate(solicitud.getCreacionFecha());
		/* 5: numero de asesor/proveedor */
		if(vendorNum!= null)
			encabezado.setVendorNum(vendorNum.toString());
		else
			encabezado.setVendorNum(null);
		/* 6: Monto total de la solicitud */
		encabezado.setInvoiceAmount(solicitud.getMontoTotal());
		/* 7: formato de moneda */
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());
		/* 8: Descripci๏ฟฝn de la solicitud. Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getTipoSolicitud().getPrefijoDescriptionEBS());
		tokens.add(solicitud.getConceptoGasto());
		tokens.add(prooveedorDescripcion);
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 1);
		encabezado.setDescription(descripcion);
		/* 9: Folio fiscal de la factura (en este tipo de solicitudes va vacio.)*/
		encabezado.setAttribute15(null);
		/* 10: Poner el status que es NEW para todas las solicitudes enviadas */
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());
		/* 11: etiquetaOrigenEBS */
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());
		/* 12: Metodo de pago. */
		encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());
		/* 13: Org ID */
		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());
		
		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		
		enviarComprobacionAnticipoDeposito(encabezado, solicitud);
		
		// metodo para agregar las lineas
		addLineasEBS(solicitud, id);
		
		
		
	}	
	
	// [7.1] EBS ComprobacionAnticipoDeposito ---------------------------------------------------------
		private void enviarComprobacionAnticipoDeposito(EncabezadoEbs encabezado, Solicitud solicitud) {
			// Generar encabezado
			List<ComprobacionDeposito> depositos = comprobacionDepositoService.getAllComprobacionDepositoBySolicitud(encabezado.getSolicitudId());
//			for (ComprobacionDeposito deposito : depositos) {
			ComprobacionDeposito deposito = new ComprobacionDeposito();
			if(depositos != null && !depositos.isEmpty()){
				deposito = depositos.get(0);
				
				String fecha = Utilerias.convertDateFormat(deposito.getCreacionFecha());
				String fechaDepositoAnticipo;
				Solicitud anticipoGastoViaje;
				
				ComprobacionAnticipo relacionCompAnt = comprobacionAnticipoService.getAnticiposByComprobacion(encabezado.getSolicitudId()).get(0);
				//Carga el anticipo
				anticipoGastoViaje = relacionCompAnt.getSolicitudByIdSolicitudAnticipo();
				
				fechaDepositoAnticipo = Utilerias.convertDateFormat(anticipoGastoViaje.getFechaPago());
				
				/* 2: InvoiceNum */
				String fechaNumber = fecha.replace("/", "");
				StringBuilder invoiceNum = new StringBuilder();
				invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append("  ").append(fechaNumber).append("  ").append(parametroService.getParametroByName("etiquetaInvoiceCompDepositoEBS") != null ? parametroService.getParametroByName("etiquetaInvoiceCompDepositoEBS").getValor(): null);
				encabezado.setInvoiceNum(invoiceNum.toString());
				/* 4: fecha de la factura unica de solicitud. */
				encabezado.setInvoiceDate(deposito.getCreacionFecha());
				/* 6: Monto total de la solicitud */
				encabezado.setInvoiceAmount(deposito.getMontoDeposito());
				/* 8: Descripci๏ฟฝn de la solicitud. Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
				ArrayList<String> tokens = new ArrayList<String>();
				tokens.add(parametroService.getParametroByName("etiquetaFichaDepositoEBS") != null ? parametroService.getParametroByName("etiquetaFichaDepositoEBS").getValor() : null);
				tokens.add(Etiquetas.F_DEL + fechaNumber);
				//Tokens + Index de la descripci๏ฟฝn.
				String descripcion = buildDescriptionEBS(tokens, 1);
				encabezado.setDescription(descripcion);
				/* 15: PrepayNum */
//				encabezado.setPrepayNum(null);
				/* 16: Prepay Apply Amount */
//				encabezado.setPrepayApplyAmount(null);
				
				Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
			}
				//logger("deposito EBS: " + id);
//			}
		}
	
	// [8] EBS AnticipoViaje ---------------------------------------------------------
	private void enviarAnticipoViaje (Solicitud solicitud) {
		
		String fecha = "";
		fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		
		// solicitudes: Anticipo.
		EncabezadoEbs encabezado = new EncabezadoEbs();

		/* 1: se carga la solicitud */
		encabezado.setSolicitudId(solicitud.getIdSolicitud());
		/* 2: InvoiceNum */
		String fechaNumber = fecha.replace("/", "");
		StringBuilder invoiceNum = new StringBuilder();
		invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" - ").append(fechaNumber).append(" - ").append(solicitud.getIdSolicitud());
		encabezado.setInvoiceNum(invoiceNum.toString());
		/* 3: Descripcion EBS */
		encabezado.setInvoiceTypeLookupCode(solicitud.getTipoSolicitud().getDescripcionInvoiceTypeEBS());
		/* 4: Fecha de la factura unica de solicitud. */
		encabezado.setInvoiceDate(solicitud.getCreacionFecha());
		/* 5: Numero de asesor/proveedor */
//		Integer vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
		Integer vendorNum = null;
		if(solicitud.getProveedor() != null){
			vendorNum = solicitud.getProveedor().getNumeroProveedor();
		}
		if(vendorNum!= null)
			encabezado.setVendorNum(vendorNum.toString());
		else
			encabezado.setVendorNum(null);
		/* 6: Monto total de la solicitud */
		encabezado.setInvoiceAmount(solicitud.getMontoTotal());
		/* 7: Formato de moneda */
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());
		/* 8: Descripci๏ฟฝn de la solicitud 
		Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getConceptoGasto());
		tokens.add(solicitud.getLocacion().getNumeroDescripcionLocacion());
		tokens.add(String.valueOf(solicitudAnticipoViajeService.getSolicitudAnticipoViajeBySolicitud(solicitud.getIdSolicitud()).getNumPersonas()));
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 0);
		encabezado.setDescription(descripcion);
		/* 9: Folio fiscal de la factura (en este tipo de solicitudes va vacio.) */
		encabezado.setAttribute15(null);
		// 10: Poner el status que es NEW para todas las solicitudes enviadas
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());
		// 11: etiquetaOrigenEBS
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());
		// 12: Metodo de pago.
		if(solicitud.getFormaPago() != null)
			encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());
			
		// 13: Org ID
		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());
		// 14: Workflow
		// 15: Terms
		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
//		
		// metodo para agregar las lineas
		addLineasEBS(solicitud, id);
	}
	
	// [9] EBS ComprobacionAnticipoViaje ---------------------------------------------------------
	private void enviarComprobacionAnticipoViaje(Solicitud solicitud) {		
		// Generar encabezado
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		Integer vendorNum;
		Solicitud anticipoGastoViaje;
		
		//Carga relacion anticipo/comprobacion
		boolean isReembolso = false;
		ComprobacionAnticipo relacionCompAnt = comprobacionAnticipoService.getAnticiposByComprobacion(solicitud.getIdSolicitud()).get(0);
		//Carga el anticipo
		anticipoGastoViaje = relacionCompAnt.getSolicitudByIdSolicitudAnticipo();
		//identifica si tiene anticipo, o se gener๏ฟฝ sin anticipo (si el id de comprobacion es igual al del anticipo, entonces entro sin anticipo)
		if(relacionCompAnt.getSolicitudByIdSolicitudAnticipo().getIdSolicitud() == relacionCompAnt.getSolicitudByIdSolicitudComprobacion().getIdSolicitud())
			isReembolso = true;
		
		// Getting proveedor
		vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
		
		// solicitudes: ComprobacionAnticipo.
		EncabezadoEbs encabezado = new EncabezadoEbs();
		
		/* 1: se carga la solicitud */
		encabezado.setSolicitudId(solicitud.getIdSolicitud());
		
		/* 2: InvoiceNum */
		//TE COMP - DDMMYYYY - ID_SOLICITUD
		String fechaNumber = fecha.replace("/", "");
		StringBuilder invoiceNum = new StringBuilder();
		//invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" COMP - ").append(fechaNumber).append(" - ").append(parametroService.getParametroByName("etiquetaCompEBS") !=null ? parametroService.getParametroByName("etiquetaCompEBS").getValor():null);
		if(isReembolso){
			invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append("COMP ").append(parametroService.getParametroByName("etiquetaDescriptionReembolsoEBS").getValor()).append(" - ").append(fechaNumber).append(" - ").append(solicitud.getIdSolicitud());
		}else{
			invoiceNum.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append("COMP - ").append(fechaNumber).append(" - ").append(solicitud.getIdSolicitud());
		}
		
		encabezado.setInvoiceNum(invoiceNum.toString());
		
		/* 3: TypeLookupCode */
		encabezado.setInvoiceTypeLookupCode(solicitud.getTipoSolicitud().getDescripcionInvoiceTypeEBS());
		/* 4: fecha de la factura unica de solicitud. */
		encabezado.setInvoiceDate(solicitud.getCreacionFecha());
		/* 5: numero de asesor/proveedor */
		if(vendorNum!= null)
			encabezado.setVendorNum(vendorNum.toString());
		else
			encabezado.setVendorNum(null);
		/* 6: Monto total de la solicitud */
		encabezado.setInvoiceAmount(solicitud.getMontoTotal());
		/* 7: formato de moneda */
		encabezado.setInvoiceCurrencyCode(solicitud.getMoneda().getDescripcionCorta());
		/* 8: Descripci๏ฟฝn de la solicitud. Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
//		if(isReembolso)
//			tokens.add(parametroService.getParametroByName("etiquetaDescriptionReembolsoEBS") !=null ? parametroService.getParametroByName("etiquetaDescriptionReembolsoEBS").getValor():null);
//		else
//			tokens.add(solicitud.getTipoSolicitud().getPrefijoDescriptionEBS());
		
		tokens.add(solicitud.getConceptoGasto());
		tokens.add(solicitud.getLocacion().getNumeroDescripcionLocacion());
		
		List<ComprobacionAnticipo> anticiposDeComprobacion = comprobacionAnticipoService.getAnticiposByComprobacion(solicitud.getIdSolicitud());
		ComprobacionAnticipo anticipoComprobacion = anticiposDeComprobacion.get(0);
		SolicitudAnticipoViaje sav = solicitudAnticipoViajeService.getSolicitudAnticipoViajeBySolicitud(anticipoComprobacion.getSolicitudByIdSolicitudAnticipo().getIdSolicitud());
		
		String numeroPersonas = "0";
		if(sav != null){
			numeroPersonas = String.valueOf(sav.getNumPersonas());
		}
		tokens.add(numeroPersonas);
		
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 0);
		encabezado.setDescription(descripcion);
		/* 9: Folio fiscal de la factura (en este tipo de solicitudes va vacio.)*/
		encabezado.setAttribute15("");
		/* 10: Poner el status que es NEW para todas las solicitudes enviadas */
		encabezado.setStatus(parametroService.getParametroByName("etiquetaEstatusEBS").getValor());
		/* 11: etiquetaOrigenEBS */
		encabezado.setSource(parametroService.getParametroByName("etiquetaOrigenEBS").getValor());
		/* 12: Metodo de pago. */
		encabezado.setPaymentMethodLookupCode(solicitud.getFormaPago().getDescripcionEbs());
		/* 13: Org ID */
		encabezado.setOrgId(solicitud.getCompania().getIdOrganizacion());
		// 14: Workflow
		// 15: Terms
//		/* 15: PrepayNum */
//		String fechaPrepayNum = Utilerias.convertDateFormat(anticipoGastoViaje.getCreacionFecha());
//		StringBuilder PrepayNum = new StringBuilder();
//		PrepayNum.append(anticipoGastoViaje.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" - ").append(fechaPrepayNum.replace("/", "")).append(" - ").append(anticipoGastoViaje.getIdSolicitud());
//		encabezado.setPrepayNum(PrepayNum.toString());
//		/* 16: Prepay Apply Amount */
//		encabezado.setPrepayApplyAmount(solicitud.getMontoTotal());
		
		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		
		if(comprobacionDepositoService.getAllComprobacionDepositoBySolicitud(solicitud.getIdSolicitud()).size() > 0){
			enviarComprobacionAnticipoViajeDeposito(encabezado);
		}
		
		// metodo para agregar las lineas
		addLineasEBS(solicitud, id);
	}
	
	// [9.1] EBS ComprobacionAnticipoViajeDeposito ---------------------------------------------------------
	private void enviarComprobacionAnticipoViajeDeposito(EncabezadoEbs encabezado) {
		// Generar encabezado
		ComprobacionDeposito deposito = comprobacionDepositoService.getAllComprobacionDepositoBySolicitud(encabezado.getSolicitudId()).get(0);
		String fecha = Utilerias.convertDateFormat(deposito.getCreacionFecha());
		String fechaDepositoAnticipo;
		Solicitud anticipoGastoViaje;
		
		ComprobacionAnticipo relacionCompAnt = comprobacionAnticipoService.getAnticiposByComprobacion(encabezado.getSolicitudId()).get(0);
		//Carga el anticipo
		anticipoGastoViaje = relacionCompAnt.getSolicitudByIdSolicitudAnticipo();
		
		fechaDepositoAnticipo = Utilerias.convertDateFormat(anticipoGastoViaje.getFechaPago());
		String fechaCreacionAnticipo = Utilerias.convertDateFormat(anticipoGastoViaje.getCreacionFecha());
		/* 2: InvoiceNum */
		String fechaNumber = fecha.replace("/", "");
		StringBuilder invoiceNum = new StringBuilder();
		invoiceNum.append(anticipoGastoViaje.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append(" DEPOSITO ").append(fechaNumber);
		encabezado.setInvoiceNum(invoiceNum.toString());
		/* 6: Monto total de la solicitud */
		encabezado.setInvoiceAmount(deposito.getMontoDeposito());
		/* 8: Descripci๏ฟฝn de la solicitud. Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add("FICHA DEPOSITO ANTICIPO " + anticipoGastoViaje.getIdSolicitud());
		tokens.add("DEL " + fechaCreacionAnticipo.replace("/", ""));
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 1);
		encabezado.setDescription(descripcion);
		// 14: Workflow
		// 15: Terms
		/* 15: PrepayNum */
//		encabezado.setPrepayNum(null);
		/* 16: Prepay Apply Amount */
//		encabezado.setPrepayApplyAmount(null);
		
		Integer id = encabezadoEbsService.createEncabezadoEbs(encabezado);
		//logger("deposito EBS: " + id);
	}
			
	// Fin funciones Enviar EBS -------------------------------------------

	private void addLineasEBS(Solicitud solicitud, Integer encabezadoID) {
		
		//encabezadoID = 36;

		// nomercancias
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasSinXML").getValor()) || solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor())) {

			Factura factura = solicitud.getFacturas().get(Etiquetas.CERO);

			// guardar lineas por desglose de la solicitud
			int lineaItem = Etiquetas.UNO;
			if (factura.getFacturaDesgloses() != null && factura.getFacturaDesgloses().isEmpty() == false) {
				for (FacturaDesglose desglose : factura.getFacturaDesgloses()) {
					
					LineasEbs linea = new LineasEbs();
					// carga de linea con informacion base para todas las lineas
					linea = loadLineaEBSNoMercancias(solicitud, encabezadoID);
					// set especifico de datos para cada linea
					linea.setLineNumber(lineaItem);
					linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
					
					// monto total de la solicitud - Control de cambios 23/06/2016 - miguelr # 172
					if(factura.getTipoFactura().getIdTipoFactura() == Integer.parseInt(parametroService.getParametroByName("idTipoFacturaNotaCredito").getValor()))
						linea.setAmount(desglose.getSubtotal().negate());
					else
						linea.setAmount(desglose.getSubtotal());
						//Tax Code
						if(factura.getIva() != null) {
							linea.setTaxCode("IVA " + Utilerias.convertTaxToInteger(factura.getPorcentajeIva()) + "% A");
						}else {
							linea.setTaxCode("IVA EXEMPT A");
						}
					
					// track asset Control de cambios # 166
					//if (factura.getTrackAsset() > Etiquetas.CERO_S) {
					//	/* Description Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
					//	ArrayList<String> tokens = new ArrayList<String>();
					//	String aid = desglose.getAid() != null ? desglose.getAid().getAid() : "";
					//	tokens.add(etiqueta.PAR + ": " + factura.getPar() + " " + etiqueta.AID + ": " + aid);
					//	
					//    if(factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S){
					//	  tokens.add(desglose.getDescripcionSku());
					//    }else{
					//	  tokens.add(desglose.getConcepto());
					//    }
					//	
					//	String descripcion = buildDescriptionEBS(tokens, 1);
					//	linea.setDescription(descripcion);
					//} else {
						
						 if(factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S){
							  linea.setDescription(desglose.getDescripcionSku());
						    }else{
								if(desglose.getConcepto().length() > 240){
									linea.setDescription(desglose.getConcepto().substring(0, 240));
								}else{
									linea.setDescription(desglose.getConcepto());
								}
						    }
						
					//}
					
					String locacionNumero = String.valueOf(desglose.getLocacion().getNumero());
					// contenacion de
					// numerodecompa๏ฟฝia-numerodelocacion-00-numerocuentacontable.
				    if(factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S){
				    	
                         linea.setDistCodeConcatenated(null);
				    	
				    }else{
						linea.setDistCodeConcatenated(
								factura.getCompaniaByIdCompania().getNumeroCompania().toString() + "-" + locacionNumero
										+ "-" + "00-" + desglose.getCuentaContable().getNumeroCuentaContable().toString()+ "-0000");
				    	
				    }
					
					// si tiene track as asset entonces cargamos cat mayor y
					// menor
					if (linea.getAssetsTrackingFlag().equals(parametroService.getParametroByName("etiquetaSiEBS").getValor())) {
						linea.setAssetCategoryIdMajor(desglose.getCategoriaMayor() != null ? desglose.getCategoriaMayor().getDescripcion() : "");
						linea.setAssetCategoryIdMinor(desglose.getCategoriaMenor() != null ? desglose.getCategoriaMenor().getDescripcion() : "");
					}
					
					
					/* ORDENES COMPRA */
					
				    if(factura.getIsOrdCompra() != null && factura.getIsOrdCompra() == Etiquetas.UNO_S){
				    	
				    	
				    	linea.setReceiptNum(desglose.getNumRecibo());
				    	XxloDataSourcePo lineaEnTablaOrdenCompra = xxloDataSourcePoService.getXxloDataSourcePo(desglose.getIdxxloDataSourceID());
				    	if(lineaEnTablaOrdenCompra != null){
				    		
				    		linea.setPoReleaseId(lineaEnTablaOrdenCompra.getPoReleaseId());
				    		linea.setLineNum(lineaEnTablaOrdenCompra.getLineNum());
				    		linea.setShipmentNum(lineaEnTablaOrdenCompra.getShipmentNum());
				    		linea.setDistributionNum(lineaEnTablaOrdenCompra.getDistributionNum());
				    		linea.setTransactionId(lineaEnTablaOrdenCompra.getTransactionId());
				    		
				    	}
				    	
				    }
					
					
					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
				}
			}

		} else if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudReembolsos").getValor()) || solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudCajaChica").getValor())) {
			
			// Reembolsos / Caja Chica.
			if (solicitud.getFacturas() != null && solicitud.getFacturas().isEmpty() == false) {

				//Set Linea Item
				int lineaItem = Etiquetas.UNO;
				BigDecimal totalIva = new BigDecimal(0);
				BigDecimal porcentajeTotalIva = new BigDecimal(0);
				BigDecimal totalIvaRetenido = new BigDecimal(0);
				BigDecimal ieps = new BigDecimal(0);
				BigDecimal porcentajeIeps = new BigDecimal(0);
				BigDecimal isrRetenido = new BigDecimal(0);
				
				//Items
				for (Factura factura : solicitud.getFacturas()) {	
					LineasEbs linea = new LineasEbs();
					linea = loadLineaReembolsosCajaChica(factura, encabezadoID, lineaItem, solicitud);
					
					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
					
					//IVA +
					if(factura.getIva() != null) {
						totalIva = totalIva.add(factura.getIva());
						porcentajeTotalIva = factura.getPorcentajeIva();
					}
					//IVA Retenido +
					if(factura.getIvaRetenido() != null) totalIvaRetenido = totalIvaRetenido.add(factura.getIvaRetenido());
					//IEPS +
					if(factura.getIeps() != null) {
						ieps = ieps.add(factura.getIeps());
						porcentajeIeps = factura.getPorcentajeIeps();
					}
					//ISR +
					if(factura.getIsrRetenido() != null) isrRetenido = isrRetenido.add(factura.getIsrRetenido());
				}
				
				//IVA
				if (totalIva.compareTo(BigDecimal.ZERO) > 0) {
					
					LineasEbs linea = new LineasEbs();
					linea.setFacturaId(solicitud.getFacturas().get(0).getIdFactura());
					linea.setInvoiceId(encabezadoID);
					linea.setLineNumber(lineaItem);
					linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaTaxEBS").getValor());
					linea.setAmount(totalIva);
					if(porcentajeTotalIva.compareTo(BigDecimal.ZERO) > 0) {
						linea.setTaxCode("IVA " + Utilerias.convertTaxToInteger(porcentajeTotalIva) + "% A");
					}else {
						linea.setTaxCode(null);
					}
					linea.setDescription("Linea de IVA");

					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
				}
				
				//IVA Retenido
				if (totalIvaRetenido.compareTo(BigDecimal.ZERO) > 0) {
					LineasEbs linea = new LineasEbs();
					linea.setFacturaId(solicitud.getFacturas().get(0).getIdFactura());
					linea.setInvoiceId(encabezadoID);
					linea.setLineNumber(lineaItem);
					linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
					linea.setAmount(totalIvaRetenido.negate());
					linea.setTaxCode(null);
					linea.setDescription("Linea de IVA Retenido");

					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
				}
				
				//IEPS
				if (ieps.compareTo(BigDecimal.ZERO) > 0) {
					LineasEbs linea = new LineasEbs();
					linea.setFacturaId(solicitud.getFacturas().get(0).getIdFactura());
					linea.setInvoiceId(encabezadoID);
					linea.setLineNumber(lineaItem);
					linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaTaxEBS").getValor());
					linea.setAmount(ieps);
					if(porcentajeIeps.compareTo(BigDecimal.ZERO) > 0) {
						linea.setTaxCode(Utilerias.convertTaxToInteger(porcentajeIeps) + "% IEPS A");
					}else {
						linea.setTaxCode(null);
					}
					linea.setDescription("Linea de IEPS");

					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
				}
				
				//ISR
				if (isrRetenido.compareTo(BigDecimal.ZERO) > 0) {
					LineasEbs linea = new LineasEbs();
					linea.setFacturaId(solicitud.getFacturas().get(0).getIdFactura());
					linea.setInvoiceId(encabezadoID);
					linea.setLineNumber(lineaItem);
					linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
					linea.setAmount(isrRetenido);
					linea.setTaxCode(null);
					linea.setDescription("Linea de ISR Retenido");

					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
				}
			}
		}
		// Kilometraje
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudKilometraje").getValor())) {
			int lineaItem = Etiquetas.UNO;
			List<FacturaKilometraje> facturaKmLst = facturaKilometrajeService.getAllFacturaKilometrajeByIdSolicitud(solicitud.getIdSolicitud());
			for(FacturaKilometraje factKm : facturaKmLst){
				LineasEbs linea = loadLineaKilometraje(encabezadoID, lineaItem, solicitud, factKm);
				lineasEbsService.createLineasEbs(linea);
				lineaItem ++;
			}
		}
		// Anticipo
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudAnticipo").getValor())) {
			int lineaItem = Etiquetas.UNO;
			LineasEbs linea = loadLineaAnticipo(encabezadoID, lineaItem, solicitud);
			lineasEbsService.createLineasEbs(linea);
		}
		
		// Comprobacion anticipo
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudComprobacionAnticipo").getValor())) {
			
			int lineaItem = Etiquetas.UNO;
			if(proveedorService.isAsesor(solicitud.getProveedor())){
				for(Factura factura : solicitud.getFacturas()){
					LineasEbs linea = loadLineaComprobacionAnticipo(factura, null, encabezadoID, lineaItem, solicitud);
					lineasEbsService.createLineasEbs(linea);
					lineaItem++;
				}
			}else{
				Factura factura = solicitud.getFacturas().get(Etiquetas.CERO);
				if (factura.getFacturaDesgloses() != null && factura.getFacturaDesgloses().isEmpty() == false) {
					for (FacturaDesglose desglose : factura.getFacturaDesgloses()) {
						LineasEbs linea = loadLineaComprobacionAnticipo(factura, desglose, encabezadoID, lineaItem, solicitud);
						lineasEbsService.createLineasEbs(linea);
						lineaItem++;
					}
				}
				
			}
			//Linea de la comprobacion del Deposito
			Factura factura = solicitud.getFacturas().get(Etiquetas.CERO);
			if (factura.getFacturaDesgloses() != null) {
				List<ComprobacionDeposito> depositos = comprobacionDepositoService.getAllComprobacionDepositoBySolicitud(encabezadoID);
				ComprobacionDeposito deposito = new ComprobacionDeposito();
				if(depositos != null && !depositos.isEmpty()){
					deposito = depositos.get(0);
					LineasEbs linea = loadLineaComprobacionAnticipoDeposito(factura, null, encabezadoID, lineaItem, solicitud, deposito);
					lineasEbsService.createLineasEbs(linea);
				}
			}
		}
		
		// Anticipo Viaje
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudAnticipoGastosViaje").getValor())) {
			int lineaItem = Etiquetas.UNO;
			LineasEbs linea = loadLineaAnticipoViaje(encabezadoID, lineaItem, solicitud);
			lineasEbsService.createLineasEbs(linea);
		}
		
		// Comprobacion Anticipo Viaje
		if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudComprobacionAnticipoViaje").getValor())) {	
			int lineaItem = Etiquetas.UNO;
			List<Factura> facturaLst = facturaService.getAllFacturaBySolicitud(solicitud.getIdSolicitud());
			for(Factura fact : facturaLst){
				LineasEbs linea = loadLineaComprobacionGastoViaje(encabezadoID, lineaItem, solicitud, fact);
				lineasEbsService.createLineasEbs(linea);
				lineaItem ++;
			}
		}
		
	}

	// metodo para cargar una linea con la informaci๏ฟฝn general.
	// [1,2] No Mercancias ----------------------------------------------------------------
	private LineasEbs loadLineaEBSNoMercancias(Solicitud solicitud, Integer encabezadoID) {

		LineasEbs linea = new LineasEbs();
		Factura factura = solicitud.getFacturas().get(Etiquetas.CERO);

		linea.setFacturaId(factura.getIdFactura());
		linea.setInvoiceId(encabezadoID);
		linea.setAttribute3(factura.getProveedor().getRfc());
		linea.setAttribute4(factura.getFactura());
		linea.setAttribute5(factura.getFolioFiscal());
		linea.setOrgId(factura.getCompaniaByIdCompania().getIdOrganizacion());

		// track asset ?
		//if (factura.getTrackAsset() > Etiquetas.CERO_S) {
		//	linea.setAssetsTrackingFlag(parametroService.getParametroByName("etiquetaSiEBS").getValor());
		// 	linea.setAssetBookTypeCode(factura.getCompaniaByIdCompania().getDescripcionEbs());
		//} else {
		//	linea.setAssetsTrackingFlag(parametroService.getParametroByName("etiquetaNoEBS").getValor());
		//}

		return linea;
	}
	
	// [3,4] Reembolsos y caja chica ----------------------------------------------------------------
	private LineasEbs loadLineaReembolsosCajaChica(Factura factura, Integer encabezadoID, int lineaItem, Solicitud solicitud){
		
		LineasEbs linea = new LineasEbs();
		
		linea.setFacturaId(factura.getIdFactura());
		linea.setInvoiceId(encabezadoID);
		linea.setLineNumber(lineaItem);
		linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
		linea.setAmount(factura.getSubtotal());
		//TaxCode
		if(factura.getIva() != null) {
			linea.setTaxCode("IVA " + Utilerias.convertTaxToInteger(factura.getPorcentajeIva()) + "% A");
		}
		
		// description
		String rfc = (factura.getProveedor() != null) ? factura.getProveedor().getRfc() : "";
		if(rfc.equals("")){
			rfc = (factura.getProveedorLibre() != null) ? factura.getProveedorLibre().getRfc() : null; 
		}
		
		if(solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudReembolsos").getValor())){
			
			
			if (solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudReembolsos").getValor())) {
				Proveedor proveedor = factura.getProveedor();
				String razonSocial = "";
				String descripcion = "";
//				Integer NumFactura = factura.getIdFactura(); // Cambio Secomenta para enviar numero factura enves de id
				String numFactura = factura.getFactura();

				if (proveedor != null) {
					razonSocial = proveedor.getDescripcion();
					descripcion = razonSocial+" / "+numFactura+" / "+solicitud.getConceptoGasto();
				}
				else {
					if(factura.getProveedorLibre() != null){
						ProveedorLibre proveedorLibre =  factura.getProveedorLibre();
						razonSocial = proveedorLibre.getDescripcion();
						descripcion = razonSocial+" / "+numFactura+" / "+solicitud.getConceptoGasto();
					}else{
						descripcion = numFactura+" / "+solicitud.getConceptoGasto();
					}
					
				}
			
			 linea.setDescription(descripcion.length() > 240 ? descripcion.substring(0,240) : descripcion);
			}
			
		
			
			//linea.setDescription(rfc + "-" + factura.getFactura() + "-"	+ solicitud.getConceptoGasto());
		}else if(solicitud.getTipoSolicitud().getIdTipoSolicitud() == Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudCajaChica").getValor())){
			
			String descripcion = "";
			if(factura.getProveedor() != null || factura.getProveedorLibre() != null){
				
				if(factura.getProveedor() != null && factura.getProveedor().getDescripcion() != null){
					descripcion = factura.getProveedor().getDescripcion() + " / " + factura.getFactura() + " / "	+ factura.getConceptoGasto();
				}else if(factura.getProveedorLibre() != null && factura.getProveedorLibre().getDescripcion() != null){
					descripcion = factura.getProveedorLibre().getDescripcion() + " / " + factura.getFactura() + " / "	+ factura.getConceptoGasto();
				}else{
					descripcion = factura.getFactura() + " / "	+ factura.getConceptoGasto();
				}
				
			}else{
				descripcion = factura.getFactura() + " / "	+ factura.getConceptoGasto();
			}
			
			linea.setDescription(descripcion.length() > 240 ? descripcion.substring(0,240) : descripcion);
		}
		
		
		String locacionNumero = "0";
		if(factura.getLocacion() != null){
		  locacionNumero = String.valueOf(factura.getLocacion().getNumero());
		}
		// contenacion de
		//COMPANIA.NUMERO_COMPANIA &"-"& LOCACION.NUMERO &"-"& "00" &"-"& CUENTA_CONTABLE.NUMERO_CUENTA_CONTABLE &"-0000"
		linea.setDistCodeConcatenated(factura.getCompaniaByIdCompania().getNumeroCompania().toString() + "-" + locacionNumero + 
			"-" + "00-" + factura.getCuentaContable().getNumeroCuentaContable().toString() + "-0000");

		linea.setAttribute3(rfc);
		linea.setAttribute4(factura.getFactura());
		linea.setAttribute5(factura.getFolioFiscal());
		/* ORG_ID */
		linea.setOrgId(factura.getCompaniaByIdCompania().getIdOrganizacion());
		linea.setAssetsTrackingFlag(parametroService.getParametroByName("etiquetaNoEBS").getValor());
		
		return linea;
		
	}
	
	// [5] Kilometraje ----------------------------------------------------------------
	private LineasEbs loadLineaKilometraje(Integer encabezadoID, int lineaItem, Solicitud solicitud, FacturaKilometraje factKm){
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		String cuenta = parametroService.getParametroByName("ccNoDeducible").getValor();
		Integer idOrganizacion = solicitud.getCompania().getIdOrganizacion();
		String companiaNum = solicitud.getCompania().getNumeroCompania();
		Integer locacion = solicitud.getLocacion().getNumero();
		
		LineasEbs linea = new LineasEbs();
		/* 1: Invoice ID */
		linea.setInvoiceId(encabezadoID);
		/* 2: LineNumber */
		linea.setLineNumber(lineaItem);
		/* 3: TypeLookupCode */
		linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
		/* 4: Importe de recorrido */
		linea.setAmount(factKm.getImporte());
		/* 5: Descripci๏ฟฝn de la solicitud */
		String origen = factKm.getKilometrajeRecorrido().getKilometrajeUbicacionByIdOrigen().getDescripcion();
		String destino = factKm.getKilometrajeRecorrido().getKilometrajeUbicacionByIdDestino().getDescripcion();
		String motivo = factKm.getMotivo();
		String kmFecha = Utilerias.convertDateFormat(factKm.getKilometrajeFecha()).replace("/", "");
		
		//DDMMYYYY - ORIGEN - DESTINO - TEXTO_MOTIVO
		String descripcion = kmFecha + " - " + origen + " - " + destino + " - " + motivo;
		
		if(descripcion.length() > 240){
			linea.setDescription(descripcion.substring(0, 240));
		}else{
			linea.setDescription(descripcion);
		}
		/* 6: TaxCode */
		linea.setTaxCode(null);
		
		/* 7: DistCodeConcatenated */
		//NUM_COMPAัIA - NUM_LOCACION - 00 - CUENTA_CONTABLE - 0000
		String concatenated = companiaNum+"-"+locacion+"-00-"+cuenta+"-0000";
		
		linea.setDistCodeConcatenated(concatenated);
		/* 8: Attributo 3 */
		linea.setAttribute3(solicitud.getUsuarioByIdUsuarioSolicita().getRfc());
		/* 9: Attributo 4 */	
		String atributo4 = "";
		String fechaNumber = fecha.replace("/", "");
		//atributo4 = solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()+" "+fechaNumber+" "+solicitud.getIdSolicitud();
		linea.setAttribute4(atributo4);
		/* 10: Attributo 5 */	
		linea.setAttribute5(null);
		/* 11: ORG_ID */
		linea.setOrgId(idOrganizacion);
		/* 12,13,14,15: TAX CODE */
		linea.setAssetsTrackingFlag("N");
		linea.setAssetBookTypeCode(null);
		linea.setAssetCategoryIdMajor(null);
		linea.setAssetCategoryIdMinor(null);
		
		return linea;
	}
	
	// [6] Anticipo -------------------------------------------------------------------
	private LineasEbs loadLineaAnticipo(Integer encabezadoID, int lineaItem, Solicitud solicitud) {
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		String cuenta = parametroService.getParametroByName("ccAnticipo").getValor();
		Integer idOrganizacion = solicitud.getCompania().getIdOrganizacion();
		String companiaNum = solicitud.getCompania().getNumeroCompania();
		Integer locacion = solicitud.getLocacion().getNumero();
		String rfcProveedor;
		String prooveedorDescripcion;
		
		/*
		 * ASESOR:
		   CONCEPTO COMPLETO / NOMBRE_ASESOR
		   
		   PROVEEDOR:
		   CONCEPTO_COMPLETO / NOMBRE_PROVEEDOR 
		*/
		
		if(solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_ASESOR){
			//vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
			Usuario usuario = solicitud.getUsuarioByIdUsuarioAsesor();
			prooveedorDescripcion = usuario.getNombreCompletoUsuario();
			rfcProveedor = solicitud.getUsuarioByIdUsuarioAsesor().getRfc();

		}else{
			//vendorNum = solicitud.getProveedor().getNumeroProveedor();
			rfcProveedor = solicitud.getProveedor().getRfc();
			prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
		}
		
		//prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
		
		LineasEbs linea = new LineasEbs();

		/* 1: Invoice ID */
		linea.setInvoiceId(encabezadoID);
		/* 2: LineNumber */
		linea.setLineNumber(lineaItem);
		/* 3: TypeLookupCode */
		linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
		/* 4: Amount */
		linea.setAmount(solicitud.getMontoTotal());
		/* 5: Descripci๏ฟฝn de la solicitud
		Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto /
		nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getConceptoGasto());
		tokens.add(prooveedorDescripcion);
		String descripcion = buildDescriptionEBS(tokens, 0);
		linea.setDescription(descripcion);
		/* 6: TAX code */
		linea.setTaxCode(null);
		
		/* 7: Concatenated (dist code)*/
		//NUM_COMPAัIA - NUM_LOCACION - 00 - CUENTA_CONTABLE - 0000
		String concatenated = companiaNum+"-"+locacion+"-00-"+cuenta+"-0000";
		linea.setDistCodeConcatenated(concatenated);
		
		/* 8: Attribute 3 */
		linea.setAttribute3(rfcProveedor);
		/* 9: Attributo 4 */
		String atributo4 = "";
		//String fechaNumber = fecha.replace("/", "");
	    //atributo4 = solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS() + "  " + fechaNumber;
		linea.setAttribute4(atributo4);
		/* 10: Attributo 5 */
		linea.setAttribute5(null);
		/* 11: Org ID */
		linea.setOrgId(idOrganizacion);
		/* 12, 13, 14, 15 Assets */
		linea.setAssetsTrackingFlag("N");
		linea.setAssetBookTypeCode(null);
		linea.setAssetCategoryIdMajor(null);
		linea.setAssetCategoryIdMinor(null);

		return linea;
	}
		
	// [7] Comprobar anticipo ---------------------------------------------------------
	private LineasEbs loadLineaComprobacionAnticipo(Factura factura, FacturaDesglose desglose, Integer encabezadoID, Integer lineaItem, Solicitud solicitud) {
		LineasEbs linea = new LineasEbs();
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		// numero de asesor/proveedor
		String prooveedorDescripcion;
		
		/*
		 * ASESOR:
		   CONCEPTO COMPLETO / NOMBRE_ASESOR
		   
		   PROVEEDOR:
		   CONCEPTO_COMPLETO / NOMBRE_PROVEEDOR 
		*/
		
		if(solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_ASESOR){
			//vendorNum = solicitud.getUsuarioByIdUsuarioSolicita().getNumeroProveedor();
			Usuario usuario = solicitud.getUsuarioByIdUsuarioAsesor();
			prooveedorDescripcion = usuario.getNombreCompletoUsuario();
		}else{
			//vendorNum = solicitud.getProveedor().getNumeroProveedor();
			prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
		}
				
		
		/* 1: Factura ID */
		linea.setFacturaId(factura.getIdFactura());
		/* 2: Invoice ID */
		linea.setInvoiceId(encabezadoID);
		/* 3: LineNumber */
		linea.setLineNumber(lineaItem);
		/* 4: TypeLookupCode */
		String lineTypeLookupCode = parametroService.getParametroByName("etiquetaItemEBS").getValor();
//		if(solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_ASESOR){
//			parametroService.getParametroByName("etiquetaItemEBS").getValor();
//		}else if(solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_PROVEEDOR){
//			parametroService.getParametroByName("etiquetaTaxEBS").getValor();
//		}
		linea.setLineTypeLookupCode(lineTypeLookupCode);
		/* 5: Amount */
		if(desglose != null)
			linea.setAmount(desglose.getSubtotal());
		else
			linea.setAmount(factura.getSubtotal());
		/* 6: Descripci๏ฟฝn de la solicitud Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getTipoSolicitud().getPrefijoDescriptionEBS() + " ANTICIPO");
		if(desglose != null)
			tokens.add(desglose.getConcepto());
		else
			tokens.add(solicitud.getConceptoGasto());
		tokens.add(prooveedorDescripcion);
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 1);
		linea.setDescription(descripcion);
		/* 7: TaxCode */
		if(factura.getIva() != null) {
			linea.setTaxCode("IVA " + Utilerias.convertTaxToInteger(factura.getPorcentajeIva()) + "% A");
		}else {
			linea.setTaxCode("IVA EXEMPT A");
		}
		
		String locacionNumero = "";
		if(desglose != null)
			locacionNumero = String.valueOf(desglose.getLocacion().getNumero());
		else
			locacionNumero = String.valueOf(factura.getLocacion().getNumero());
		/* 8: Concatenated */
		if(desglose != null)
			linea.setDistCodeConcatenated(factura.getCompaniaByIdCompania().getNumeroCompania().toString() + "-" + locacionNumero+ "-" + "00-" + desglose.getCuentaContable().getNumeroCuentaContable().toString()+ "-0000");
		else
			linea.setDistCodeConcatenated(factura.getCompaniaByIdCompania().getNumeroCompania().toString() + "-" + locacionNumero+ "-" + "00-" + factura.getCuentaContable().getNumeroCuentaContable().toString()+ "-0000");
		/* 9: Atributo 3 */
		linea.setAttribute3(factura.getProveedor().getRfc());
		/* 10: Atributo 4 */
		String atributo4 = "";
		String fechaNumber = fecha.replace("/", "");
		atributo4 = solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS() + "  " + fechaNumber;
		linea.setAttribute4(atributo4);
//		linea.setAttribute4(factura.getFactura());
		/* 11: Atributo 5 */
		linea.setAttribute5(factura.getFolioFiscal());
		/* 12: Org ID */
		linea.setOrgId(factura.getCompaniaByIdCompania().getIdOrganizacion());
		/* Otros */
		linea.setAssetsTrackingFlag("N");
		if (linea.getAssetsTrackingFlag().equals(parametroService.getParametroByName("etiquetaSiEBS").getValor())) {
			linea.setAssetCategoryIdMajor(desglose.getCategoriaMayor() != null ? desglose.getCategoriaMayor().getDescripcion() : "");
			linea.setAssetCategoryIdMinor(desglose.getCategoriaMenor() != null ? desglose.getCategoriaMenor().getDescripcion() : "");
		}else {
			linea.setAssetBookTypeCode(null);
			linea.setAssetCategoryIdMajor(null);
			linea.setAssetCategoryIdMinor(null);
		}
//		lineasEbsService.createLineasEbs(linea);
//		lineaItem++;

		return linea;
	}
	
	// [7.1] Comprobar anticipo Deposito---------------------------------------------------------
		private LineasEbs loadLineaComprobacionAnticipoDeposito(Factura factura, FacturaDesglose desglose, Integer encabezadoID, Integer lineaItem, Solicitud solicitud, ComprobacionDeposito deposito) {
				LineasEbs linea = new LineasEbs();
			
				String fecha = Utilerias.convertDateFormat(deposito.getCreacionFecha());
				String fechaNumber = fecha.replace("/", "");
				
//				String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
				// numero de asesor/proveedor
				String prooveedorDescripcion;
				if(proveedorService.isAsesor(solicitud.getProveedor())){
					prooveedorDescripcion = solicitud.getUsuarioByIdUsuario().getNombreCompletoUsuario();
				}else{
					prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
				}
				
				/* 1: Factura ID */
				linea.setFacturaId(factura.getIdFactura());
				/* 2: Invoice ID */
				linea.setInvoiceId(encabezadoID);
				/* 3: LineNumber */
				linea.setLineNumber(lineaItem);
				/* 4: TypeLookupCode */
				String lineTypeLookupCode = parametroService.getParametroByName("etiquetaItemEBS").getValor();
				linea.setLineTypeLookupCode(lineTypeLookupCode);
				/* 5: Amount */
				linea.setAmount(solicitud.getMontoTotal());
				/* 6: Descripci๏ฟฝn de la solicitud Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto / nombre"] */
				ArrayList<String> tokens = new ArrayList<String>();
				tokens.add(parametroService.getParametroByName("etiquetaFichaDepositoEBS") != null ? parametroService.getParametroByName("etiquetaFichaDepositoEBS").getValor() : null);
				tokens.add(Etiquetas.F_DEL + fechaNumber);
				//Tokens + Index de la descripci๏ฟฝn.
				String descripcion = buildDescriptionEBS(tokens, 1);
				linea.setDescription(descripcion);
				/* 7: TaxCode */
				linea.setTaxCode(null);
				String locacionNumero = "";
				if(desglose != null)
					locacionNumero = String.valueOf(desglose.getLocacion().getNumero());
				else
					locacionNumero = String.valueOf(factura.getLocacion().getNumero());
				/* 8: Concatenated */
				if(desglose != null)
					linea.setDistCodeConcatenated(factura.getCompaniaByIdCompania().getNumeroCompania().toString() + "-" + locacionNumero+ "-" + "00-" + desglose.getCuentaContable().getNumeroCuentaContable().toString()+ "-0000");
				else
					linea.setDistCodeConcatenated(factura.getCompaniaByIdCompania().getNumeroCompania().toString() + "-" + locacionNumero+ "-" + "00-" + factura.getCuentaContable().getNumeroCuentaContable().toString()+ "-0000");
				/* 9: Atributo 3 */
				linea.setAttribute3(factura.getProveedor().getRfc());
				/* 10: Atributo 4 */
//				String atributo4 = "";
//				String fechaNumber = fecha.replace("/", "");
				StringBuilder atributo4 = new StringBuilder();
				atributo4.append(solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()).append("  ").append(fechaNumber).append("  ").append(parametroService.getParametroByName("etiquetaInvoiceCompDepositoEBS") != null ? parametroService.getParametroByName("etiquetaInvoiceCompDepositoEBS").getValor(): null);
//				atributo4 = solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS() + "  " + fechaNumber + ;
				linea.setAttribute4(atributo4.toString());
//				linea.setAttribute4(factura.getFactura());
				/* 11: Atributo 5 */
				linea.setAttribute5(null);
				/* 12: Org ID */
				linea.setOrgId(factura.getCompaniaByIdCompania().getIdOrganizacion());
				/* Otros */
				linea.setAssetsTrackingFlag("N");
				if (linea.getAssetsTrackingFlag().equals(parametroService.getParametroByName("etiquetaSiEBS").getValor())) {
					linea.setAssetCategoryIdMajor(desglose.getCategoriaMayor() != null ? desglose.getCategoriaMayor().getDescripcion() : "");
					linea.setAssetCategoryIdMinor(desglose.getCategoriaMenor() != null ? desglose.getCategoriaMenor().getDescripcion() : "");
				} else {
					linea.setAssetBookTypeCode(null);
					linea.setAssetCategoryIdMajor(null);
					linea.setAssetCategoryIdMinor(null);
				}
			
//			lineasEbsService.createLineasEbs(linea);
//			lineaItem++;

			return linea;
		}
	
	// [8] Anticipo -------------------------------------------------------------------
	private LineasEbs loadLineaAnticipoViaje(Integer encabezadoID, int lineaItem, Solicitud solicitud) {
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		String cuenta = parametroService.getParametroByName("ccAnticipo").getValor();
		Integer idOrganizacion = solicitud.getCompania().getIdOrganizacion();
		String companiaNum = solicitud.getCompania().getNumeroCompania();
		Integer locacion = solicitud.getLocacion().getNumero();

		
		String rfcProveedor=null;
		String prooveedorDescripcion="";
		
		
//		if (solicitud.getTipoProveedor().getIdTipoProveedor() == Etiquetas.TIPO_PROVEEDOR_ASESOR) {
//			rfcProveedor = solicitud.getUsuarioByIdUsuarioAsesor().getRfc();
//			prooveedorDescripcion = solicitud.getUsuarioByIdUsuario().getNombreCompletoUsuario();
//		} else {
//			rfcProveedor = solicitud.getProveedor().getRfc();
//			prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
//		}
		
		if(solicitud.getProveedor()!=null){
			rfcProveedor = solicitud.getProveedor().getRfc();
			prooveedorDescripcion = solicitud.getProveedor().getDescripcion();
		}


		LineasEbs linea = new LineasEbs();
		/* 2: Invoice ID*/
		linea.setInvoiceId(encabezadoID);
		/* 3: Line number*/
		linea.setLineNumber(lineaItem);
		/* 4: TypeLookupCode*/
		linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
		/* 5: Amount*/
		linea.setAmount(solicitud.getMontoTotal());
		
		/* 6: Descripci๏ฟฝn de la solicitud Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto /nombre"] */
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getConceptoGasto());
		tokens.add(solicitud.getLocacion().getNumeroDescripcionLocacion());
		tokens.add(String.valueOf(solicitudAnticipoViajeService.getSolicitudAnticipoViajeBySolicitud(solicitud.getIdSolicitud()).getNumPersonas()));
		//Tokens + Index de la descripci๏ฟฝn.
		String descripcion = buildDescriptionEBS(tokens, 0);
		linea.setDescription(descripcion);
		
		
		/* 7: Tax Code*/
		linea.setTaxCode(null);
		/* 8: Concatenated */		
		Usuario usuario = solicitud.getUsuarioByIdUsuario();
		
		/* 7: Concatenated (dist code)*/
		//NUM_COMPAัIA - NUM_LOCACION - 00 - CUENTA_CONTABLE - 0000
		String concatenated = companiaNum+"-"+locacion+"-00-"+cuenta+"-0000";
		linea.setDistCodeConcatenated(concatenated);
		
		
		/* 9: Attribute 3 */
		linea.setAttribute3(rfcProveedor);
		/* 10: Attributo 4 */
		String atributo4 = "";
		String fechaNumber = fecha.replace("/", "");
//		atributo4 = solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS() + " - " + fechaNumber + " - "+ solicitud.getIdSolicitud();
		linea.setAttribute4(null);
		/* 11: Attributo 5 */
		linea.setAttribute5(null);
		/* 12: Org ID */
		linea.setOrgId(idOrganizacion);
		// Assets 13, 14, 15, 16
		linea.setAssetsTrackingFlag("N");
		linea.setAssetBookTypeCode(null);
		linea.setAssetCategoryIdMajor(null);
		linea.setAssetCategoryIdMinor(null);
		return linea;
	}
	
	// [9] Comprobacion Gasto Viaje -------------------------------------------------------------------
	private LineasEbs loadLineaComprobacionGastoViaje(Integer encabezadoID, int lineaItem, Solicitud solicitud, Factura fact) {
		String fecha = Utilerias.convertDateFormat(solicitud.getCreacionFecha());
		String cuenta = parametroService.getParametroByName("ccNoDeducible").getValor();
		Integer idOrganizacion = solicitud.getCompania().getIdOrganizacion();
		String companiaNum = solicitud.getCompania().getNumeroCompania();
		Integer locacion = solicitud.getLocacion().getNumero();
		//
		LineasEbs linea = new LineasEbs();
		/* 1: ID Factura */
		linea.setFacturaId(fact.getIdFactura());
		/* 2: Invoice ID */
		linea.setInvoiceId(encabezadoID);
		/* 2: LineNumber */
		linea.setLineNumber(lineaItem);
		/* 3: TypeLookupCode */
		linea.setLineTypeLookupCode(parametroService.getParametroByName("etiquetaItemEBS").getValor());
		/* 4: Importe de recorrido */
		linea.setAmount(fact.getTotal());
		/* 5: Descripci๏ฟฝn de la solicitud
		Ingrese los tokens en orden deseado. ejemplo: ["ConceptoGasto /
		nombre"] */
		
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add(solicitud.getConceptoGasto());
		FacturaGastoViaje facturaGv = facturaGastoViajeService.getFacturaGastoViajeByIdFactura(fact.getIdFactura());
		if(facturaGv.getViajeConcepto().getIdViajeConcepto() == Integer.parseInt(parametroService.getParametroByName("idViajeConceptoAlimentos").getValor()))
			tokens.add(facturaGv.getViajeTipoAlimentos().getDescripcion());		
		String descripcion = buildDescriptionEBS(tokens, 0);
		linea.setDescription(descripcion);
		
		/* 6: TaxCode */
		linea.setTaxCode(null);
		/* 7: DistCodeConcatenated */
		String concatenated = companiaNum+"-"+locacion+"-00-"+cuenta+"-0000";
		linea.setDistCodeConcatenated(concatenated);
		/* 8: Attributo 3 */
		linea.setAttribute3(solicitud.getUsuarioByIdUsuarioSolicita().getRfc());
		/* 9: Attributo 4 */	
		String atributo4 = "";
		String fechaNumber = fecha.replace("/", "");
//		atributo4 = solicitud.getTipoSolicitud().getPrefijoInvoiceNumEBS()+" - "+fechaNumber+" - "+solicitud.getIdSolicitud();
		atributo4 = fact.getFactura();//Folio Factura
		linea.setAttribute4(atributo4);
		/* 10: Attributo 5 */	
		linea.setAttribute5(fact.getFolioFiscal());
		/* 11: ORG_ID */
		linea.setOrgId(idOrganizacion);
		/* 12,13,14,15: TAX CODE */
		linea.setAssetsTrackingFlag("N");
		linea.setAssetBookTypeCode(null);
		linea.setAssetCategoryIdMajor(null);
		linea.setAssetCategoryIdMinor(null);
		
		return linea;
	}
	// Fin comprobarAnticipo ------------------------------------------------------
	
	public static List<Locacion> getLocacionesPermitidasPorUsuario(List<UsuarioConfSolicitante> uconfigSol, Integer tipoSolicitud, List<Locacion> lc, Integer idUsuario) {

		// cargar los tipos de locacion permitidos por usuario
		List<Integer> idsLocaciones = new ArrayList<>();
		for (UsuarioConfSolicitante conf : uconfigSol) {
			if (conf != null && conf.getTipoSolicitud().getIdTipoSolicitud() == tipoSolicitud && conf.getUsuario().getIdUsuario() == idUsuario) {
				idsLocaciones.add(conf.getLocacion().getIdLocacion());
			}
		}

		List<Locacion> lcPermitidas = new ArrayList<>();
		for (Locacion l : lc) {
			if (idsLocaciones.contains(l.getIdLocacion())) {
				lcPermitidas.add(l);
			}
		}

		return lcPermitidas;
	}
	
	
	// metodo ajax para el cambio de estatus a cancelada.
	@RequestMapping(value = "/enviarAProceso", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	    public @ResponseBody ResponseEntity<String> enviarAProceso(HttpSession session, @RequestParam Integer idSolicitud, HttpServletRequest request, HttpServletResponse response) {
		  
		  String json = null;
	      HashMap<String, String> result = new HashMap<String, String>();

		  if(idSolicitud != null && idSolicitud > Etiquetas.CERO){
			  
		      Solicitud solicitud = solicitudService.getSolicitud(idSolicitud);
			  Usuario	usuario = usuarioService.getUsuario(usuarioService.getUsuarioSesion().getIdUsuario());
			  
			  Map<Boolean, String> respuesta = new HashMap<>();
			  
			  respuesta = solicitudManagerService.enviarSolicitud(solicitud,  usuario);
			  
			  String respuestaTrue = respuesta.get(true);
			  String respuestaFalse = respuesta.get(false);
			  
			  if(respuestaTrue != null){
				  result.put("resultado", "true");
				  result.put("mensaje", respuestaTrue);

			  }else if(respuestaFalse != null){
				  result.put("resultado", "false");
				  result.put("mensaje", respuestaFalse);
			  }
	
		  }else{
			  result.put("resultado", "false");
		  }
		  
        //bind json
	        ObjectMapper map = new ObjectMapper();
	        if (!result.isEmpty()) {
	            try {
	                json = map.writeValueAsString(result);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	        
	        //respuesta
	        HttpHeaders responseHeaders = new HttpHeaders(); 
	        responseHeaders.add("Content-Type", "application/json; charset=utf-8"); 
	        return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
	    }
	  
	  
	  public HashMap<String, Object> getConXmlDatos(HttpSession session, Integer id, Integer idUsuario,
			UsuarioConfSolicitanteService uConfsolicitanteService, List<Locacion> locaciones,
			List<CuentaContable> ccontable, AidService aidService, CategoriaMayorService categoriaMayor,
			CategoriaMenorService categoriaMenor, CompaniaService companiaService, ProveedorService proveedorService,
			MonedaService monedaService, UsuarioService usuarioService, SolicitudService solicitudService,SolicitudArchivoService solicitudArchivoService, ParametroService parametroService) {

		HashMap<String, Object> model = new HashMap<String, Object>();
		FacturaConXML facturaConXML = new FacturaConXML();
		Integer idEstadoSolicitud = 0;

			if (id == null) {
				// destruir la sesion, creada por parametro
				session.setAttribute("solicitud", null);
			}

			// configuraciones permitidas por usuario
			List<UsuarioConfSolicitante> uconfigSol = uConfsolicitanteService.getUsuarioConfSolByIdUsuario(idUsuario);
			// Obtener combos filtrador por configuracion de solicitante.
			List<Locacion> lcPermitidas = UtilController.getLocacionesPermitidasPorUsuario(uconfigSol,Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()),locaciones,idUsuario);
			List<CuentaContable> ccPermitidas = getCuentasContablesPermitidasPorUsuario(uconfigSol,ccontable);

			// OBTENER AID'S
			List<Aid> listAids = aidService.getAllAid();

			// Obtener categoria mayor y menor
			List<CategoriaMayor> listCatMayor = categoriaMayor.getAllCategoriaMayor();
			List<CategoriaMenor> listCatMenor = categoriaMenor.getAllCategoriaMenor();

			// Obtener Companias, Proveedores y Monedas
			List<Compania> lstCompanias = companiaService.getAllCompania();
			List<Proveedor> lstProveedores = proveedorService.getAllProveedoresActivos();
			List<Moneda> lstMoneda = monedaService.getAllMoneda();

			// obtener los jefe para opcion de usuario solicitante.
			List<Usuario> lstUsuariosJefe = new ArrayList<>();
			Usuario usuario = usuarioService.getUsuario(idUsuario);
			if(usuario.getUsuario() != null){
			  lstUsuariosJefe.add(usuario.getUsuario());
			}

			Solicitud solicitud = null;
			Factura factura = null;
			List<FacturaDesglose> facturasDesglose = new ArrayList<>();
			List<FacturaDesgloseDTO> facturasDesgloseDTO = new ArrayList<>();

			// Obtener solicitud,factura y su desglose si se requiere:
			if (id != null && id > Etiquetas.CERO) {
				facturaConXML.setIdSolicitudSession(id);
				solicitud = solicitudService.getSolicitud(facturaConXML.getIdSolicitudSession());
				if(solicitud != null){
				  idEstadoSolicitud = solicitud.getEstadoSolicitud().getIdEstadoSolicitud();
				}
			}

			if (solicitud != null && solicitud.getFacturas().size() > Etiquetas.CERO) {

				// si la solicitud es v๏ฟฝlida entonces crear una variable de sesi๏ฟฝn
				// para el update.
				session.setAttribute("solicitud", solicitud);

				factura = solicitud.getFacturas().get(Etiquetas.CERO);
				if (factura != null && solicitud.getFacturas().size() > Etiquetas.CERO) {
					
					if(factura.getCompaniaByIdCompania() != null)
						facturaConXML.setCompania(factura.getCompaniaByIdCompania());
					if(factura.getProveedor() != null){
						facturaConXML.setProveedor(factura.getProveedor());				
						facturaConXML.setRfcEmisor(factura.getProveedor().getRfc());
					}
					if(factura.getFolioFiscal() != null)
						facturaConXML.setFolioFiscal(factura.getFolioFiscal());
					facturaConXML.setFolio(factura.getFactura()); //
					if(factura.getSerieFactura() != null)				
						facturaConXML.setSerie(factura.getSerieFactura());
					if(factura.getTipoFactura() != null)				
						facturaConXML.setTipoFactura(factura.getTipoFactura().getIdTipoFactura());								
					
					//retenciones
					if(factura.getConRetenciones() > Etiquetas.CERO_S){
						facturaConXML.setConRetenciones(Etiquetas.TRUE);
						facturaConXML.setStrIsr_retenido(factura.getIsrRetenido() != null ? factura.getIsrRetenido().toString() : "");
						facturaConXML.setStrIva_retenido(factura.getIvaRetenido() != null ? factura.getIvaRetenido().toString() : "");
					}else{
						facturaConXML.setConRetenciones(Etiquetas.FALSE);
					}
					
					String fechaString = null;
					SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat sdfOut = new SimpleDateFormat("MM/dd/yyyy");
					
					try {
						fechaString = sdfOut.format(sdfIn.parse(factura.getFechaFactura().toString()));
						facturaConXML.setFecha_factura(fechaString);
					} catch (ParseException e) {
						
					}
	                
					facturaConXML.setConcepto(solicitud.getConceptoGasto());
					
					/*if(factura.getTrackAsset() > Etiquetas.CERO){
					  facturaConXML.setTrack_asset(Etiquetas.TRUE);
					}else{
					  facturaConXML.setTrack_asset(Etiquetas.FALSE);
					}
					
					facturaConXML.setPar(factura.getPar());
					facturaConXML.setId_compania_libro_contable(factura.getCompaniaByIdCompaniaLibroContable());*/

					facturaConXML.setStrIva(factura.getIva()!=null ? factura.getIva().toString(): "");
					facturaConXML.setStrIeps(factura.getIeps()!=null ? factura.getIeps().toString() : "");
					facturaConXML.setStrTotal(factura.getTotal()!=null ? factura.getTotal().toString():"");
					facturaConXML.setStrSubTotal(factura.getSubtotal() !=null ? factura.getSubtotal().toString() : "");;
					
					facturaConXML.setMoneda(factura.getMoneda());
					if (solicitud.getUsuarioByIdUsuario().getIdUsuario() != solicitud.getUsuarioByIdUsuarioSolicita()
							.getIdUsuario()) {
						facturaConXML.setSolicitante(true);
						facturaConXML.setIdSolicitanteJefe(solicitud.getUsuarioByIdUsuarioSolicita().getIdUsuario());
					} else {
						facturaConXML.setSolicitante(false);
					}

					facturasDesglose = factura.getFacturaDesgloses();
					if (facturasDesglose != null && facturasDesglose.size() > Etiquetas.CERO) {
						for (FacturaDesglose fd : facturasDesglose) {
							FacturaDesgloseDTO fdd = new FacturaDesgloseDTO();
							if (fd.getAid() != null && fd.getAid().getIdAid() > Etiquetas.CERO) {
								fdd.setAid(fd.getAid());
							}
							if (fd.getCategoriaMayor() != null
									&& fd.getCategoriaMayor().getIdCategoriaMayor() > Etiquetas.CERO) {
								fdd.setCategoriaMayor(fd.getCategoriaMayor());
							}
							if (fd.getCategoriaMenor() != null
									&& fd.getCategoriaMenor().getIdCategoriaMenor() > Etiquetas.CERO) {
								fdd.setCategoriaMenor(fd.getCategoriaMenor());
							}
							fdd.setConcepto(fd.getConcepto());
							fdd.setCuentaContable(fd.getCuentaContable());
							fdd.setLocacion(fd.getLocacion());
							fdd.setStrSubTotal(fd.getSubtotal().toString());
							facturasDesgloseDTO.add(fdd);
						}
					}
				}
			} else {
				// mensaje de solicitud no valida a la vista.
				facturaConXML.setIdSolicitudSession(Etiquetas.CERO);
			}

			facturaConXML.setFacturaDesgloseList(facturasDesgloseDTO);
			
			//revisar si se trata de una modificaci๏ฟฝn para activar mensaje en la vista
			if(session.getAttribute("actualizacion") != null){
				facturaConXML.setModificacion(Etiquetas.TRUE);
				session.setAttribute("actualizacion", null);
			}else{
				facturaConXML.setModificacion(Etiquetas.FALSE);
			}
			
			//revisar si se trata de un nuevo registro para activar mensaje en la vista
			if(session.getAttribute("creacion") != null){
				facturaConXML.setCreacion(Etiquetas.TRUE);
				session.setAttribute("creacion", null);
			}else{
				facturaConXML.setCreacion(Etiquetas.FALSE);
			}
			
	        
			// Enviar archivos anexados por solicitud:
			
			List<SolicitudArchivo> solicitudArchivoList = new ArrayList<>();
			if(facturaConXML.getIdSolicitudSession() > Etiquetas.CERO){
				solicitudArchivoList = solicitudArchivoService.getAllSolicitudArchivoBySolicitud(facturaConXML.getIdSolicitudSession()); 
			}
			
			Usuario usuarioSession = usuarioService.getUsuarioSesion();
			
			model.put("facturaConXML", facturaConXML);
			model.put("lcPermitidas", lcPermitidas);
			model.put("ccPermitidas", ccPermitidas);
			model.put("listAids", listAids);
			model.put("listCatMayor", listCatMayor);
			model.put("listCatMenor", listCatMenor);
			model.put("lstCompanias", lstCompanias);
			model.put("lstProveedores", lstProveedores);
			model.put("lstMoneda", lstMoneda);
			if(facturaConXML.isSolicitante() == true && facturaConXML.getIdSolicitanteJefe() == usuarioSession.getIdUsuario()){
				lstUsuariosJefe = new ArrayList<>();
				lstUsuariosJefe.add(usuarioSession);
				model.put("lstUsuariosJefe", lstUsuariosJefe);
			}else{
				model.put("lstUsuariosJefe", lstUsuariosJefe);

			}
			
			model.put("estadoSolicitud", idEstadoSolicitud);
			model.put("solicitudArchivoList", solicitudArchivoList);
			model.put("tipoSolicitud",Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudNoMercanciasXML").getValor()));
			model.put("isSolicitante", usuario.getEspecificaSolicitante());
			model.put("usuarioSession", usuarioSession);
			
			return model;
	  }
	  
	  
	public static List<CuentaContable> getCuentasContablesPermitidasPorUsuario(List<UsuarioConfSolicitante> uconfigSol, List<CuentaContable> ccontable) {
		// cargar los tipos de cuenta contable permitidos por usuario
		List<Integer> idCuentaContable = new ArrayList<>();
		for (UsuarioConfSolicitante confCuentas : uconfigSol) {
			if (confCuentas != null) {
				idCuentaContable.add(confCuentas.getCuentaContable().getIdCuentaContable());
			}
		}

		List<CuentaContable> ccontableAux = new ArrayList<>();
		for (CuentaContable cc : ccontable) {
			if (idCuentaContable.contains(cc.getIdCuentaContable())) {
				ccontableAux.add(cc);
			}
		}

		return ccontableAux;
	}
		
	public static List<CuentaContable> getCuentasContablesPermitidasPorLocacion(Integer locacionID,
			List<UsuarioConfSolicitante> uconfigSol, Integer tipoSolicitud) {
		List<CuentaContable> ccontableAux = new ArrayList<>();

		if (uconfigSol != null && uconfigSol.isEmpty() == false) {
			for (UsuarioConfSolicitante conf : uconfigSol) {
              if(conf.getLocacion().getIdLocacion() == locacionID && conf.getTipoSolicitud().getIdTipoSolicitud() == tipoSolicitud){
            	  ccontableAux.add(conf.getCuentaContable());
              }
			}
		}

		return ccontableAux;
	}
		
	public static String getCuentasContablesHTML(Integer idLocacion, List<UsuarioConfSolicitante> uconfigSol, Integer tipoSolicitud, CuentaContable ccPAR){
		  StringBuilder row2 = new StringBuilder();
			row2.append("<option value=\"-1\">Seleccione:</option>");
			

			List<CuentaContable> ccPermitidas = new ArrayList<>();
	        //List<CuentaContable> ccPermitidasAnticipo = UtilController.getCuentasContablesPermitidasPorLocacion(idLocacion, uconfigSol, Integer.parseInt(parametroService.getParametroByName("idTipoSolicitudAnticipo").getValor())_GASTOS_VIAJE);
	        List<CuentaContable> ccPermitidasComprobacion = UtilController.getCuentasContablesPermitidasPorLocacion(idLocacion, uconfigSol, tipoSolicitud);
	        
	        //ccPermitidas.addAll(ccPermitidasAnticipo);
	        ccPermitidas.addAll(ccPermitidasComprobacion);
	        
		    for (CuentaContable cc : ccPermitidas) {
				row2.append("<option value=\"");
				row2.append(String.valueOf(cc.getIdCuentaContable()));
				StringBuilder ccontableDesc = new StringBuilder();
				ccontableDesc.append(cc.getNumeroDescripcionCuentaContable());
				row2.append("\"> " + ccontableDesc.toString() + " </option>");
			}
		    
		    
             if(ccPAR != null && ccPermitidas.contains(ccPAR) == false){
				row2.append("<option value=\"");
				row2.append(String.valueOf(ccPAR.getIdCuentaContable()));
				StringBuilder ccontableDesc2 = new StringBuilder();
				ccontableDesc2.append(ccPAR.getNumeroDescripcionCuentaContable());
				row2.append("\"> " + ccontableDesc2.toString() + " </option>");		    	
		    }
		    
		  
		    
		    
		    return row2.toString();
	  }
		  
	public static String getCuentasContablesHTML(Integer idLocacion, List<UsuarioConfSolicitante> uconfigSol,Integer idCuentaContable, Integer tipoSolicitud, CuentaContable ccPAR){
		  StringBuilder row2 = new StringBuilder();
		  boolean tienelaCuentaConfigurada = false;
			row2.append("<option value=\"-1\">Seleccione:</option>");
	        List<CuentaContable> ccPermitidas = UtilController.getCuentasContablesPermitidasPorLocacion(idLocacion, uconfigSol, tipoSolicitud);
		    for (CuentaContable cc : ccPermitidas) {
		    	
		    	if(idCuentaContable == cc.getIdCuentaContable()){
				row2.append("<option selected value=\"");
				tienelaCuentaConfigurada = true;
		    	}else{
					row2.append("<option value=\"");
		    	}
				
				row2.append(String.valueOf(cc.getIdCuentaContable()));
				StringBuilder ccontableDesc = new StringBuilder();
				ccontableDesc.append(cc.getNumeroDescripcionCuentaContable());
				row2.append("\"> " + ccontableDesc.toString() + " </option>");
			}
		    
		    if(tienelaCuentaConfigurada == false){
		    	
		    	row2.append("<option selected value=\"").append(ccPAR.getIdCuentaContable()).append("\">").append(ccPAR.getNumeroDescripcionCuentaContable()).append("</option>");
		    	
		    }else if(tienelaCuentaConfigurada == false){
		    	row2.append("<option  value=\"").append(ccPAR.getIdCuentaContable()).append("\">").append(ccPAR.getNumeroDescripcionCuentaContable()).append("</option>");
		    }
		    
		    return row2.toString();
	  }
		
	public static Integer getIDByCuentaContable(String cuentaContable, List<CuentaContable> cuentasContables){
		
		Integer idCuentaContable = null;
		
		for(CuentaContable cuenta : cuentasContables){
			try{
			if(cuenta.getNumeroCuentaContable() != null && cuentaContable.equals(cuenta.getNumeroCuentaContable())){
				Integer t = 0;
				//System.out.println(t);
				idCuentaContable = cuenta.getIdCuentaContable();
				break;
			}
			}catch(Exception e){
				logger.error(e);
			}
		}
			
		return idCuentaContable;
	}
		
	/**
	 * @param facturaSolicitudDTO
	 * @return ProveedorLibre si se guard๏ฟฝ correctamente, null si hubo un error al guardar.
	 * CREA UN NUEVO REGISTRO EN LA TABLA DE PROVEEDOR_LIBRE CUANDO EL PROVEEDOR DE LA FACTURA NO EXISTE EN EL SISTEMA
	 */
	private ProveedorLibre agregaNuevoProveedor(String descripcion, String Rfc) {
		ProveedorLibre nuevo = new ProveedorLibre();
		nuevo.setDescripcion(descripcion);
		nuevo.setRfc(Rfc);
		nuevo.setActivo(Etiquetas.UNO_S);
		nuevo.setCreacionUsuario(usuarioService.getUsuarioSesion().getIdUsuario());
		nuevo.setCreacionFecha(new Date());
		Integer idProveedorLibre = proveedorLibreService.createProveedorLibre(nuevo);
		if (idProveedorLibre != null)
			return nuevo;
		else
			return null;
	}
		
	private String buildDescriptionEBS(ArrayList<String> tokens, int indexDesc) {
		int joins = 3 * (tokens.size() - 1);
		int charsLeft = 240 - joins;
		for (int i = tokens.size() - 1; i >= 0; i--) {
			if (i != indexDesc)
				charsLeft -= tokens.get(i).length();
		}
		if (tokens.get(indexDesc).length() > charsLeft)
			tokens.set(indexDesc, tokens.get(indexDesc).substring(0, charsLeft));

		return String.join(" / ", tokens);
	}
	
	
		@RequestMapping(value = "/conceptoFactura", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
		    public @ResponseBody ResponseEntity<String> conceptoFactura(HttpSession session, @RequestParam Integer idFactura, HttpServletRequest request, HttpServletResponse response) {
			  
			  String json = null;
		      HashMap<String, String> result = new HashMap<String, String>();

			  if(idFactura != null && idFactura > Etiquetas.CERO){
				  result.put("conceptoGasto", facturaService.getFactura(idFactura).getConceptoGasto());
				  result.put("idSolicitud", String.valueOf(facturaService.getFactura(idFactura).getSolicitud().getIdSolicitud()));
				  
			  }else{
				  result.put("conceptoGasto", null);
				  result.put("idSolicitud", null);
			  }
			  
	        //bind json
		        ObjectMapper map = new ObjectMapper();
		        if (!result.isEmpty()) {
		            try {
		                json = map.writeValueAsString(result);
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		        
		        //respuesta
		        HttpHeaders responseHeaders = new HttpHeaders(); 
		        responseHeaders.add("Content-Type", "application/json; charset=utf-8"); 
		        return new ResponseEntity<String>(json, responseHeaders, HttpStatus.CREATED);
		    }
		  
	
	
	
	
}