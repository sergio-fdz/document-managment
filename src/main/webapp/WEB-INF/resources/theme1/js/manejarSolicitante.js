

var ischanged = false;

$("#solicitante").click(function() {
	if ($("#solicitante").is(":checked")) {
		openModalCambiarSolicitante();
		$("#idSolicitanteJefe").prop('disabled', false);
	} else {
		$("#idSolicitanteJefe").prop('disabled', true);
		$("#idSolicitanteJefe").val(-1);
		openModalCambiarSolicitante();
	}
});


$(document).on('change', '#idSolicitanteJefe', function() {
	openModalCambiarSolicitante();
	console.log("el cambo de seleccion de solicitante cambio onchange.");
	ischanged = true;
});

//acepto cambiar solicitante
$("#cambiar_solicitante_button").click(function() {
	limpiarFactura();
	$("#file").val(null);
	$("#pdf").val(null);
	$(".file-selected").text(null);
	cleanDesglose();
	if(ischanged == false){
		$("#idSolicitanteJefe").val(-1);
	}
	$("#modal-solicitud").modal("hide");
	sumSbt();
});

$("#selecciono_ord_compra").click(function() {
	cleanDesglose();
	$("#modal-solicitud").modal("hide");
	sumSbt();

});

$("#selecciono_ord_compra_cancelar").click(function() {
	$("#orden_compra").prop("checked",false);
	$("#modal-solicitud").modal("hide");
});

$("#cancelar_ord_compra_").show();
$("#cancelar_ord_compra_cancelar_").show();


$("#cancelar_ord_compra_cancelar_").click(function() {
	
	cancelarOrdenCompra();

});

$("#cambiar_ord_compra").click(function() {
	
	cleanDesglose();
	
	$("#seleccionaTodoRecibos").prop("checked",false);
	$("#modal-solicitud").modal("hide");
	limpiarPARInputs();
	$("#recibosList").empty();
	
	//$("#idOrdenCompra").val(null);
	//$("#boton_agregar").show();
	//$("#concepto_xml").show();
	//$(".th_po").hide();
	//$(".td_po").hide();
	
	$("#cambiar_ord_compra").hide();
	$("#cambiar_ord_compra_cancelar").hide();
	var orcompra =  $("#numero_ord_compra_hi").val();
	$("#idOrdenCompra").val(orcompra);
	ordenCompraSelect(orcompra,true);
	sumSbt();

	
});

$("#cambiar_ord_compra_cancelar").click(function() {
	
	var ordenCompra = $("#numero_ord_compra_anterior").val();
	console.log("ordenCompra:  "+ordenCompra);
	$("#idOrdenCompra").val(ordenCompra);
	
	$("#cambiar_ord_compra").hide();
	$("#cambiar_ord_compra_cancelar").hide();
	
	$("#modal-solicitud").modal("hide");
	

	
});






$("#cancelar_ord_compra_").click(function() {
	$("#orden_compra").prop("checked",true);
	$("#wrapper_recibos").show();
	$("#modal-solicitud").modal("hide");
});






//cancelo cambiar solicitante
$("#solicitud_button_cancelar").click(function() {
	
	
	var solChecked = $("#solicitante").is(":checked")
	var selectedSol = $("#idSolicitanteJefe").val();
	
	// si cambian el estatus del check de solicitante
	if(solChecked == true && ischanged == false){
		$("#solicitante").prop("checked", false);
	}else{
		$("#solicitante").prop("checked", true);
	}
	
	// si el cambio viene de la seleccion de otra opcion en el select entonces
	if(solChecked == true && selectedSol == -1 && ischanged == true){
	    $("#idSolicitanteJefe").val($("#idSolicitanteJefe option:eq(1)").val());
		ischanged = false;
	}
	
	$("#cambiarxml_button_cancelar").show();
	$("#solicitud_button_cancelar").hide();
	$("#modal-solicitud").modal("hide");
});


function openModalCambiarSolicitante(){
	
	console.log("solicitud_global_mabejar: "+tipoSolicitudGlobal);
	if(tipoSolicitudGlobal == 1  || tipoSolicitudGlobal == 2){
		console.log("entro a ocultar botones desde manejar sol.");
		ocultarBotonesModal();
	}
	
	$("#cambiar_solicitante_button").show();
	$("#solicitud_button_cancelar").show();
	
	//Se exluye la linea de totales para caso reembolso y caja chica.
	var length = $("#tablaDesglose > tbody > tr").not('.tr-totales').length;
	
	if (length > 0 || $("#orden_compra").is(":checked")) {
		$("#mensaje-dialogo").text(MENSAJE_CAMBIO_SOLICITANTE_NOXML);
		$("#modal-solicitud").modal({
			backdrop : 'static',
			keyboard : false
		});
		$("#cambiar_solicitante_button").show();
		$("#cambiarxml_button_cancelar").hide();
		$("#solicitud_button_cancelar").show();
	}
}



