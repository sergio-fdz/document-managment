/**
 * Funcionalidad ord de compra.
 * 
 */

  $(document).ready(function(){
	  
	     
	    if(isOrdenCompra == 'true'){
	    	
	    	vistaOrdenCompra();
		    console.log("ocultar boton agregar if is orden compra: "+isOrdenCompra);

	    	checkIfOrdenCompra();
	    	
	    	    	
	    }else{
	    	
		    console.log("no es orden de compra: "+isOrdenCompra);

	    	
		    /*Estado original de la tambla sin habilitar la orden de compra*/
			$(".th_po").hide();
			$(".td_po").hide();
			
			if(idSolicitudGlobal > 0){
				console.log("idSolicitudGlobal ordcompra.js : "+idSolicitudGlobal);
				$("#orden_compra").prop("disabled", true);
			}

	    	
	    }
	    
	    console.log("es orden de compra: "+isOrdenCompra);

		
	    $(".scrollable table").colResizable({
	        fixed:false,
	        liveDrag:true,
	        draggingClass:"dragging",
	        disabledColumns: [0],
	        resizeMode:'overflow'
	    });
		
	  
	  
	  
	  $("#orden_compra").change(function() {
		  
		  checkIfOrdenCompra();
		  if($("#orden_compra").is(":checked")) {
				ventanaModalOrdenCompraCambio();
		  }else{
		    	promptSeguroCancelarPO();
		  }

		    
		});
	  
	  
	  
	  $("#deselecciona_recibo_ok").click(function(){
		  
		  $("#seleccionaTodoRecibos").prop("checked", false);
		  
		  
		  var recibo = $("#recibo_hi").val();
		   removerArticulos(recibo);
		   calculateLine();
		    // remover recibo del backend
			removerRecibo(recibo);
		   
		   
		    $("#deselecciona_recibo_ok").hide();
			$("#deselecciona_recibo_cancelar").hide();
			$("#modal-solicitud").modal("hide");
			
			
			//sumSbt();
		  
	  });
	  
	  
	  $("#deselecciona_recibo_cancelar").click(function(){
		  
		  var recibo = $("#recibo_hi").val();
		  $(".recibosOrd").each(function(i){
			  if($(this).val() == recibo){
				  $(this).prop("checked",true);
			  }
		  });
		  
		    $("#deselecciona_recibo_ok").hide();
			$("#deselecciona_recibo_cancelar").hide();
			$("#modal-solicitud").modal("hide");
		  
	  });
	  
	  
	  $("#deselecciona_todos_recibo_ok").click(function(){
		  
			  $(".recibosOrd").each(function( index ) {
				  $(this).prop("checked", false);
				  removerArticulos($(this).val());
			  });
			
		    $("#deselecciona_todos_recibo_cancelar").hide();
			$("#deselecciona_todos_recibo_ok").hide();
			$("#modal-solicitud").modal("hide");
			
		  
	  });
	  
	  
	  $("#deselecciona_todos_recibo_cancelar").click(function(){
		     
		    $("#seleccionaTodoRecibos").prop("checked",true);
		    $("#deselecciona_todos_recibo_cancelar").hide();
			$("#deselecciona_todos_recibo_ok").hide();
			$("#modal-solicitud").modal("hide");
			var ordCompra = $("#idOrdenCompra").val();
			
			  $(".recibosOrd").each(function( index ) {
				  console.log("each rexibos : "+this);
				  $(this).prop("checked", true);
				  accionRecibos(this,ordCompra);
			  });
			
			
			
		  
	  }); 
	  
	  
	  if(isOrdenCompra && idSolicitudGlobal > 0){
			console.log("idSolicitudGlobal ordcompra.js : "+idSolicitudGlobal);
			if(idStatus > 1 && idStatus != 4){
				$("#orden_compra").prop("disabled", true);
				$("#idOrdenCompra").prop("disabled", true);
				  $(".recibosOrd").prop("disabled",true);
				  
			}else{
				$("#orden_compra").prop("disabled", false);
			}

	  }
	  
	  
	  
	   $("#tiene_orden_compra_aceptar").click(function(){
		   $("#tiene_orden_compra").val(0);
		   var length = $("#tablaDesglose > tbody > tr").length;
			$("#modal-solicitud").modal("hide");
		    ocultarBotonesModal();
		    callRow(length);
	   });
	   
	   $("#tiene_orden_compra_cancelar").click(function(){
			$("#modal-solicitud").modal("hide");
		    ocultarBotonesModal();
	   });
	   
	   
	   
	   if(idSolicitudGlobal > 0 && tipoSolicitudGlobal == 2){
			$(".cMayor").prop("disabled",true);
			$(".cMenor").prop("disabled",true);
	   }
	   
	   
	   $("#compania").change(function(){
			  if(tipoSolicitudGlobal == 2){
				  habilitaOrdenCompraSin();
			  }
	   });

	   
	   
	   
	   if($("#orden_compra").is(":checked") && tipoSolicitudGlobal == 2){
			console.log("idSolicitudGlobal conxml : "+idSolicitudGlobal);
			console.log("orden de compra is enabled.");
			$(".ordCompraRow").prop('disabled',true);
		}
	   
  });	
  
  
  
  function getPOs(){
	  
	     var idTipoFactura = 1;
	     if(tipoSolicitudGlobal == 1){
	    	 console.log("get pos tipo solicitud es con xml toma el valor del combo ");
		     idTipoFactura = $("#tipoFactura").val();
	     }
	     
	     var idSolicitanteActual = 0;
	     var esSolicitante = 0;
	     	     
	     if($("#solicitante").is(":checked")){
	    	 esSolicitante = 1;
	    	 idSolicitanteActual = $("#idSolicitanteJefe").val();
	     }
	     
		 loading(true);
		 
	     $.ajax({
	    		type : "GET",
	    		cache : false,
	    		url : url_server+"/getPOs",
	    		async : false,
	    		data : "tipoFactura=" + idTipoFactura + "&idSolicitante=" + idSolicitanteActual + "&esSolicitante=" + esSolicitante+ "&idSolicitud=" + idSolicitudGlobal,
	    		success : function(response) {
	    			loading(false);
	    			
	    			$("#idOrdenCompra").empty().append(response.optionsOrdCompra);
	    			
	    			if(idOrdenCompraGlobal > 0 && enEdicionOrden == false){
	    				$("#idOrdenCompra").val(idOrdenCompraGlobal);
	    				ordenCompraSelect(idOrdenCompraGlobal,true);
	    			}
	
	    		},
	    		error : function(e) {
	    			loading(false);
	    			console.log('Error: ' + e);
	    		},
	    	});  
	  
  }
  
  function ordenCompraSelect(ordenCompraSelect,automatico){
	  
	     loading(true);
	     
	     console.log("ordenCompraSelect: "+ordenCompraSelect);
	  
	 var length = $("#tablaDesglose > tbody > tr").length;
	 
		var anterior =  $("#numero_ord_compra_anterior").val();
		var actual = $("#numero_ord_compra_hi").val();
		
		if(anterior != null && anterior > 0){
			
			console.log("anterior es > 0"+anterior);
			console.log("actual es = "+actual);
			
			$("#numero_ord_compra_anterior").val(actual);
			$("#numero_ord_compra_hi").val(ordenCompraSelect);
		}else{
			
			console.log("anterior es diferte ceri 0"+anterior);
			console.log("actual es = "+actual);
			
			$("#numero_ord_compra_hi").val(ordenCompraSelect);
			$("#numero_ord_compra_anterior").val(ordenCompraSelect);

		}

	  if(length > 0 && automatico == false){
		  console.log("si tiene filas es que ya selecciono uno.");
		  cambioOrdenCompra(ordenCompraSelect);
	  }else{
		  getRecibos(ordenCompraSelect);
		  checkIfPAR(ordenCompraSelect);

	  }
	  
     
	     loading(false);

  }
  
  function getRecibos(){

	  var idTipoFactura = 1;
	     if(tipoSolicitudGlobal == 1){
	    	 console.log("get pos tipo solicitud es con xml toma el valor del combo ");
		     idTipoFactura = $("#tipoFactura").val();
	     }
	     var idSolicitanteActual = 0;
	     var esSolicitante = 0;
	     var idPO = $("#idOrdenCompra").val();
	     
	     if($("#solicitante").is(":checked")){
	    	 esSolicitante = 1;
	    	 idSolicitanteActual = $("#idSolicitanteJefe").val();
	     }
	     
		 loading(true);
		 
	     $.ajax({
	    		type : "GET",
	    		cache : false,
	    		url : url_server+"/getRecibos",
	    		async : false,
	    		data : "tipoFactura=" + idTipoFactura + "&idSolicitante=" + idSolicitanteActual +
	    		"&esSolicitante=" + esSolicitante+ "&idPO=" + idPO + "&idSolicitud="+idSolicitudGlobal,
	    		success : function(response) {
	    			loading(false);
	    			
	    			$("#recibosList").empty().append(response.recibos);
                 	$('.currencyFormatRecibo').number( true, 2 );
                 	
	    		},
	    		error : function(e) {
	    			loading(false);
	    			console.log('Error: ' + e);
	    		},
	    	});  
	  
  }
  
  
  function checkIfPAR(ordenCompraSelect){
	    
	     var idCompania = $("#compania").val();
	     console.log("compañia: "+ idCompania);
	     loading(true);
	  
	     $.ajax({
	    		type : "GET",
	    		cache : false,
	    		url : url_server+"/checkPAR",
	    		async : false,
	    		data : "idPO=" + ordenCompraSelect + "&idCompania="+idCompania,
	    		success : function(response) {
	    			console.log("AjaxPAR: " +response.par);
	    			if(response.par != "null"){
	    				
	    				$("#track_asset").prop('checked', true);
	    				$("#track_asset").prop('disabled', true);
	    				$("#id_compania_libro_contable").val(response.compania);
	    				$("#par").val(response.par);
	    			}else{
	    				limpiarPARInputs();
	    				deshabilitarPAR(true);
	    			}
	    			
	    			loading(false);
	    		},
	    		error : function(e) {
	    			loading(false);
	    			console.log('Error: ' + e);
	    		},
	    	}); 
	  
	  
  }
  
  function limpiarPARInputs(){
	  $("#track_asset").prop("checked",false);
	  $("#id_compania_libro_contable").val(-1);
	  $("#par").val(null);
	  $("#track_asset").prop('disabled', false);

  }
  
  
  function deshabilitarPAR(b){
	  $("#par").prop("disabled",b);
	  $("#track_asset").prop("disabled",b);
	  $("#id_compania_libro_contable").prop("disabled",b);
  }
  
  
  function ventanaModalOrdenCompraCambio(){
	  
	    ocultarBotonesModal();
		$("#selecciono_ord_compra").show();
		$("#selecciono_ord_compra_cancelar").show();  
	  
		//Se exluye la linea de totales para caso reembolso y caja chica.
		var length = $("#tablaDesglose > tbody > tr").not('.tr-totales').length;
		if (length > 0) {
			$("#mensaje-dialogo").text(SELECCIONO_ORDEN_COMPRA);
			$("#modal-solicitud").modal({
				backdrop : 'static',
				keyboard : false
			});
			
	
			
		}
	  
  }
  
  function promptSeguroCancelarPO(){
	  
	  ocultarBotonesModal();
		$("#cancelar_ord_compra_").show();
		$("#cancelar_ord_compra_cancelar_").show();
			  
	  $("#mensaje-dialogo").text(CANCELAR_ORDEN_COMPRA);
		$("#modal-solicitud").modal({
			backdrop : 'static',
			keyboard : false
		});
		
			  
  }
  
  function accionRecibos(check,idPO){
	  
	  var recibo = $(check).val();
	  console.log("el checkbox en accion rexibos: "+check);
	  $("#recibo_hi").val(recibo);

	   if($(check).is(":checked")){
		   agregarArticulos(recibo,idPO);
		   
	   }else{
		   
		    ocultarBotonesModal();
			
			$("#deselecciona_recibo_ok").show();
			$("#deselecciona_recibo_cancelar").show();
			
			//$("#numero_ord_compra_hi").val(val);
				  
		  $("#mensaje-dialogo").text(EXCLUIR_RECIBO_ORD);
			$("#modal-solicitud").modal({
				backdrop : 'static',
				keyboard : false
			});
			
	   }
	   
	   sumSbt();
  }
  
  
  function agregarArticulos(recibo,idPO){
	  
	     var length = $("#tablaDesglose > tbody > tr").length;
	     var tipoFactura = 1;
	     if(tipoSolicitudGlobal == 1){
	    	 console.log("get pos tipo solicitud es con xml toma el valor del combo ");
	    	 tipoFactura = $("#tipoFactura").val();
	     }
	     
	     var idCompania = 0;
	     if(tipoSolicitudGlobal == 1){
		     idCompania = $("#id_compania_libro_contable").val();
	     }else if(tipoSolicitudGlobal == 2){
		     idCompania = $("#compania").val();
	     }

		 loading(true);
	     $.ajax({
	    		type : "GET",
	    		cache : false,
	    		url : url_server+"/addRowRecibos",
	    		async : false,
	    		data : "idRecibo=" + recibo + "&numrows=" + length + "&tipoFactura="+tipoFactura + 
	    		"&ordenCompra="+idPO + "&idCompania="+idCompania + "&idSolicitud="+idSolicitudGlobal + "&track_asset=" + $("#track_asset").is(":checked"),
	    		success : function(response) {
	    			loading(false);
	    			//console.log(response.recibos);
					$("#tablaDesglose").append(response.recibos);
					$(".currencyFormatSKU").number( true, 2 );
					
					
					if($("#track_asset").is(":checked") == false){
						$(".cMayor").prop("disabled",true);
						$(".cMenor").prop("disabled",true);
					}
					
	    		},
	    		error : function(e) {
	    			loading(false);
	    			console.log('Error: ' + e);
	    		},
	    	});  
	  
  }
  
  
  function removerArticulos(recibo){
	  
	 console.log("el recibo: "+recibo); 
	 var id2 = "aid_"+recibo;
     $("."+id2).remove();
     
	 calculateLine();
	 
     sumSbt();

	 
  }
  
  function cambioOrdenCompra(val){
	  
		$("#cambiarxml_button_cancelar").hide();
		$("#solicitud_button_cancelar").hide();
		$("#selecciono_ord_compra").hide();
		$("#selecciono_ord_compra_cancelar").hide();
		$("#cancelar_ord_compra_").hide();
		$("#cancelar_ord_compra_cancelar_").hide();
		$("#cambiar_ord_compra").show();
		$("#cambiar_ord_compra_cancelar").show();
		
				
			  
	  $("#mensaje-dialogo").text(SELECCIONO_OTRA_ORDEN_COMPRA);
		$("#modal-solicitud").modal({
			backdrop : 'static',
			keyboard : false
		});
		
	  
	  
  }
  
  
  function  seleccionaTodo(checkbox){
	  
	  
	  /*ANTES DE LA FUNCIINALIDAD DE AGREGAR TODOS LIMPIAR.*/
	  $(".recibosOrd").each(function( index ) {
		  $(this).prop("checked", false);
		  removerArticulos($(this).val());
	  });
	  
	
	  
	  console.log("checkbox todo: "+checkbox);
	  var ordCompra = $("#idOrdenCompra").val();
	  
	  if($(checkbox).is(":checked")){
		  
		  $(".recibosOrd").each(function( index ) {
			  console.log("each rexibos : "+this);
			  $(this).prop("checked", true);
			  accionRecibos(this,ordCompra);
		  });
		  
	  }else{
		  
		  
		  $("#cambiarxml_button_cancelar").hide();
			$("#solicitud_button_cancelar").hide();
			$("#selecciono_ord_compra").hide();
			$("#selecciono_ord_compra_cancelar").hide();
			$("#cancelar_ord_compra_").hide();
			$("#cancelar_ord_compra_cancelar_").hide();
			$("#cambiar_ord_compra").hide();
			$("#cambiar_ord_compra_cancelar").hide();
			$("#deselecciona_recibo_ok").hide();
			$("#deselecciona_recibo_cancelar").hide();
			
			$("#deselecciona_todos_recibo_ok").show();
			$("#deselecciona_todos_recibo_cancelar").show();
			
			
			
			
				  
		  $("#mensaje-dialogo").text(DESELECCIONO_TODOS_RECIBOS);
			$("#modal-solicitud").modal({
				backdrop : 'static',
				keyboard : false
			});
		  
		 
		  

		  
	  }
	  
  }
  
  
function removerRecibo(recibo){
	
    $.ajax({
		type : "GET",
		cache : false,
		url : url_server+"/removeRecibo",
		async : false,
		data : "idRecibo=" + recibo + "&idSolicitud=" + idSolicitudGlobal ,
		success : function(response) {
			loading(false);
		},
		error : function(e) {
			loading(false);
			console.log('Error: ' + e);
		},
	});  
	
	
}

  
  function vistaOrdenCompra(){
	  
	    console.log("ocultar boton agregar ...............");
	  
		$("#boton_agregar").hide();
		$("#concepto_xml").hide();
		$("#borrar_linea").hide();
		$(".locaciones").prop("disabled",true);
		$(".aidAll").prop("disabled",true);
		$(".cMayor").prop("disabled",true);
		$(".cMenor").prop("disabled",true);
		$(".ccontable").prop("disabled",true);
		$(".conXML").hide();
		$(".th_po").show();
		$(".td_po").show();
	  
  }
  
  function checkIfOrdenCompra(){
	  
	    if($("#orden_compra").is(":checked")) {
	  	    
	    	$("#wrapper_recibos").show();
	    	$("#idOrdenCompra").prop("disabled", false);
	  	    getPOs();
	  	    deshabilitarPAR(true);
		    console.log("ocultar boton agregar check");
	  	    vistaOrdenCompra();
		
			
	    }else{
			console.log("Clcick and NOT checked");
	    	$("#wrapper_recibos").hide();
	    }
	  
  }
  
  function verificarOrdenesCompraAutomatico(tipoFactura){
	  
	     var idSolicitanteActual = 0;
	     var esSolicitante = 0;
	     	     
	     if($("#solicitante").is(":checked")){
	    	 esSolicitante = 1;
	    	 idSolicitanteActual = $("#idSolicitanteJefe").val();
	     }
	     
		 loading(true);
	  
	     $.ajax({
	    		type : "GET",
	    		cache : false,
	    		url : url_server+"/tieneOrdenes",
	    		async : false,
	    		data : "tipoFactura=" + tipoFactura + "&idSolicitante=" + idSolicitanteActual 
	    		+ "&esSolicitante=" + esSolicitante+ "&idSolicitud=" + idSolicitudGlobal+ "&idProveedor=" + $("#proveedor").val(),
	    		success : function(response) {
	    			loading(false);
	    			
	    			$("#tiene_orden_compra").val(response.tieneOrdenCompra);
	
	    		},
	    		error : function(e) {
	    			loading(false);
	    			console.log('Error: ' + e);
	    		},
	    	});  
	  
	  
  }
  
  
  function ocultarBotonesModal(){
	  
	  $("#cambiarxml_button_cancelar").hide();
	  $("#cambiarxml_button").hide();
	  $("#cancelar_button").hide();
	  $("#cambiar_solicitante_button").hide();
	  $("#selecciono_ord_compra").hide();
	  $("#cancelar_ord_compra_").hide();
	  $("#cancelar_ord_compra_cancelar_").hide();
	  $("#cambiar_ord_compra").hide();
	  $("#cambiar_ord_compra_cancelar").hide();
	  $("#selecciono_ord_compra_cancelar").hide();
	  $("#solicitud_button_cancelar").hide();
	  $("#deselecciona_recibo_ok").hide();
	  $("#deselecciona_recibo_cancelar").hide();
	  $("#deselecciona_todos_recibo_ok").hide();
	  $("#deselecciona_todos_recibo_cancelar").hide();
	  $("#tiene_orden_compra_aceptar").hide();
	  $("#tiene_orden_compra_cancelar").hide();
  }
  
  
  function cancelarOrdenCompra(){
	  
	  $("#seleccionaTodoRecibos").prop("checked", false);
		cleanDesglose();
		$("#modal-solicitud").modal("hide");
		$("#idOrdenCompra").prop("disabled", true);
		$("#track_asset").prop("disabled",false);
		limpiarPARInputs();
		$("#idOrdenCompra").val(null);
		$("#boton_agregar").show();
		$("#concepto_xml").show();
		$(".th_po").hide();
		$(".td_po").hide();
		$("#recibosList").empty();
		$("#borrar_linea").show();
		$("#numero_ord_compra_anterior").val(null);
		$("#numero_ord_compra_hi").val(null);
		enEdicionOrden = true;

		//sumSbt();
	  
  }
  
  
  /* orden de compra en sin xml :S*/
  
  function habilitaOrdenCompraSin(){
	  
	  var totalFactura = parseInt($("#strSubTotal").val());
	  
	  if($("#compania").val() != -1 && $("#strSubTotal").val() != null && totalFactura > 0){
		  
		  $("#orden_compra").prop("disabled",false);
		  
	  }else{
		  $("#orden_compra").prop("disabled",true);
	  }
	  
  }
  
  