<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.lowes.util.Etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	Etiquetas etiqueta = new Etiquetas("es");
%>
<%
	String errorHead = request.getParameter("errorHead");
%>
<%
	String errorBody = request.getParameter("errorBody");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

<jsp:include page="template/head.jsp" />

<style>
.panel-body {
	padding: 0px;
}

.show-grid {
	margin: 0px 0;
}

.hackCss {
	width: 100px;
	margin-left: 78px;
	margin-top: 13px;
}

.show-grid [class^=col-] {
	padding-top: 0;
	padding-bottom: 0px;
	border: 1px solid #FFF;
	background-color: #FFF !important;
}

.form-group {
	margin-bottom: 3px;
}

.trackAsset {
	
}

.removerFila {
	
}

.linea {
	font-weight: bold;
}

.locaciones {
	
}

.aidAll{
}
.cMayor{
}
.cMenor{
}

.ccontable {
	
}

.conceptogrid {
	
}

.formatNumber {
	
}

.numerico {
	
}

.sbtGrid{}

.deleterow {
	cursor: pointer
}
.checkbox {
	line-height: 31px;
}


</style>



</head>

<body>



	<div id="wrapper">

		<jsp:include page="template/nav_superior.jsp" />

		<!-- Page Content -->
		<form:form method="post" 
			id="conXMLForm"
			enctype="multipart/form-data"
			action="saveFacturaXML" 
			modelAttribute="facturaConXML">
			<form:hidden disabled="true" value="" path="idSolicitudSession" />
			<form:hidden disabled="true" value="${tipoSolicitud}" path="tipoSolicitud" />
			<form:hidden disabled="true" value="" path="tipoFactura" />
			<form:hidden id="tasaIVA" value="" path="tasaIVA" />
			<input id="numero_ord_compra_hi" type="hidden" value="">
			<input id="numero_ord_compra_anterior" type="hidden" value="">
			<input id="recibo_hi" type="hidden" value="">
			<input id="tiene_orden_compra" type="hidden" value="">
			
				
			<div id="page-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<h1 class="page-header">
								<c:if test="${tipoSolicitud eq 1}">
									<%=etiqueta.CONXML%>
								</c:if>
								<c:if test="${tipoSolicitud eq 2}">
									<%=etiqueta.SINXML%>
								</c:if>
							</h1>
						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container-fluid -->

				<div style="display: none;" id="error-alert"
					class="alert alert-danger fade in">
					<strong><span id="error-head"><%=etiqueta.ERROR%></span></strong>
					<span id="error-body"><%=etiqueta.COMPLETE%></span>
				</div>


				<div style="display: none;" id="ok-alert"
					class="alert alert-success fade in">
					<strong> <span id="ok-head"> </span>
					</strong> <span id="ok-body"> <%=etiqueta.SOLICITUD_GUARDADA%>
					</span>
				</div>
				
				<!-- Warning -->
				<div style="display: none;" id="alert-warning"
					class="alert alert-warning fade in">
					<strong> <span id="warning-head"><%=etiqueta.ATENCION%></span>
					</strong> <span id="warning-body"> </span>
				</div>

				<div class="panel panel-default">
					<div style="height: 54px;" class="panel-heading">
						<c:if test="${estadoSolicitud != 7}">
							
							<c:if test="${estadoSolicitud >= 1}">
								<c:if test="${estadoSolicitud eq 1 or estadoSolicitud eq 4 or model.usuario.getPuesto().getIdPuesto() eq 14}">
									<button id="adjuntar_archivo" style="margin-left: 10px; float: right;"
										type="button" class="btn btn-primary" data-toggle="modal" data-target="#anexarDoc">
										<%=etiqueta.DOCUMENTO_SOPORTE%>
									</button>
								</c:if>
							
								<button onclick="verDetalleEstatus(${facturaConXML.idSolicitudSession})" id="consultar_auth" 
									style="margin-left: 10px; float: right;" type="button"
									class="btn btn-success"><%=etiqueta.CONSULTAR_AUTORIZACION%>
								</button>
							</c:if>

							<c:if test="${estadoSolicitud eq 1 or estadoSolicitud eq 4}">
								<button onclick="enviarSolicitudProceso()" id="enviarSolicitud"
									style="margin-left: 10px; float: right;" type="button"
									class="btn btn-danger"><%=etiqueta.ENVIAR_AUTORIZACION%>
								</button>
							</c:if>	

							<c:if test="${estadoSolicitud eq 1 or estadoSolicitud eq 4}">
								<button id="cancelar_boton" onclick="cancelar()"
									${facturaConXML.idSolicitudSession > 0 ? '' : 'disabled'}
									style="margin-left: 10px; float: right;" type="button"
									class="btn btn-warning"><%=etiqueta.CANCELAR%>
								</button>
							</c:if>

							<c:if test="${estadoSolicitud eq 0 or estadoSolicitud eq  1 or estadoSolicitud eq 4}">
								<button id="save_button" onclick="return valTodo();"
									style="margin-left: 10px; float: right;" type="submit"
									class="btn btn-default"><%=etiqueta.GUARDAR%>
								</button>
							</c:if>
							
						</c:if>
					</div>
					<div style="margin:4px;" class="panel-body">
					   <c:if test="${isSolicitante eq 1}">
					   

							<c:choose>
								<c:when	test="${facturaConXML.idSolicitanteJefe ne usuarioSession.idUsuario}">
								
									<div class="row show-grid col-md-3">
										<div class="col-xs-12 col-md-12">
											<div class="col-xs-12">
												<div class="col-xs-12">
													<div class="form-group">
														<div class="checkbox fix-check" style="margin-bottom: 18px; bottom: 0px; margin-left: 0px;">
															<form:checkbox id="solicitante" path="solicitante"
																value="" /><%=etiqueta.ESPECIFICA_SOLICITANTE%>
														</div>
													</div>
												</div>
												<div class="col-xs-6"></div>
											</div>
											<div class="col-xs-6"></div>
										</div>
										<div class="col-xs-6 col-md-4"></div>
									</div>
								</c:when>
								</c:choose>
								
							         <div class="row">
							         	<div class="col-md-6" style="position: relative; top: 6px;">
								         	<label class="col-md-2" style="padding-right: 0px;top: 7px;"><%=etiqueta.SOLICITANTE%>:</label>
										    <div class="form-group col-md-8">
												<form:select path="idSolicitanteJefe"
													cssClass="form-control" disabled="true">
													<option title="<%=etiqueta.SELECCIONE%>" value="-1"><%=etiqueta.SELECCIONE%></option>
													<c:if test="${fn:length(lstUsuariosJefe) > 0}">
														<c:forEach items="${lstUsuariosJefe}"
															var="lstUsuariosJefe">
															<option
																${facturaConXML.idSolicitanteJefe == lstUsuariosJefe.idUsuario ? 'selected' : ''}
																value="${lstUsuariosJefe.idUsuario}">${lstUsuariosJefe.nombre}
																${lstUsuariosJefe.apellidoPaterno}
																${lstUsuariosJefe.apellidoMaterno}</option>
														</c:forEach>
													</c:if>
												</form:select>
											</div>
							         	</div>
									</div>
                        </c:if>
					</div>
					<div class="panel-body">
							<div id="form-info-fiscal" style="margin-left: 0px; border-top: 1px solid #ddd; padding-top: 8px;" class="row">
						       <div class="row show-grid">
                                
                                <div style="margin: 4px; width: 308px;" class="col-md-8">
                                    <div class="col-md-12">
                                       <div class="form-group">
												<label><%=etiqueta.ADJUNTAR_PDF%></label> 
												<input	${facturaConXML.idSolicitudSession > 0 ? 'disabled' : ''} id="pdf" name="file" type="file">
												<label class="file-selected"></label>
											</div>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                
						<c:if test="${tipoSolicitud eq 1}">
                                
                                 <div style="margin: 4px;" class="col-md-8">
                                    <div class="col-md-6">
                                     	<div class="form-group input-file">
												<label><%=etiqueta.ADJUNTAR_XML%></label> <input onchange="limpiarFactura();"
													${facturaConXML.idSolicitudSession > 0 ? 'disabled' : ''}
													id="file" name="file" type="file">
												<label class="file-selected"></label>
											</div>
                                    </div>
                                    <div class="col-md-6">
                                     <button
												${facturaConXML.idSolicitudSession > 0 ? 'disabled' : ''}
												id="validarXMLb" onclick="valXML()"
												style="margin-left: 0px; float: left;" type="button"
												class="btn btn-default"><%=etiqueta.VALIDAR_XML%></button>
											<img id="loaderXML" style="display: none;"
												src="${pageContext.request.contextPath}/resources/images/loader.gif" />
                                    </div>
                                 
                                </div>
                                
                                <div style="margin-left: 41px;" class="col-md-12">
                                   <div id="wrapCambiarXML" class="form-group">
										<div class="checkbox">
											<form:checkbox id="cambiarxml" path="cambiarxml" value="" /><%=etiqueta.CAMBIAR_FACTURA%>
										</div>
									</div>
                                </div>
                                
						</c:if>
                                
                            </div>
							</div>
						<!-- /.row (nested) -->
					</div>


					<!-- /.panel-body -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<c:if test="${tipoSolicitud eq 1}">
							<h4><%=etiqueta.REVISA_INFORMACION_FACTURA%>:</h4>
							</c:if>
							<c:if test="${tipoSolicitud eq 2}">
							<h4><%=etiqueta.INGRESE_INFORMACION_FACTURA%>:</h4>
							</c:if>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel-body">
										<div class="row show-grid">
											<div class="col-md-4">
												<div class="form-group">
													<label id="companiasLabel" data-toggle="tooltip" data-trigger="hover" data-placement="right" title=""><%=etiqueta.COMPANIA%>:</label>
													<form:select path="compania.idcompania" id="compania"
														disabled="false" cssClass="form-control noMercanciasTooltips">
														<option value="-1"><%=etiqueta.SELECCIONE%></option>
														<c:forEach items="${lstCompanias}" var="lstCompanias">
															<option
																${facturaConXML.compania.idcompania == lstCompanias.idcompania ? 'selected' : ''}
																value="${lstCompanias.idcompania}">${lstCompanias.descripcion}
															</option>
														</c:forEach>
													</form:select>
												</div>
											</div>
											<div class="col-xs-6 col-md-4">
												<div title="titlss" class="form-group">
													<label id="proveedorLabel" data-toggle="tooltip" data-trigger="hover" data-placement="right" title=""><%=etiqueta.PROVEEDOR%>:</label>
													<form:select  title="<%=etiqueta.SELECCIONE%>"  path="proveedor.idProveedor" id="proveedor"
														cssClass="form-control noMercanciasTooltips" disabled="false">
														<option   value="-1"><%=etiqueta.SELECCIONE%></option>
														<c:forEach items="${lstProveedores}" var="lstProveedores">
															<option
																${facturaConXML.proveedor.idProveedor == lstProveedores.idProveedor ? 'selected' : ''}
																value="${lstProveedores.idProveedor}">${lstProveedores.descripcion}
															</option>
														</c:forEach>
													</form:select>
												</div>
											</div>
											<div class="col-xs-6 col-md-2">
												<div class="form-group">
															<label><%=etiqueta.RFC%>:</label>
															<form:input readonly="true" id="rfcEmisor" path="rfcEmisor"
																cssClass="form-control" disabled="false" style="width:130px"
																value="${facturaConXML.rfcEmisor}" />
														</div>
											</div>
										</div>
										
										<div class="row show-grid">
											<div class="col-xs-5 col-md-2">
												<div class="form-group">
													<label><%=etiqueta.FOLIO%>:</label>
													<form:input id="folio" path="folio" cssClass="form-control"
														style="min-width:120px"
														disabled="false" value="${facturaConXML.folio}" maxlength="100" />
												</div>
											</div>
										    
										    <c:if test="${tipoSolicitud eq 1}">
											<div class="col-xs-1 col-md-2">
													<div class="form-group">
														<label><%=etiqueta.SERIE%>:</label>
														<form:input id="serie" path="serie"
															cssClass="form-control" disabled="false"
															value="${facturaConXML.serie}" maxlength="100" />
													</div>
											</div>
											</c:if>
											
											<c:if test="${tipoSolicitud eq 1}">
														<div class="col-xs-6 col-md-4">
														<div class="form-group">
															<label><%=etiqueta.FOLIO_FISCAL%>:</label>
															<form:input id="folioFiscal" path="folioFiscal"
																class="form-control" disabled="false"
																value="${facturaConXML.folioFiscal}" />

														</div>
												</div>
												</c:if>
												
											
											<div class="col-xs-4 col-md-2">
												<div id="sandbox-container" class="form-group">
													<label><%=etiqueta.FECHA%>:</label>
													<form:input readonly="true" id="fecha" path="fecha_factura"
														cssClass="form-control" disabled="false"
														style="text-align:center"
														value="${facturaConXML.fecha_factura}" />
												</div>
											</div>
										</div>

										<div class="row show-grid">
											<div class="col-xs-12 col-md-2">
											<label><%=etiqueta.SUBTOTAL%>:</label>
													<div class="form-group input-group">
														<span class="input-group-addon">$</span>
														<form:input onblur="actualizarSubtotal();" id="strSubTotal" path="strSubTotal"
															cssClass="form-control numberFormat" disabled="false"
															style=""
															value="${facturaConXML.strSubTotal}" />
													</div>
											</div>
											
											<c:if test="${tipoSolicitud eq 1}">
											<div class="col-xs-12 col-md-2" style="">
												<label><%=etiqueta.IVA%>:</label>
												<div class="form-group input-group">													
													<span class="input-group-addon">$</span>
													<form:input id="iva" path="strIva"
														cssClass="form-control numerico" disabled="false"

														value="${facturaConXML.strIva}" />
												</div>
											</div>
							                </c:if>
											
											<c:if test="${tipoSolicitud eq 1}">
												<div class="col-xs-12 col-md-2" style="">
													<label><%=etiqueta.IEPS%>:</label>
													<div class="form-group input-group">													
														<span class="input-group-addon">$</span>
														<form:input path="strIeps" id="ieps" disabled="false"
															cssClass="form-control numerico"
															value="${facturaConXML.strIeps}" />
													</div>
												</div>
											
											
												<div class="col-xs-12 col-md-2">
													<label><%=etiqueta.DESCUENTO%>:</label>
													<div class="form-group input-group">
														<span class="input-group-addon">$</span>
														<form:input id="descuento" path="descuento"
															class="form-control numerico" disabled="true"
															style="width:100%;"
															value="${facturaConXML.descuento}" />
													</div>
												</div>
											</c:if>
											 
											<div class="col-xs-12 col-md-2">
												<label><%=etiqueta.TOTAL%>:</label>
												<div class="form-group input-group">
													<span class="input-group-addon">$</span>
													<form:input onblur="cloneTotalAmount()" id="total" path="strTotal"
														class="form-control numerico importe-total" disabled="false"
														style="width:100%;"
														value="${facturaConXML.strTotal}" />
												</div>
											 </div>
											
										</div>
										
										<!-- Opcionales -->
											<div class="row show-grid">
												
												<c:if test="${tipoSolicitud eq 1}">	
								                    <div class="col-xs-3 col-md-2">
														<label><%=etiqueta.IVA_RETENIDO%>:</label>
														<div class="form-group input-group">													
															<span class="input-group-addon">$</span>
															<form:input path="strIva_retenido" id="iva_retenido"
																disabled="false" cssClass="form-control numerico"
																style=""
																value="${facturaConXML.strIva_retenido}" />
														</div>
													</div>
											
													<div class="col-xs-3 col-md-2" style="">
														<label><%=etiqueta.ISR_RETENIDO%>:</label>
														<div class="form-group input-group">
															<span class="input-group-addon">$</span>
															<form:input path="strIsr_retenido" id="isr_retenido"
																disabled="false" cssClass="form-control numerico"
																style=""
																value="${facturaConXML.strIsr_retenido}" />
														</div>
													</div>
													
													<div class="col-xs-3 col-md-2" style="">
														<label><%=etiqueta.FORMA_PAGO%>:</label>
														<div class="form-group">
															<form:input path="formaPago" id="forma_pago"
																disabled="true" cssClass="form-control"
																style=""
																value="${facturaConXML.formaPago}" />
														</div>
													</div>
												
													<div class="col-xs-3 col-md-2" style="">
														<label><%=etiqueta.METODO_PAGO%>:</label>
														<div class="form-group">
															<form:input path="metodoPago" id="metodo_pago"
																disabled="true" cssClass="form-control"
																style=""
																value="${facturaConXML.metodoPago}" />
														</div>
													</div>
												</c:if>
												
												<div class="col-xs-12 col-md-2">
													<label><%=etiqueta.MONEDA%>:</label>
													<div class="form-group">
														<form:select id="moneda" path="moneda.idMoneda" style="height: 24px;"
															cssClass="form-control"
															disabled="false">
															<option value="-1"><%=etiqueta.SELECCIONE%></option>
															<c:forEach items="${lstMoneda}" var="lstMoneda">
																<option
																	${facturaConXML.moneda.idMoneda == lstMoneda.idMoneda ? 'selected' : ''}
																	value="${lstMoneda.idMoneda}">${lstMoneda.descripcion}
															</option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											
											<div class="row show-grid">
												<div class="col-xs-12 col-md-2">
													<div style="margin-bottom: 0px; margin-top: 34px;"
														class="form-group">
														<div class="checkbox fix-check">
															<form:checkbox id="conRetenciones" path="conRetenciones"
																value="" /><label><%=etiqueta.CON_RETENCIONES%></label>
														</div>
													</div>
												</div>
											</div>
										
										
										<div class="row show-grid">
											
											<div class="col-xs-6 col-md-4"></div>
											
										</div>

										<div class="row show-grid" style="border-top: 1px dotted #ddd; padding-top: 12px;">
											<div style="margin-left: 0px !important;" class="col-xs-12 col-md-offset-10 col-md-2">
										
											</div>
											<div class="col-xs-6 col-md-4"></div>
											<div class="col-xs-6 col-md-4"></div>
										</div>


										<div class="row show-grid">
											<div class="col-md-12">
												<div class="form-group">
													<label style="margin-top: 5px;" for="comment"><%=etiqueta.CONCEPTO_DEL_GASTO%>:</label>
													<form:textarea maxlength="240" path="concepto" style="margin-bottom:10px; resize: vertical; text-transform:uppercase;"
														value="${facturaConXML.concepto}" cssClass="form-control"
														rows="5" placeholder="Límite 240 Caracteres" onkeyup="this.value = this.value.toUpperCase();" />
												</div>
											</div>
											<div class="col-md-4"></div>
										</div>

									</div>
								</div>
							</div>
							<!-- /.row (nested) -->
						</div>
					</div>

					<!-- PANEL INFERIOR -->

					<div style=" border-color: #fff;" class="panel panel-default">
						
							<div class="panel-body">

								<div style="  margin-left: 20px; margin-bottom: 10px;" class="row">
								
								    <div style="padding-right:0px; width: 10.666667%;" class="col-md-2">
									   <div class="form-group">
											<div style="float: right;" class="checkbox">
												<form:checkbox disabled="true"  id="orden_compra" path="orden_compra" value="" style=" line-height: 31px;"/>Orden Compra
											</div>
										</div>
									</div>
									
									<div class="col-md-2">
											<label id="ordenDeCompraLabel" data-toggle="tooltip"
												data-trigger="hover" data-placement="top" title=""></label>
											<form:select cssStyle="margin-top: 8px;" onchange="ordenCompraSelect(this.value,false)" path="idOrdenCompra"
												id="idOrdenCompra"
												cssClass="form-control noMercanciasTooltips"
												disabled="true">
												<option value="-1"><%=etiqueta.SELECCIONE%></option>
												<c:forEach items="${ordenesCompra}" var="lstOrdCompra">
													<option
														value="${lstOrdCompra.po}">${lstOrdCompra.po}
													</option>
												</c:forEach>
											</form:select>
									</div>
									
							<div id="wrapper_recibos" style="display: none; margin-right: 40px;" class="col-md-3">
								<div class="">
									<table style="margin-top: 7px;" class="table"
										id="tablaRecibos">
											<thead>
												<tr>
													<th style="text-align: center;" colspan="2">
													
													Recibos
													<input style="float: left;" type="checkbox" value ="" id="seleccionaTodoRecibos"  onchange="seleccionaTodo(this)" />
													
													</th>
													
												</tr>
												
												<tr><th style="text-align: center;">No.</th><th style="text-align: center;">Importe</th></tr>
											</thead>
											<tbody id="recibosList">
											    
											</tbody>
											
									</table>	
								</div>
							</div>
						  </div>
						</div>

						<div style="height: 54px" class="panel-heading">
							<h4 style="float: left;"><%=etiqueta.INGRESE_INFORMACION_DESGLOSE_FACTURA%>:</h4>
							<c:if test="${estadoSolicitud eq 0 or estadoSolicitud eq  1 or estadoSolicitud eq 4}">
								<button id="boton_agregar" onclick="addRowToTable()"
									style="margin-left: 10px; float: right;" type="button"
									class="btn btn-primary"><%=etiqueta.AGREGAR%></button>
							</c:if>

						</div>
						<div class="panel-body tableGradient" style="position:relative">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="tablaDesglose" style="max-height:343px; overflow-y:auto; display:block;">
									<thead >
										<tr>
											<th style="width:3%;"><%=etiqueta.LINEA%></th>
											<!-- <th class="th_po">No. Recibo</th>
											<th class="th_po">Cantidad</th>
											<th class="th_po">SKU</th>
											<th class="th_po">Descripción</th>
											<th class="th_po">Precio Unitario</th> -->
											<th style="width:10%;"><%=etiqueta.SUBTOTAL%></th>
											<th style="width:10%;"><%=etiqueta.LOCACION%></th>
											<th style="width:10%;"><%=etiqueta.CUENTA_CONTABLE_LBL%></th>
											<th id="concepto_xml" style="width:10%;">Descripción</th>
											<th style="width:7%;"><%=etiqueta.TRACK_ASSET%></th>
											<th style="width:10%;"><%=etiqueta.LIBRO_CONTABLE%></th>
											<th style="width:10%;"><%=etiqueta.PAR%></th>
											<th style="width:10%;"><%=etiqueta.AID%></th>
											<th style="width:10%;"><%=etiqueta.CATEGORIA_MAYOR%></th>
											<th style="width:10%;"><%=etiqueta.CATEGORIA_MENOR%></th>
											<th id="borrar_linea"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${facturaConXML.facturaDesgloseList}" varStatus="status" var="facturaDesgloseList">
											
											<form:hidden value="${facturaDesgloseList.id_tabla_po}"  path="facturaDesgloseList[${status.index}].id_tabla_po"/>
											
											<tr class="aid_${facturaDesgloseList.num_recibo}">
												<td class="linea" align="center" >${status.count}</td>
												
												<td class="td_po"><form:input cssStyle="text-align:right;" disabled="true" maxlength="30" cssClass="form-control ordCompraRow"
														path="facturaDesgloseList[${status.index}].num_recibo"
														value="${facturaDesgloseList.num_recibo}" /></td>
														
											    <td class="td_po"><form:input cssStyle="text-align:right;" disabled="true" cssClass="form-control ordCompraRow"
														path="facturaDesgloseList[${status.index}].cantidad"
														value="${facturaDesgloseList.cantidad}" /></td>
												
												<td class="td_po"><form:input disabled="true"  maxlength="40" cssClass="form-control ordCompraRow"
														path="facturaDesgloseList[${status.index}].sku"
														value="${facturaDesgloseList.sku}" /></td>
												
												<td class="td_po"><form:input disabled="true" maxlength="240" cssClass="form-control ordCompraRow"
														path="facturaDesgloseList[${status.index}].descripcion_articulo"
														value="${facturaDesgloseList.descripcion_articulo}" /></td>
												
												<td class="td_po"><form:input disabled="true" maxlength="100" cssStyle="text-align:right;" cssClass="form-control preciosUnitarios ordCompraRow"
														path="facturaDesgloseList[${status.index}].precio_unitario"
														value="${facturaDesgloseList.precio_unitario}" /></td>					
												
												<td >
												
												<!-- Default -->	
												<c:if test="${facturaConXML.orden_compra eq false}">
												
													<form:input  step="any"
														onblur="sumSbt()" 
														cssClass="form-control subtotales currencyFormat sbtGrid ordCompraRow"
														path="facturaDesgloseList[${status.index}].strSubTotal"
														value="${facturaDesgloseList.strSubTotal}" />
													</c:if>	
													
													<c:if test="${facturaConXML.orden_compra eq true}">
												
													<form:input disabled="true" step="any"
														onblur="sumSbt()" 
														cssClass="form-control subtotales ordCompraRow currencyFormat sbtGrid"
														path="facturaDesgloseList[${status.index}].strSubTotal"
														value="${facturaDesgloseList.strSubTotal}" />
													</c:if>	
														
												</td>

												<td ><form:select
												        onchange="actualizarCuentas(this.id)"
														path="facturaDesgloseList[${status.index}].locacion.idLocacion"
														cssClass="form-control locaciones ordCompraRow">
														<option value="-1"><%=etiqueta.SELECCIONE%></option>
														<c:forEach items="${lcPermitidas}" var="llist">
															<option
																${facturaDesgloseList.locacion.idLocacion == llist.idLocacion ? 'selected' : ''}
																value="${llist.idLocacion}">${llist.numeroDescripcionLocacion}
															</option>
														</c:forEach>
													</form:select>
													
													</td>

												<td ><form:select
														path="facturaDesgloseList[${status.index}].cuentaContable.idCuentaContable"
														cssClass="form-control ccontable">
														
														<option value="-1"><%=etiqueta.SELECCIONE%> ${fn:length(facturaDesgloseList.cuentasContables)}  ${ccNoComprobante.idCuentaContable} </option>
														
													    <c:forEach items="${facturaDesgloseList.cuentasContables}" var="cclist2">
															<option
																${facturaDesgloseList.cuentaContable.idCuentaContable == cclist2.idCuentaContable ? 'selected' : ''}
																value="${cclist2.idCuentaContable}">${cclist2.numeroDescripcionCuentaContable}</option>
													   </c:forEach>
														   
													</form:select></td>

													<td><form:input maxlength="240" cssClass="form-control conceptogrid"
														path="facturaDesgloseList[${status.index}].concepto"
														value="${facturaDesgloseList.concepto}" /></td>
													
													<td style="text-align: center;">
														<form:checkbox
															id= "facturaDesgloseList${status.index}.trackAsset"
															path= "facturaDesgloseList[${status.index}].trackAsset" 
															onclick="activeTrackAssetOptions(${status.index});"
														/>
													</td>
													
													<td>
														<form:select
															path="facturaDesgloseList[${status.index}].libroContable.idcompania"
															cssClass="form-control"
															onchange="selectLibroContable(this.value, ${status.index})"
															disabled="${!facturaDesgloseList.isTrackAsset()}" >
															<option value="-1"><%=etiqueta.SELECCIONE%></option>
															<c:forEach items="${lstCompanias}" var="lstCompanias">
																<c:if test="${lstCompanias.esContable == 1}">
																	<div>${lstCompanias.idcompania}</div>
																	<option
																		${facturaDesgloseList.libroContable.idcompania == lstCompanias.idcompania ? 'selected' : ''}
																		value="${lstCompanias.idcompania}">${lstCompanias.descripcion}
																	</option>
																</c:if>
															</c:forEach>
														</form:select>
													</td>
													
													<td>
														<form:input type="text" cssClass="form-control"
														path="facturaDesgloseList[${status.index}].par"
														value="${facturaDesgloseList.par}"
														disabled="${!facturaDesgloseList.isTrackAsset()}" />
													</td>
													
													<td><form:select
															path="facturaDesgloseList[${status.index}].aid.idAid"
															cssClass="form-control"
															onchange="alSeleccionarAID(this.value, ${status.index})"
															disabled="${!facturaDesgloseList.isTrackAsset()}" >
															<option value="-1"><%=etiqueta.SELECCIONE%></option>
															<c:forEach items="${listAids}" var="aidlist">
																<option
																	${facturaDesgloseList.aid.idAid == aidlist.idAid ? 'selected' : ''}
																	value="${aidlist.idAid}">${aidlist.numeroDescripcionAid}
																</option>
															</c:forEach>
														</form:select></td>

													<td ><form:select
															path="facturaDesgloseList[${status.index}].categoriaMayor.idCategoriaMayor"
															cssClass="form-control"
															onchange="alSeleccionarCatMayor(this.value, ${status.index})"
															disabled="${!facturaDesgloseList.isTrackAsset()}" >
															<option value="-1"><%=etiqueta.SELECCIONE%></option>
															<c:forEach items="${listCatMayor}" var="listCatMayor">
																<option
																	${facturaDesgloseList.categoriaMayor.idCategoriaMayor == listCatMayor.idCategoriaMayor ? 'selected' : ''}
																	value="${listCatMayor.idCategoriaMayor}">${listCatMayor.descripcion}
																</option>
															</c:forEach>
														</form:select></td>

													<td ><form:select
															path="facturaDesgloseList[${status.index}].categoriaMenor.idCategoriaMenor"
															cssClass="form-control"
															disabled="${!facturaDesgloseList.isTrackAsset()}" >
															<option value="-1"><%=etiqueta.SELECCIONE%></option>
															<c:forEach items="${listCatMenor}" var="listCatMenor">
																<option
																	${facturaDesgloseList.categoriaMenor.idCategoriaMenor == listCatMenor.idCategoriaMenor ? 'selected' : ''}
																	value="${listCatMenor.idCategoriaMenor}">${listCatMenor.descripcion}
																</option>
															</c:forEach>
														</form:select></td>

												<td class="conXML">
													<button type="button" class="btn btn-danger removerFila"><%=etiqueta.REMOVER%></button>
											  	</td>

											</tr>
										</c:forEach>
									</tbody>
								</table>
								<div style="clear: both;"></div>
								<table style="width: 40%;"
									class="table table-striped table-bordered table-hover col-xs-0  col-md-offset-6"
									id="tablaDesgloseValores">
									<thead>
										<tr>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width: 50%"><%=etiqueta.SALDO_COMPROBADO%>:</td>
											<td>
												<input value="0.00" class="form-control currencyFormat importe-total" id="sbt"
													type="text" disabled="disabled">
											</td>
										</tr>

										<tr>
											<td style="width: 50%"><%=etiqueta.SALDO_PENDIENTE%>:</td>
											<td><input value="0.00" class="form-control currencyFormat importe-total"
												id="restante" type="text" disabled="disabled"></td>
										</tr>
										
										

 										<tr style="display: none;" >
											<td style="width: 50%"><%=etiqueta.SUBTOTAL%>:</td>
											<td><input value="${facturaConXML.strTotal}"
												class="form-control currencyFormat importe-total" id="sppc" type="text"
												disabled="disabled"></td>
										</tr> 

									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
					</div>
					<!-- /PANEL INFERIOR  -->
				</div>
			</div>
			<!-- /#page-wrapper -->
		</form:form>
		<!-- /Page Content -->

		<!-- Modal Warning Eliminar -->
		<div  id="modal-solicitud" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 style="float: left;">
							<span id="titulo-dialogo"><%=etiqueta.ATENCION%></span>
						</h4>
					</div>
					<div class="modal-body">

						<p id="mensaje-dialogo">
							<%=etiqueta.MENSAJE_DIALOGO_CON_XML%>
						</p>
					</div>
					<div class="modal-footer">

						<a style="display: none;" href="#" id="cambiarxml_button"
							class="btn btn-danger" role="button"><%=etiqueta.DE_ACUERDO%>
						</a>
						
						<a style="display: none;" href="#" id="cancelar_button"
							class="btn btn-danger" role="button"><%=etiqueta.DE_ACUERDO_CANCELAR%>
						</a> 
						
						<a style="display: none;" href="#" id="cambiar_solicitante_button" class="btn btn-danger"
							role="button"><%=etiqueta.DE_ACUERDO_CAMBIAR_SOLICITANTE%>
						</a>
						
							
						<a style="display: none;" href="#" id="selecciono_ord_compra" class="btn btn-danger"
							role="button"><%=etiqueta.ACEPTAR%></a>
			
						 <a style="display: none;" href="#"	id="selecciono_ord_compra_cancelar" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a> 	 	
							
					    <a href="#"	id="cambiarxml_button_cancelar" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a> 
							
						<a href="#"	style="display: none;" id="solicitud_button_cancelar"
							class="btn btn-default" role="button"><%=etiqueta.CANCELAR%></a>
							
							
						<a style="display: none;" href="#" id="cambiar_ord_compra" class="btn btn-danger"
							role="button"><%=etiqueta.ACEPTAR%></a>	
							
						<a style="display: none;" href="#"	id="cambiar_ord_compra_cancelar" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a>
								
							
						 <a style="display: none;" href="#" id="cancelar_ord_compra_cancelar_" class="btn btn-danger"
							role="button"><%=etiqueta.ACEPTAR%></a>
							
						 <a style="display: none;" href="#"	id="cancelar_ord_compra_" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a>	
							
						
						 <a style="display: none;" href="#" id="deselecciona_recibo_ok" class="btn btn-danger"
							role="button"><%=etiqueta.ACEPTAR%></a>
							
						<a style="display: none;" href="#"	id="deselecciona_recibo_cancelar" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a>	
							
							
					    <a style="display: none;" href="#" id="deselecciona_todos_recibo_ok" class="btn btn-danger"
							role="button"><%=etiqueta.ACEPTAR%></a>
							
						<a style="display: none;" href="#"	id="deselecciona_todos_recibo_cancelar" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a>
							
						
						<a style="display: none;" href="#" id="tiene_orden_compra_aceptar" class="btn btn-danger"
							role="button"><%=etiqueta.ACEPTAR%></a>
							
						<a style="display: none;" href="#"	id="tiene_orden_compra_cancelar" class="btn btn-default"
							role="button"><%=etiqueta.CANCELAR%></a>	
							
									

					</div>
				</div>

			</div>
		</div>

		    	<!-- Modal Documentos de soporte -->
    	<jsp:include page="modalArchivosSolicitud.jsp" />


    <jsp:include page="modalConsultarAutorizacion.jsp" />

	</div>

	<jsp:include page="template/includes.jsp" />

	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/locales/bootstrap-datepicker.es.min.js"
		charset="UTF-8"></script>

	<script>
		
		var idSolicitudGlobal = '${facturaConXML.idSolicitudSession}';
		var tipoSolicitudGlobal = '${tipoSolicitud}';
		var idStatus = '${estadoSolicitud}';
		var hasmod = false;
		var especificaSol = '${isSolicitante}';
		var idOrdenCompraGlobal = '${facturaConXML.idOrdenCompra}';
		var tipoFacturaGlobal = '${facturaConXML.tipoFactura}';
		var masMenosglobal = '${valorMasMenos}';
		var parametroCContable = '${parametroCContable}';
		var parametroCmayor = '${parametroCmayor}';
		var parametroCmaenor = '${parametroCmaenor}';
        var enEdicionOrden = false;

		
		</script>
	<script src="${pageContext.request.contextPath}/resources/js/jqueryFormat.js"></script>

	<script>
	
	var isOrdenCompra = '${facturaConXML.orden_compra}';

		   function valTodo(){
			   $(window).unbind('beforeunload');
			   var error = false;
			   var pdfError = false;
			   var errorConcepto = false;
			   var errorFolio = false;
			   var errorFolioDuplicado = false;
			   var trackAsAssetError = false;
			   var saldo_negativo = false;
			   var idSolicitud = '${facturaConXML.idSolicitudSession}';
			   
			   //if(idSolicitudGlobal == 1){
			   //  $("#par").removeClass("errorx");
			   //  $("#id_compania_libro_contable").removeClass("errorx");
		       // }
				
			   if ($("#tablaDesglose > tbody > tr").length == 0) {
					$("#tablaDesglose").addClass("errorx");
					error = true;
				} else {
					$("#tablaDesglose").removeClass("errorx");
				}
			   
			   var length = $("#tablaDesglose > tbody > tr").length;
			   
			   if(length > 0){
				   
			    $('.subtotales').each(function() {
			    	if($(this).val() == "" || $(this).val() == 0){
			    		$(this).addClass("errorx");
			    		error = true;
			    	}else{
			    		$(this).removeClass("errorx");
			    	}
				});
			    
			    $('.locaciones').each(function() {
			    	if($(this).val() == -1){
			    		$(this).addClass("errorx");
			    		error = true;
			    	}else{
			    		$(this).removeClass("errorx");
			    	}
				});
			    
			    
			    $('.ccontable').each(function() {
			    	if($(this).val() == -1){
			    		$(this).addClass("errorx");
			    		error = true;
			    	}else{
			    		$(this).removeClass("errorx");
			    	}
				});
			    
			    if($("#concepto").val() == ""){
					 $("#concepto").addClass("errorx");
					 error = true;
				}else if($("#concepto").val().length > 240){
					$("#concepto").addClass("errorx");
					error = true;
					errorConcepto = true;
				}else{
					$("#concepto").removeClass("errorx");
				}
			    
			    $('.conceptogrid').each(function() {
			    	if($(this).val() == ""){
			    		$(this).addClass("errorx");
			    		error = true;
			    	}else{
			    		$(this).removeClass("errorx");
			    	}
				});
			    
			   }
			   
			   //VALIDACIONES.
			   if($("#solicitante").is(":checked")){
				   if($("#idSolicitanteJefe").val() == -1){
					   $("#idSolicitanteJefe").addClass("errorx");
					   error = true;
				   }else{
					   $("#idSolicitanteJefe").removeClass("errorx");
				   }
			   }
			   
			   if($("#conRetenciones").is(":checked")){
				   
				   if($("#isr_retenido").val() == ""){
					   $("#isr_retenido").addClass("errorx");
					    error = true;
				   }else{
					   $("#isr_retenido").removeClass("errorx");
				   }
				   
				   if($("#iva_retenido").val() == ""){
					   $("#iva_retenido").addClass("errorx");
					    error = true;
				   }else{
					   $("#iva_retenido").removeClass("errorx");
				   }
			   }else{
				   $("#isr_retenido").removeClass("errorx");
				   $("#iva_retenido").removeClass("errorx");
			   }
			   
			   if($("#moneda").val() == -1){
				   $("#moneda").addClass("errorx");
				   error = true;
			   }else{
				   $("#moneda").removeClass("errorx");
			   }
			   
			   if(tipoSolicitudGlobal == 0){
				   if(document.getElementById("file").files.length == 0){
					   $("#file").addClass("errorx");
					   error = true;
				   }else{
					   $("#file").removeClass("errorx");
				   }
				   
				   
				   if(document.getElementById("pdf").files.length == 0){
					   $("#pdf").addClass("errorx");
					   error = true;
				   }else{
					   $("#pdf").removeClass("errorx");
				   }
		      }
			   
			   
			   if($("#compania").val() == -1){
				   $("#validarXMLb").addClass("errorx");
				   error = true;
			   }else{
				   $("#validarXMLb").removeClass("errorx");
			   }
			   
			   //validarPDF
			  /*  if(validarPDF() == false){
				   
				   $("#pdf").addClass("errorx");
			   }else{
				   $("#pdf").removeClass("errorx");
			   } */
		
			   
				if(idSolicitudGlobal == 0){   
					var ext = $('#pdf').val().split('.').pop().toLowerCase();
					if(ext != "pdf"){
					   error = true;
					   pdfError = true;
					   $("#pdf").addClass("errorx");
					}else{
					   $("#pdf").removeClass("errorx");
					}
				}
			   
			   //Se comentan ya que en CFDI 3.3 ya no es requerido SF
			   //if(tipoSolicitudGlobal == 1){
				//if($("#folio").val() == ""){
				//	 $("#folio").addClass("errorx");
				//	   error = true;
				//}
				if($("#folio").val().length > 100){
					 $("#folio").addClass("errorx");
					 error = true;
					 errorFolio = true;
				}else{
					 $("#folio").removeClass("errorx");
				}
			   
				   
			   /*if($("#track_asset").is(":checked")){
				   
				   console.log("entro a la validacion de track as asset");
				   
				   if($("#id_compania_libro_contable").val() == -1){
					   $("#id_compania_libro_contable").addClass("errorx");
					   error = true;
				   }else{
					   if($("#id_compania_libro_contable").val() != $("#compania").val()){
						   $("#id_compania_libro_contable").addClass("errorx");
						   error = true;
						   trackAsAssetError = true;
					   }else{
						   $("#id_compania_libro_contable").removeClass("errorx");
					   }
				   }
				   
				   if($("#par").val() == ""){
					   $("#par").addClass("errorx");
					   error = true;
				   }else{
					   $("#par").removeClass("errorx");
				   }

				   $('.aidAll').each(function() {
				    	if($(this).val() == -1){
				    		$(this).addClass("errorx");
				    		error = true;
				    	}else{
				    		$(this).removeClass("errorx");
				    	}
					});

				    $('.cMayor').each(function() {
				    	if($(this).val() == -1){
				    		$(this).addClass("errorx");
				    		error = true;
				    	}else{
				    		$(this).removeClass("errorx");
				    	}
					});
					
				    $('.cMenor').each(function() {
				    	if($(this).val() == -1){
				    		$(this).addClass("errorx");
				    		error = true;
				    	}else{
				    		$(this).removeClass("errorx");
				    	}
					});
				   
			   }*/
			   
			   //Validaciones SINXML
			   if(tipoSolicitudGlobal == 2){
				
				 if($("#compania").val() == - 1){
					  $("#compania").addClass("errorx");
					   error = true;
				 }else{
					   $("#compania").removeClass("errorx");
				 }  
				 
				 if($("#proveedor").val() == - 1){
					 $("#proveedor").addClass("errorx");
					   error = true;
				 }else{
					 $("#proveedor").removeClass("errorx");
				 }
				 
				 if($("#rfcEmisor").val() == ""){
					 $("#rfcEmisor").addClass("errorx");
					   error = true;
				 }else{
					 $("#rfcEmisor").removeClass("errorx");
				 }
				
				if($("#folio").val().length > 100){
					 $("#folio").addClass("errorx");
					 error = true;
					 errorFolio = true;
				}else{
					 $("#folio").removeClass("errorx");
				}
				 
				 if($("#fecha").val() == ""){
					 $("#fecha").addClass("errorx");
					   error = true;
				 }else{
					 $("#fecha").removeClass("errorx");
				 }
				 
				 if($("#strSubTotal").val() == ""){
					 $("#strSubTotal").addClass("errorx");
					   error = true;
				 }else{
					 $("#strSubTotal").removeClass("errorx");
				 }

				 if($("#total").val() == ""){
					 $("#total").addClass("errorx");
					   error = true;
				 }else{
					 $("#total").removeClass("errorx");
				 }
			
			  }
			  
			 	//Validacion Desgloce Track Asset
			 	$("#tablaDesglose tbody tr:visible").each(function(index, tr){
			 		
			 		if(tr){
			 			//Reset
			 			$(tr).find("td input:text,select").removeClass("errorx");
			 			
			 			//Columns
			 			subtotal = $(tr).find("td:eq(6) input").val();
			 			locacion = $(tr).find("td:eq(7) select option:selected").val();
			 			cContable = $(tr).find("td:eq(8) select option:selected").val();
			 			concepto = $(tr).find("td:eq(9) input").val();
			 			
			 			trackAsset = $(tr).find("td:eq(10) input").is(":checked");
			 			lContable = $(tr).find("td:eq(11) select option:selected").val();
			 			par = $(tr).find("td:eq(12) input").val();
			 			aid = $(tr).find("td:eq(13) select option:selected").val();
			 			cMayor = $(tr).find("td:eq(14) select option:selected").val();
			 			cMenor = $(tr).find("td:eq(15) select option:selected").val();
			 			
			 			if(trackAsset){
			 				if(lContable == "-1" || lContable == -1){
			 					$(tr).find("td:eq(11) select").addClass("errorx");
			 					error = true;
			 				}
			 				if(!par){
			 					$(tr).find("td:eq(12) input").addClass("errorx");
			 					error = true;
			 				}
			 				if(aid == "-1" || aid == -1){
			 					$(tr).find("td:eq(13) select").addClass("errorx");
			 					error = true;
			 				}
			 				if(cMayor == "-1" || cMayor == -1){
			 					$(tr).find("td:eq(14) select").addClass("errorx");
			 					error = true;
			 				}
			 				if(cMenor == "-1" || cMenor == -1){
			 					$(tr).find("td:eq(15) select").addClass("errorx");
			 					error = true;
			 				}
			 			}
			 		}
			 	});
			 	
				//Valida Subtotal
				if(error == false){
					if(Math.abs($("#total").val()) != Math.abs($("#sbt").val())){
						$("#alert-warning").hide();
						$("#restante").addClass("errorx");
						saldo_negativo = true;
						error = true;
					}
				}

				//Validar Folio Unico
				if(error == false){
					var folio = $("#folio").val(),
						proveedor_id = $("#proveedor option:selected").val();
					if(folio){
						if(uniqFolio(folio, proveedor_id) == false || uniqFolio(folio, proveedor_id) == "false"){
							errorFolioDuplicado = true;
							error = true;
						}
					}
				}
			   
			   if(error == true){
				   $("#error-head").text(ERROR);
				   if(pdfError){
					   $("#error-body").text(VALIDA_ARCHIVO_PDF);
				   }else if(trackAsAssetError){
					   $("#error-body").text(VALIDA_LIBRO_CONTABLE);
				   }else if(errorConcepto){
					   $("#error-body").text(CONCEPTO_EXCEDE);
				   }else if(errorFolio){
					   $("#error-body").text(FOLIO_EXCEDE);
				   }else if(errorFolioDuplicado){
					   $("#error-body").text(FOLIO_DUPLICADO);
				   }else if(saldo_negativo){
					   $("#error-body").text(ATENCION_SALDO_NEGATIVO);
				   }else{
					   $("#error-body").text(COMPLETE);
				   }
				   error_alert();
				   return false;
			   }else{
				   $("#enviarSolicitud").prop("disabled", false);
				   disabledEnabledFields(false);
				   loading(false);
				   return true;
			   }
			   
		   }
		   
			function uniqFolio(folio, proveedor_id){
				var result;
				var id_solicitud = idSolicitudGlobal;
				$.ajax({
					type : "GET",
					url : "${pageContext.request.contextPath}/validUniqFolio",
					async : false,
					data : "id_solicitud=" + id_solicitud + "&folio=" + folio+ "&proveedor_id=" + proveedor_id,
					success : function(is_valid) {
						if(is_valid == true || is_valid == "true"){
							result = true;
							$("#folio").removeClass("errorx");
						}else{
							result = false;
							$("#folio").addClass("errorx");
						}
					}
				});
				return result;
			}
		
		   function sumSbt(){
		 	   var sum = 0;
		 	  
			   console.log("----- suma");
			   
			   if($('.subtotales').length > 0){
					  $('.subtotales').each(function(){
						  if($.isNumeric($(this).val())){
					      var valorSubtotal = Math.abs($(this).val());  
					      console.log("MATH_ABS:"+valorSubtotal);
					      sum += parseFloat(valorSubtotal);
					   }else{
							  console.log("no es numerico : "+$(this));
							  $(this).val(0);
						  }
					  }); 
			   }else{
				      console.log("----- suma else");
				      sum = 0;
			   }
			   
			      /*calculo del saldo pendiente por comprobar*/
			      var subtotalFactura = parseFloat($("#sppc").val());
			      var sumaSubtotales = sum;
			      var saldoPendienteComprobar = subtotalFactura - sumaSubtotales;

			      //Warning Saldo Negativo
			      if(saldoPendienteComprobar < 0){
			    	  $("#restante").addClass("errorx");
					  $("#warning-body").text(ATENCION_SALDO_NEGATIVO);
					  alert_warning();
			      }else{
			    	  $("#restante").removeClass("errorx");
			      }
			      
			      $("#restante").val(saldoPendienteComprobar);
			      
			      /* Saldo comprobado = suma de subtotales de desglose */
			      $("#sbt").val(sum);
			     
			      
			      $("#restante").number( true, 2 );
			      $('#sbt').number( true, 2 );
			      $('#sppc').number( true, 2 );
			
		   }
		   
		   function addRowToTable(){
			   var idSolicitud = '${facturaConXML.idSolicitudSession}';
			   var length = $("#tablaDesglose > tbody > tr").length;
			   var estaEnEdicion = $("#cambiarxml").is(":checked");
			   var solicitante = $("#solicitante").is(":checked");
			   var tieneOrdCompra = $("#tiene_orden_compra").val();
			   console.log("tiene ordenes de compra: "+tieneOrdCompra);
			   
			   var solicitanteSeleccionado = true;
			   if(solicitante){
				  if($("#idSolicitanteJefe").val() == -1){
					  solicitanteSeleccionado = false;
				  } 
			   }
			   
			  if(solicitanteSeleccionado){
			    $("#idSolicitanteJefe").removeClass("errorx");
		      if(idSolicitud == 1){
				   if(idSolicitud == 0 && estaEnEdicion == false){
					   valFilesFiscales();
				   }else if(idSolicitud > 0 && estaEnEdicion == false){
					   callRow(length);
				   }else if(idSolicitud > 0 && estaEnEdicion == true){
					   valFilesFiscales()
				   }
		     }else{
		    	 var valorTotalAgregado  = parseInt($("#strSubTotal").val());
		    	 if($("#strSubTotal").val() == "" || valorTotalAgregado == 0){
		    		  $("#strSubTotal").addClass("errorx");
					  $("#error-head").text(ERROR);
					  $("#error-body").text(CAPTURE_SUBTOTAL);
		    		  error_alert();

		    	 }else{
					   if(tieneOrdCompra == "1"){
						   
						   ocultarBotonesModal();
						   $("#tiene_orden_compra_aceptar").show();
							$("#tiene_orden_compra_cancelar").show();
							$("#cambiarxml_button_cancelar").hide();


						   $("#mensaje-dialogo").text(TIENES_RECIBOS);
							$("#modal-solicitud").modal({
								backdrop : 'static',
								keyboard : false
							});
						   
						   
					   }else{
						   callRow(length);
					   }
		    	 }
		     }
		   }else{
			   // especifico solicitante pero no selecciono ninguno.
			   $("#error-head").text(ERROR);
			   $("#error-body").text(ESPECIFICA_SOLICITANTE);
			   $("#idSolicitanteJefe").addClass("errorx");
	    	   error_alert();

		   }
			   
		   }//adrowtable
		   
		   function valFilesFiscales(){
			   var length = $("#tablaDesglose > tbody > tr").length;

			   if(document.getElementById("file").files.length != 0 && $("#strSubTotal").val() != ""){
				   callRow(length);
			   }  else{
					  $("#file").addClass("errorx");
					  $("#error-alert").show();
					  $("#error-head").text(ERROR);
					  $("#error-body").text(COMPLETE);
			   }
		   }
		   
		 
		   function callRow(length){
			   
			   var idSolicitante = -1;
			   if(especificaSol == '1'){
				  idSolicitante = $('#idSolicitanteJefe option:selected').val();
			   }
			   
			   var conceptoFactura = $("#concepto").val();
			   
			   var tipoSolicitud = '${tipoSolicitud}';
			   loading(true);
				$.ajax({
					type : "GET",
					cache : false,
					url : "${pageContext.request.contextPath}/addRowConXML",
					async : true,
					data : "numrows=" + length + "&idSolicitud=" + tipoSolicitudGlobal + "&idSolicitante=" + idSolicitante 
					+ "&tipoSolicitud=" + tipoSolicitud + "&concepto=" + encodeURIComponent(conceptoFactura),
    				success : function(response) {
    					loading(false);
						$("#tablaDesglose").append(response);
						//valTrackAsset();
						calculateLine();
					
						
						$(".subtotales").last().number( true, 2 );
					},
					error : function(e) {
						loading(false);
						$("#error-head").text(ERROR);
						$("#error-body").text("No fue posible agregar desglose, contacte al administrador.");
						error_alert();
						console.log('Error: ' + e);
					},
				});
		   }
		   

		  $(document).ready(function(){
			  
			  $('#sandbox-container input').datepicker({
				  format: 'dd/mm/yyyy',
				  language: 'es',
				  endDate: '+0d',
				  autoclose: true,
				  todayHighlight: true
			  });
			  
			  //console.log("tipo solicitud: "+tipoSolicitudGlobal);
			  //console.log("estado solicitud: "+idStatus);
			  //console.log("id solicitud: "+idSolicitudGlobal);

			  
			  $('#proveedor').on('change', function() {
		    	    getRFC(this.value);
		    	    if(tipoSolicitudGlobal == 2){
		    	    	verificarOrdenesCompraAutomatico(1);
		    	    }
				});
			  
				  
			  $('.noMercanciasTooltips').on('change', function() {
		    	    refreshTooltip();
				});
			  
			  
			  $('.currencyFormat').number( true, 2 );
			  $('#strSubTotal').number( true, 2 );
			  $('#iva').number( true, 2 );
			  $('#iva_retenido').number( true, 2 );
			  $('#ieps').number( true, 2 );
			  $('#isr_retenido').number( true, 2 );
			  $('#total').number( true, 2 );
			  $('#descuento').number( true, 2 );
			
			 	$('.sbtGrid').on('blur', function() {
				 $("#sppc").val($('#strSubTotal').val());
				 sumSbt();
				});	
			  
			  $( ".currencyFormat" ).keyup(function() {
				  sumSbt();
				});
			  			 			   
			   sumSbt();
			   //valTrackAsset();
			   
			  //$("#track_asset").click(function(){
			  //  valTrackAsset();
			  //});
			  
			   var idSolicitud = '${facturaConXML.idSolicitudSession}';
			   
			   if(idSolicitud > 0){
				   $("#wrapCambiarXML").show();
			   }else{
				   $("#wrapCambiarXML").hide();
			   }
				
			   if(hasmod == true){
				   console.log("hasmod: "+hasmod);
				   $(window).bind('beforeunload', function(){
						return PERDERA_INFORMACION;
					}); 
			   }
			  
			   $('#tablaDesglose').on('click', ".removerFila", function(){
				    $(this).closest ('tr').remove ();
				    sumSbt();
				    calculateLine();
				}); 
			   
			   if(tipoSolicitudGlobal == 2){
					  disabledEnabledFields(false);
					  $("#idSolicitanteJefe").prop('disabled',true);
				  }else{
			          disabledEnabledFields(true);
				  }
			 
				  $("#cambiarxml").click(function(){
					  
					  ocultarBotonesModal();
					  
					  $("#cambiarxml_button").show();
					  $("#cambiarxml_button_cancelar").show();
					  
					  $("#mensaje-dialogo").text(MENSAJE_DIALOGO_CON_XML);
					  $("#modal-solicitud").modal({backdrop: 'static', keyboard: false});
					  $("#cambiarxml_button").show();
				  });
				 
				  $("#cambiarxml_button").click(function(){
					 limpiarFactura();
				  });
				  
				  $("#cancelar_button").click(function(){
					  
					  var id = '${facturaConXML.idSolicitudSession}';
					  	loading(true);
					     $.ajax({
					    		type : "GET",
					    		cache : false,
					    		url : "${pageContext.request.contextPath}/cancelarSolicitud",
					    		async : false,
					    		data : "idSolicitud=" + id,
					    		success : function(response) {
					    			loading(false);
					    	        if(response.resultado == 'true'){
					    				$(window).unbind('beforeunload');
					    	        	location.reload(true)
					    	        } 
					    		},
					    		error : function(e) {
					    			loading(false);
					    			console.log('Error: ' + e);
					    		},
					    	});  
				  });
				  
				 
				  
				  $("#cambiarxml_button_cancelar").click(function(){
					  $("#cambiarxml").prop("checked",false);
					  $("#modal-solicitud").modal("hide");
				  });
				  
				  
				  var isModificacion = '${facturaConXML.modificacion}';
				  var isCreacion = '${facturaConXML.creacion}';
				  
				  if(isModificacion == 'true'){
					  $("#ok-head").text(ACTUALIZACION);
					  $("#ok-body").text(INFORMACION_ACTUALIZADA);
					  ok_alert();
				  }
				  
				  if(isCreacion == 'true'){
					  $("#ok-head").text(NUEVA_SOLICITUD);
					  $("#ok-body").text(SOLICITUD_CREADA);
					  ok_alert();
				  }
				  
				  
				  $('#modal-solicitud').on('hidden.bs.modal', function () {
					   $("#cambiarxml_button").hide();
					   $("#cancelar_button").hide();
					   $("#cambiar_solicitante_button").hide();
					   $("#selecciono_ord_compra").hide();
					   $("#selecciono_ord_compra_cancelar").hide();
					   $("#cancelar_ord_compra_").hide();
					   $("#cancelar_ord_compra_cancelar_").hide();
					})
					  
				  
				  /*PARA activar y refrescar tooltip*/
				  refreshTooltip();
				  activeToolTip();
				
				   $("#isr_retenido").prop("disabled",true);
				   $("#iva_retenido").prop("disabled",true);
				   $("#conRetenciones").click(function(){
					   if(tipoSolicitudGlobal == 2){
					   $("#isr_retenido").prop("disabled",!$("#conRetenciones").is(":checked"));
					   $("#iva_retenido").prop("disabled",!$("#conRetenciones").is(":checked"));
					   }
				   });
				   
				   if(tipoSolicitudGlobal == 2){
						//$(".ccontable").prop('disabled', true);
						$("#fecha").addClass("backBlank");
					}else{
						$(".ccontable").prop('disabled', false);
					}
				   

					
				   fileUploadedName('#pdf');
				   fileUploadedName('#file');
				   $("#strSubTotal").prop('disabled', true);
				   
				   /* negativos si es tipo factura = nota credito*/
					      if(tipoFacturaGlobal == 2){
					    	  
						          $("#strSubTotal").val("-"+$("#strSubTotal").val());
						          $("#total").val("-"+$("#total").val());
						        
								  $('.subtotales').each(function(){
									  $(this).val("-"+$(this).val());
								  }); 
								  
								  $('.preciosUnitarios').each(function(){
									  $(this).val("-"+$(this).val());
								  }); 
								  
					        }
				   
				   
						   setStatusScreen(idStatus);

				   
				}); 
		  
		//Active Track Asset Options
		function activeTrackAssetOptions(id){
			trackAsset = $('input[id="facturaDesgloseList' + id + '.trackAsset"]');
			
			if(trackAsset){
				if($(trackAsset).is(":checked")){
					$('select[id="facturaDesgloseList' + id + '.libroContable.idcompania"]').removeAttr("disabled");
					$('input[id="facturaDesgloseList' + id + '.par"]').removeAttr("disabled");
					
				}else{
					//Reset Options
					$('select[id="facturaDesgloseList' + id + '.libroContable.idcompania"]').find("option:eq(0)").attr("selected", true);
					$('select[id="facturaDesgloseList' + id + '.libroContable.idcompania"]').attr("disabled", true);
					$('input[id="facturaDesgloseList' + id + '.par"]').val("").attr("disabled", true);
					$('select[id="facturaDesgloseList' + id + '.aid.idAid"]').find("option").not(":first").remove();
					$('select[id="facturaDesgloseList' + id + '.aid.idAid"]').attr("disabled", true);
					$('select[id="facturaDesgloseList' + id + '.categoriaMayor.idCategoriaMayor"]').find("option").not(":first").remove();
					$('select[id="facturaDesgloseList' + id + '.categoriaMayor.idCategoriaMayor"]').attr("disabled", true);
					$('select[id="facturaDesgloseList' + id + '.categoriaMenor.idCategoriaMenor"]').find("option").not(":first").remove();
					$('select[id="facturaDesgloseList' + id + '.categoriaMenor.idCategoriaMenor"]').attr("disabled", true);
				}
			}
		 }
		  
		  function cancelar(){
			  ocultarBotonesModal();
			  $("#cancelar_button").show();
			  $("#cambiarxml_button_cancelar").show();
			  $("#mensaje-dialogo").text(MENSAJE_CANCELACION_NOXML);
			  $("#modal-solicitud").modal({backdrop: 'static', keyboard: false});
			  $("#cancelar_button").show();
		  }
		  
		

		  function valXML(){
			  $("#folio").removeClass("errorx");

			  if(document.getElementById("file").files.length != 0){
				  $("#error-alert").hide();
				  
				  var data = new FormData();
				  jQuery.each(jQuery('#file')[0].files, function(i, file) {
				      data.append('file-'+i, file);
				  });
				  
				  loading(true);
				  if(data){
					  jQuery.ajax({
						  
				    		url : "${pageContext.request.contextPath}/resolverXML",
						    data: data,
						    cache: false,
						    contentType: false,
						    processData: false,
						    type: 'POST',
						    success: function(data){
						    	
						    	loading(false);
						    	
						    	if(data.faltaProveedor == 'false'){
								$("#loaderXML").hide();
					      	    $("#file").removeClass("errorx");

						        if(data.validxml == "true"){
						        	
						        	console.log("FACTURA VALIDA");
						        	
						        	$("#compania").val(data.idCompania);
						        	$("#proveedor").val(data.idProveedor);
						        	
						        	$("#moneda").val(data.idMoneda);
						        	
							        $("#folioFiscal").val(data.folioFiscal);
							        
							        // si tipo factura = nota credito
							        if(data.tipoFactura == 2){
								        $("#strSubTotal").val("-"+data.subTotal);
								        $("#total").val("-"+data.total);

							        }else{
								        $("#strSubTotal").val(data.subTotal);
								        $("#total").val(data.total);
							        }
							        $("#descuento").val("-"+data.descuento);
							        $("#restante").val(data.total);
							        $("#sppc").val(data.total);
							        
							        $("#rfcEmisor").val(data.rfcEmisor);
							        $("#serie").val(data.serie);
							        $("#folio").val(data.folio);
							        $("#iva").val(data.iva);
							        $("#ieps").val(data.ieps);
							        
							        //Validacion Conceptos 240 Caracteres
							        //if(data.warningConceptos == 'true'){
									//	$("#concepto").addClass("errorx");
									//	$("#warning-body").text(CONCEPTO_LIMITE_240);
									//	alert_warning();
							        //}
							        //$("#concepto").val(data.concepto);
							        
							        
							        $("#tipoFactura").val(data.tipoFactura);
							        $("#tasaIVA").val(data.tasaIva);
							        
							        if(data.incluyeRetenciones == 'true'){
							          $("#conRetenciones").prop("checked",true);	
							        }
							        
							        $("#iva_retenido").val(data.ivaRetenido);
							        $("#isr_retenido").val(data.isrRetenido);
							        $("#fecha").val(data.fechaEmision);
							        
									$("#orden_compra").prop("disabled", false);
									
									$("#forma_pago").val(data.formaPago);
									$("#metodo_pago").val(data.metodoPago);
									
							        if(data.wsmensaje != null){
							        	if(data.wscode == 0 || data.wscode == 34){
							        		$("#ok-head").text(FACTURA_VALIDA);
										    $("#ok-body").text(data.wsmensaje);
							        		ok_alert();
							        		
							        		
							        	}else{
							        	    $("#error-head").text(ERROR);
											$("#error-body").text(data.wsmensaje);
							        		error_alert();
							        	}
							        }
							        
							        
							        refreshTooltip();
									verificarOrdenesCompraAutomatico(data.tipoFactura);
						        }else{
						      	    $("#file").addClass("errorx");
								    $("#error-alert").show();
								    $("#error-head").text(ERROR);
								    $("#error-body").text(data.wsmensaje);
								    $("#file").val(null);
						        }
						      }else{
						    	  $("#file").addClass("errorx");
								    $("#error-alert").show();
								    $("#error-head").text(ERROR);
								    $("#error-body").text(NOPROVEEDOR);
								    $("#file").val(null);
						      }
						    }
						});
				  }
				  
			  }else{
				  $("#loaderXML").hide();
  				  $("#file").addClass("errorx");
				  $("#error-alert").show();
				  $("#error-head").text(ERROR);
				  $("#error-body").text(COMPLETE);
			  }
		  }
		  
		  
					function calculateLine() {
						$('#tablaDesglose tr').each(function(i, row) {
							if (i > 0) {
								var nlinea = $(this).find(".linea").text(i);
							}
						});
					}
				
					function disabledEnabledFields(state){
					    $("#compania").prop('disabled', state);
					    $("#proveedor").prop('disabled', state);
					    $("#rfcEmisor").prop('disabled', state);
					    $("#folioFiscal").prop('disabled', state);
					    //$("#serie").prop('disabled', state);
					    //$("#folio").prop('disabled', state);
					    $("#fecha").prop('disabled', state);
					    $("#strSubTotal").prop('disabled', state);
					    $("#moneda").prop('disabled', state);
					    $("#iva").prop('disabled', state);
					    $("#ieps").prop('disabled', state);
					    $("#total").prop('disabled', state);
					    $("#conRetenciones").prop('disabled', state);
					    $("#iva_retenido").prop('disabled', state);
					    $("#isr_retenido").prop('disabled', state);
					    $("#tipoSolicitud").prop('disabled',state);
					    $("#idSolicitanteJefe").prop('disabled',state);
						$(".ccontable").prop('disabled', state);
						$("#tipoFactura").prop('disabled', state);
						$("#forma_pago").prop('disabled', state);
						$("#metodo_pago").prop('disabled', state);
						
						/*Orden de compra*/
						if($("#orden_compra").is(":checked")){
							console.log("idSolicitudGlobal conxml : "+idSolicitudGlobal);
							console.log("orden de compra is enabled.");
							$("#orden_compra").prop('disabled', state);
							$("#idOrdenCompra").prop('disabled', state);
							//$("#track_asset").prop('disabled', state);
							$(".ordCompraRow").prop('disabled',state);
						}
						
						 
					    //if($("#track_asset").is(":checked")){
					    //	$("#par").prop('disabled',state);
					    //	$("#id_compania_libro_contable").prop('disabled',state);
						//	$(".cMayor").prop('disabled', state);
						//	$(".cMenor").prop('disabled', state);
					    //}
					}
					
	          function enviarSolicitudProceso(){
						 var idSolicitud = '${facturaConXML.idSolicitudSession}';
						if(validaEnvioProceso()){
							
							console.log("el valor de hasmod: "+hasmod);
							
							if(hasmod == false){
/*  						 	 if(tipoSolicitudGlobal == 2){
						 			loading(true);
								    $.ajax({
							    		type : "GET",
							    		cache : false,
							    		url : "${pageContext.request.contextPath}/revisarArchivosEnSolicitud",
							    		async : false,
							    		data : "idSolicitud=" + idSolicitud,
							    		success : function(response) {
							    			 loading(false);
							    			 console.log(response);
							    	         if(response.respuesta == 'true'){
							    	        	 enviaProceso(idSolicitud);
							    	         }else{
							    	        	 $("#error-head").text(ERROR);
												 $("#error-body").text(DOCUMENTO_SOPORTE_ANEXAR);
											     error_alert();
							    	         }
							    		},
							    		error : function(e) {
							    			loading(false);
							    			$("#error-head").text(ERROR);
											 $("#error-body").text(NO_SE_ENVIO);
										     error_alert();
							    			
							    		},
							    	}); 
							 }else{
								 enviaProceso(idSolicitud);
							 }  */
							 
								enviaProceso(idSolicitud);
							 
						}else{
							$("#error-head").text(ERROR);
							 $("#error-body").text(GUARDE_ENVIAR);
						     error_alert();
						}
						}
					 }
					
					function getRFC(idProveedor){
						if(idProveedor != -1){
							 var idSolicitud = '${facturaConXML.idSolicitudSession}';
							 loading(true);
				    	     $("#rfcEmisor").val(null);
						     $.ajax({
						    		type : "GET",
						    		cache : false,
						    		url : "${pageContext.request.contextPath}/getRFC",
						    		async : false,
						    		data : "idProveedor=" + idProveedor,
						    		success : function(response) {
						    			loading(false);
						    			$("#rfcEmisor").val(response);
						    		},
						    		error : function(e) {
						    			loading(false);
						    			console.log('Error: ' + e);
						    		},
						    	}); 
						   }else{
							   $("#rfcEmisor").val(null);
						   }
					 }
					
					
					function validaEnvioProceso(){
						
						var valido = true;
						var msj = null;
						
						var length = $("#tablaDesglose > tbody > tr").length;
						if(length > 0){
							if(validaGrid() == false){
							   if($("#restante").val() == 0){
								 $("#sppc").removeClass("errorx");
							     return true;
							   }else{
								   
								   if($("#orden_compra").is(":checked")) {
								   // si es una orden de comprar dar mas menos
								   
								   console.log("masMenosglobal: "+masMenosglobal);
								   
								   if($("#restante").val() > masMenosglobal || $("#restante").val() < -Math.abs(masMenosglobal)){
								        
									    $("#error-head").text(ERROR);
									    $("#error-body").text(SALDO_PENDIENTE_CERO);
									    $("#restante").addClass("errorx");
										error_alert();
										hasmod = true;
										return false; 
									   
								   }else{
									   
									   return true;
									   
								   }
									   
									   
								   }else{
									  
									   $("#error-head").text(ERROR);
									    $("#error-body").text(SALDO_PENDIENTE_CERO);
									    $("#restante").addClass("errorx");
										error_alert();
										hasmod = true;
										return false; 
									   
								   }

							   }
							}else{
								$("#error-head").text(ERROR);
							    $("#error-body").text(COMPLETE);
								error_alert();
								hasmod = true;
								return false;
							}
						}else{
							$("#error-head").text(ERROR);
						    $("#error-body").text(DESGLOSE_MINIMO);
							error_alert();
							hasmod = true;
							return false;
						}
					}
					
					function setStatusScreen(idStatus){
						if(idStatus > 0){
							if(idStatus > 1 && idStatus != 4){
								
								console.log("status screen");
								
						      $("#orden_compra").prop("disabled",true);
							  $("#form-info-fiscal").hide();
							  $("#solicitante").prop("disabled",true);
				     		  $("#concepto").prop("disabled",true);
							  //$("#track_asset").prop("disabled",true);
							  $(".subtotales").prop("disabled",true);
							  $(".trackAsset").prop("disabled",true);
							  $(".removerFila").prop("disabled",true);
							  $(".locaciones").prop("disabled",true);
							  $(".ccontable").prop("disabled",true);
							  $(".conceptogrid").prop("disabled",true);
							  $(".recibosOrd").prop("disabled",true);
							  
							  //restantes
							  disabledEnabledFields(true);
							  checkStatusBehavior(idStatus);
							  
							}
						}
					}
					
				
					function validaGrid(){
						
						var error = false;
						
					    $('.subtotales').each(function() {
					    	if($(this).val() == "" || $(this).val() == 0){
					    		$(this).addClass("errorx");
					    		error = true;
					    	}else{
					    		$(this).removeClass("errorx");
					    	}
						});
					    
					    $('.locaciones').each(function() {
					    	if($(this).val() == -1){
					    		$(this).addClass("errorx");
					    		error = true;
					    	}else{
					    		$(this).removeClass("errorx");
					    	}
						});
					    
					    
					    $('.ccontable').each(function() {
					    	if($(this).val() == -1){
					    		$(this).addClass("errorx");
					    		error = true;
					    	}else{
					    		$(this).removeClass("errorx");
					    	}
						});
					    
					    $('.conceptogrid').each(function() {
					    	if($(this).val() == ""){
					    		$(this).addClass("errorx");
					    		error = true;
					    	}else{
					    		$(this).removeClass("errorx");
					    	}
						});
					    
					    return error;
					}
					
					function check_numericos(){
						  $('.numerico').each(function(){
							  if($.isNumeric($(this).val()) == false){				  
								  $(this).val(0);
							  }
						  });
					}
					
				function cleanDesglose(){
					$('#tablaDesglose tr').each(function(i, row){
						if(i > 0){
						$(this).closest('tr').remove();
					    sumSbt();
					    calculateLine();
					    }
					});
					
					//$(".track_asset").prop("checked",false);
					//$("#par").prop('disabled', true);
					//$("#id_compania_libro_contable").prop('disabled', true);
					//$("#id_compania_libro_contable").val(-1);
					//$('select.trackAsset').each(function() {
					//	$( this ).val(-1);
					//});	
					//$("#par").val(null);
					$("#sbt").val(null);
					$("#restante").val(null);
					
				}	
				
			     function verDetalleEstatus(id) {
			         
			  		if(id > 0){
			          	console.log(id);
			          	  loading(true);
			              $.ajax({
			                  type: "GET",
			                  cache: false,
			                  url: "${pageContext.request.contextPath}/getEstatus",
			                  async: true,
			                  data: "intxnId=" + id,
			                  success: function(result) {
			                	loading(false);
			                  	$("#tablaDetalle").empty().append(result.lista);
			                  	//abrir ventana modal
			                  	$('#modalEstatus').modal('show'); 
			                  },
			                  error: function(e) {
			                	  loading(false);
			                      console.log('Error: ' + e);
			                  },
			              }); 
			              
			          }//if
			  	}
			     
			     //recibe el id del elemento
			     function actualizarCuentas(idLoc){
			    	 console.log("---idLocacion: "+idLoc);
			    	 
			    	 var id = idLoc;
			    	 var idLocacion = document.getElementById(id).value;
			    	 	loading(true);
			    	   $.ajax({
			                  type: "GET",
			                  cache: false,
			                  url: "${pageContext.request.contextPath}/seleccionLocacion",
			                  async: true,
								data : "idElemento=" + id + "&idLocacion=" + idLocacion+ "&tipoSolicitud=" + tipoSolicitudGlobal,
			                  success: function(result) {
			                	 loading(false);
			                  	 console.log(result.idElemento);
			                  	 document.getElementById(result.idElemento).innerHTML=result.options;
			                  	 document.getElementById(result.idElemento).disabled = false;
			                  	 console.log(result.options);
			                  },
			                  error: function(e) {
			                	  loading(false);
			                      console.log('Error: ' + e);
			                  },
			              }); 
			    	 
			     }			   
			     
			   
			     
			     function refreshTooltip(){
			    	 
			         var selectedOp = $("#proveedor option:selected").text();
			         var selectedOpComp = $("#compania option:selected").text();
			         //var selectedOpLibroCont = $("#id_compania_libro_contable option:selected").text();
				         
				         $('#proveedorLabel').tooltip('hide')
				          .attr('data-original-title', selectedOp)
				          .tooltip('fixTitle');
				         
				         $('#companiasLabel').tooltip('hide')
				          .attr('data-original-title', selectedOpComp)
				          .tooltip('fixTitle');
				         
				         //$('#libroContableLabel').tooltip('hide')
				         // .attr('data-original-title', selectedOpLibroCont)
				         // .tooltip('fixTitle');
			     }

			     function selectLibroContable(idLibroContable, linea_id){

				   $("select[id='facturaDesgloseList" + linea_id + ".aid.idAid']").attr("disabled", true);
				   $("select[id='facturaDesgloseList" + linea_id + ".categoriaMayor.idCategoriaMayor']").attr("disabled", true);
				   $("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").attr("disabled", true);
				   $("select[id='facturaDesgloseList" + linea_id + ".aid.idAid']").find("option").not(":first").empty();
				   $("select[id='facturaDesgloseList" + linea_id + ".categoriaMayor.idCategoriaMayor']").find("option").not(":first").empty();
				   $("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").find("option").not(":first").empty();
				   
				   if(idLibroContable != "-1"){
				       loading(true);
				  	   $.ajax({
			                  type: "GET",
			                  cache: false,
			                  url: "${pageContext.request.contextPath}/seleccionaLibroContable",
			                  async: true,
								data : "idLibro=" + idLibroContable,
			                  success: function(result) {
			                	     loading(false);
			                	     $("select[id='facturaDesgloseList" + linea_id + ".aid.idAid']").removeAttr("disabled");
			                	     $("select[id='facturaDesgloseList" + linea_id + ".aid.idAid']").empty().append(result.options);
			                  },
			                  error: function(e) {
			                	  loading(false);
			                      console.log('Error: ' + e);
			                  },
			              });
				   }
				 }
			     
			     
			     function alSeleccionarAID(idAID, linea_id){
					
					$("select[id='facturaDesgloseList" + linea_id + ".categoriaMayor.idCategoriaMayor']").attr("disabled", true);
					$("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").attr("disabled", true);
					$("select[id='facturaDesgloseList" + linea_id + ".categoriaMayor.idCategoriaMayor']").find("option").not(":first").empty();
					$("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").find("option").not(":first").empty();
					
					if(idAID != "-1"){
				    	var idLibro = $("select[id='facturaDesgloseList" + linea_id + ".libroContable.idcompania'] option:selected").val() || 0;
						loading(true);
						$.ajax({
			                  type: "GET",
			                  cache: false,
			                  url: "${pageContext.request.contextPath}/seleccionAID",
			                  async: true,
								data : "idLibro=" + idLibro + "&idAid=" + idAID,
			                  success: function(result) {
			                	     loading(false);
			                	     $("select[id='facturaDesgloseList" + linea_id + ".categoriaMayor.idCategoriaMayor']").removeAttr("disabled");
			                	     $("select[id='facturaDesgloseList" + linea_id + ".categoriaMayor.idCategoriaMayor']").empty().append(result.options);
			                  },
			                  error: function(e) {
			                	  loading(false);
			                      console.log('Error: ' + e);
			                  },
			            });
					}
			     }

               function alSeleccionarCatMayor(idCatMayor, linea_id){

					$("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").attr("disabled", true);
					$("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").find("option").not(":first").empty();
					
					if(idCatMayor != "-1"){
	            	   var idLibro = $("select[id='facturaDesgloseList" + linea_id + ".libroContable.idcompania'] option:selected").val() || 0;
	            	   var idAid = $("select[id='facturaDesgloseList" + linea_id + ".aid.idAid'] option:selected").val() || 0;
	            	   
			    	   loading(true);
			    	   $.ajax({
		                  type: "GET",
		                  cache: false,
		                  url: "${pageContext.request.contextPath}/seleccionCatMayor",
		                  async: true,
							data : "idCatMayor=" + idCatMayor + "&idAid=" + idAid + "&idLibro=" + idLibro,
		                  success: function(result) {
		                	     loading(false);
		                	     $("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").removeAttr("disabled");
		                	     $("select[id='facturaDesgloseList" + linea_id + ".categoriaMenor.idCategoriaMenor']").empty().append(result.options);
		                  },
		                  error: function(e) {
		                	  loading(false);
		                      console.log('Error: ' + e);
		                  },
		               });
					}
			     }
			     
			   
			     
			     function actualizarSubtotal(){
					 $("#sppc").val($('#strSubTotal').val());
					 sumSbt();
			     }
			     
			     $(".form-control").on('change', function() {
			    	  //console.log("cambio: "+$(this).val());
			    	  hasmod = true;
				 });
			     
			
				  function checkPar(){
					  if(!$.isNumeric($("#par").val())){				  
						  $("#par").val(null);
						}
				  }
				  
				  function cloneTotalAmount(){
					  $("#strSubTotal").val($("#total").val());
					  
					  if(tipoSolicitudGlobal == 2){
						  actualizarSubtotal();
						  habilitaOrdenCompraSin();
					  }
				  }
				  
				  
				  function limpiarFactura(){
					  
					    $("#cambiarxml").prop("checked",true);
					    $("#compania").val(-1);
			        	$("#proveedor").val(-1);
		        	    $("#moneda").val(-1);
					    $("#folioFiscal").val(null);
				        $("#total").val(null);
				        $("#strSubTotal").val(null);
				        $("#sppc").val(null);
				        $("#rfcEmisor").val(null);
				        $("#serie").val(null);
				        $("#folio").val(null);
				        $("#iva").val(null);
				        $("#ieps").val(null);
				        $("#iva_retenido").val(null);
				        $("#isr_retenido").val(null);
				        $("#fecha").val(null);
				        $("#concepto").val(null);
						$("#modal-solicitud").modal("hide");
						//$("#track_asset").prop("checked",false);
						//$("#id_compania_libro_contable").val(-1);
						//$("#par").val(null);
						//$("#par").prop('disabled', true);
						//$("#id_compania_libro_contable").prop('disabled', true);
						//$('select.trackAsset').each(function() {
						//	$( this ).val(-1);
						//});	
						
						$('#tablaDesglose tr').each(function(i, row){
							if(i > 0){
							$(this).closest('tr').remove();
						    sumSbt();
						    calculateLine();
						    }
						});
						
						$("#sbt").val(0);
						$("#restante").val(0);
						$("#validarXMLb").prop("disabled", false);
						$("#file").prop("disabled",false);
						$("#pdf").prop("disabled",false);
						
						
						if($("#orden_compra").is(":checked")){
						  cancelarOrdenCompra();
						  $("#orden_compra").prop("checked",false);
						  $("#orden_compra").prop("disabled",true);

						}
						
						
						
						
				  }
				  
				  
				  
				  //function actualizarCategorias(cuentaSeleccionada,numeroCatMayor){
						
					//	var num = parseInt($(cuentaSeleccionada).attr('id').match(/\d+/),10);
					//	console.log("cuenta contable seleccionada: "+num);
						
					//	var idCategoriaMayor = "facturaDesgloseList@.categoriaMayor.idCategoriaMayor";
					//	idCategoriaMayor = idCategoriaMayor.replace("@",num);
					//	console.log("cuentaSeleccionada: "+$(cuentaSeleccionada).val());
					//	console.log("parametroCContable: "+parametroCContable);
					  
 					 //   if($(cuentaSeleccionada).val() != parametroCContable){
 					    	
 					//		console.log("cuenta contable id : "+idCategoriaMayor);
					//        var element = document.getElementById(idCategoriaMayor);

 							
 					//	    var categoriasMayor = document.getElementById(idCategoriaMayor), trend, i;
 					//	    var categoriaMayorExiste = false;

 					//	     for(i = 0; i < categoriasMayor.length; i++) {
 					//	           categoriaMayor = categoriasMayor[i];
 					//	           if(categoriaMayor.value == parametroCmayor){
 					//	        	    element.value = parametroCmayor;
 					//	        	    categoriaMayorExiste = true;
 					//	        	    alSeleccionarCatMayor(idCategoriaMayor);
 					//	        	    break;
 					//	           }
 					//	     }
 						     
 					//	     if(categoriaMayorExiste == false){
					 //       	    element.value = -1;
 					//	     }
 						     
 						     
 					//	    element.disabled=true;

						  
					//    }else{
					//        var element = document.getElementById(idCategoriaMayor);
					//        if(numeroCatMayor == 1){
	 				//		    element.disabled=true;
					//        }else{
	 				//		    element.disabled=false;
					//        }
					//    }
					   
					  
				  //}
				  
			     
					
					</script>
	<!-- Scripts especificos ....  -->
	<script>
	
	 $('#conXMLForm').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    return false;
		  }
		});
	
	  $(document).ready(function(){
		  
		  url_server = '${pageContext.request.contextPath}';
		  hasmod = false;
	
	  });	  	  	
	</script>
	<script	src="${pageContext.request.contextPath}/resources/js/solicitudArchivos.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/manejarSolicitante.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/jquery.number.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/statusBehavior.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/enviarAProceso.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/ordenCompra.js"></script>
</body>
</html>
