<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.lowes.util.Etiquetas"%>
<%  Etiquetas etiqueta = new Etiquetas("es"); %>
<%  String errorHead = request.getParameter("errorHead"); %>
<%  String errorBody = request.getParameter("errorBody"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

	<jsp:include page="template/head.jsp" />
	
	<style>
		.remarcado {
			background-color: #eee !important;
		}
	
		.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
			background-color: #eee !important;
		}
		
		.check {
			
		}
		
		.check-back {
			background-color: #fff !important;
		}
		
		.busquedaForm{
			height:auto;
		}
	</style>

</head>

<body>

	<div id="wrapper">
		<jsp:include page="template/nav_superior.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header"><%= etiqueta.USUARIO_CONFIGURACION %></h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->

			<div style="display: none;" id="error-alert"
				class="alert alert-danger fade in">
				<strong> <span id="error-head"><%=errorHead%></span></strong> <span
					id="error-body"><%=errorBody%></span>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div style="height: 54px;" class="panel-heading">
							<button style="float: right;" type="button" class="boton_crear btn btn-primary" data-toggle="modal" data-target="#myModal"><%= etiqueta.AGREGAR_USUARIO_CONFIGURACION %></button>
						</div>
						<!--  Buscador -->
						<br/>
						<form:form method="post" enctype="multipart/form-data; charset=UTF-8" action="usuarioConfSolicitanteBuscar" modelAttribute="usuarioConfSolicitanteDTO" class="img-rounded busquedaForm">
							<div class="row  header-search">
								<div class="col-md-12">
									<h4 style="" class="col-md-10">
											<%=etiqueta.ESPECIFIQUE_CRITERIOS_BUSQUEDA%>
									</h4>
									<button id="buscar" onclick="return validar();"
										style="" type="submit"
									class="btn btn-primary col-md-2">
										<%=etiqueta.BUSCAR%></button>
			
									<a id="mostrar-busqueda" onclick=""
										style="display:none"
										class="btn btn-primary col-md-2">
										<%=etiqueta.MOSTRAR%></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<div class="form-group">
											<!-- Usuario -->
											<label><%=etiqueta.NOMBRE%>:</label>
											<form:input cssClass="form-control selectpicker" path="nombreUsuario" value=""/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<!-- Tipo de Solicitud -->
											<label><%=etiqueta.TIPOS_SOLICITUD%>:</label>
											<form:select path="idTipoSolicitud" cssClass="form-control selectpicker" id="tipoSolicitud_list">
												<option value="-1"><%= etiqueta.SELECCIONE %></option>
												<c:forEach items="${tipoSolicitudList}" var="tipoSolist">
													<option value="${tipoSolist.idTipoSolicitud}">${tipoSolist.descripcion}</option>
												</c:forEach>
											</form:select>	
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<!-- Locacion -->
											<label><%=etiqueta.LOCACION%>:</label>
											<form:select path="locacion" cssClass="form-control selectpicker" id="locacion_list">
												<option value="-1"><%= etiqueta.SELECCIONE %></option>
												<c:forEach items="${locacionList}" var="locList">
													<option value="${locList.idLocacion}">${locList.numero} - ${locList.descripcion}</option>
												</c:forEach>
											</form:select>		
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<div class="form-group">
											<!-- Cuenta Contable -->
											<label><%=etiqueta.CUENTA_CONTABLE%>:</label>
											<form:select path="cuentaContable" 
												cssClass="form-control selectpicker" 
												data-live-search="true"
												id="cuentaContable_list">
												<option value="-1"><%= etiqueta.SELECCIONE %></option>
												<c:forEach items="${cuentaContableList}" var="cuentaContList">
													<option value="${cuentaContList.idCuentaContable}">${cuentaContList.numeroCuentaContable} - ${cuentaContList.descripcion}</option>
												</c:forEach>
											</form:select>	
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<!-- Activo -->
											<div style="margin-bottom: 0px; margin-top: 34px;">
												<div class="checkbox fix-check">
													<form:checkbox id="" path="activo"
														value="" /><label><%=etiqueta.ACTIVO%></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</form:form>
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>

		</div>

	</div>





	

	<jsp:include page="template/includes.jsp" />

	<!-- DataTables JavaScript -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	<script-
		src="${pageContext.request.contextPath}/resources/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

	<script>
	
	e_titulo = '<%= etiqueta.CONFIGURACION_SOLICITANTE %>';
	    
	$(function(){
		
		$("#usuarioConfSolicitanteForm").submit(function(event){
			$("#error-sign").hide();
			var ban = false;
			
			
			//USUARIO
			if($("#usuario_list").val() == '-1'){
				ban = true;
				$('[data-id="usuario_list"]').addClass("errorx");
			}else{
				$('[data-id="usuario_list"]').removeClass("errorx");
			}
			
			//tipo solicitud
			if($("#tipoSolicitud_list").val() == '-1'){
				ban = true;
				$("#tipoSolicitud_list").addClass("errorx");
			}else{
				$("#tipoSolicitud_list").removeClass("errorx");
			}
			
			//locacion
			if($("#locacion_list").val() == '-1'){
				ban = true;
				$("#locacion_list").addClass("errorx");
			}else{
				$("#locacion_list").removeClass("errorx");
			}
			
			//cuenta contable
			if($("#cuentaContable_list").val() == '-1'){
				ban = true;
				$('[data-id="cuentaContable_list"]').addClass("errorx");
			}else{
				$('[data-id="cuentaContable_list"]').removeClass("errorx");
			}
			
			
			if(ban){
				$("#error-sign").show();
				console.log("false");
				loading(false);
				return false;
			}else{
				console.log("true");
				return true;
			}
		});
	});
	
    $(document).ready(function() {
    
		console.log("<%=errorHead%>");
    	
    	if("<%=errorHead%>" != "null"){
	    	if(true){
	    		$("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
	    		    $("#error-alert").alert('close');
	    		    
	    		});
	    	}
    	}
    	
        $('#dataTables-usuarioConfSolicitante').DataTable({
                responsive: true
        });
        
        $('#myModal').on('hidden.bs.modal', function () {
        	// limpiar formulario.
        	$("#idUsuarioConfSolicitante").val(0);
        	$("#usuario_list").val(-1);
        	$("#tipoSolicitud_list").val(-1);
        	$("#locacion_list").val(-1);
        	$("#cuentaContable_list").val(-1);
        	$('.selectpicker').selectpicker('refresh');
        });
        
    });
    
    function editar(id) {
        if(id > 0){
        console.log(id);
       		 loading(true);
            $.ajax({
                type: "GET",
                cache: false,
                url: "${pageContext.request.contextPath}/getUsuarioConfSolicitante",
                async: true,
                data: "intxnId=" + id,
                success: function(response) {
                	loading(false);
                	console.log(response.descripcion +" -> "+response.numero+" -> "+response.id);
                	$("#usuario_list").val(response.idUsuario);
                	$("#tipoSolicitud_list").val(response.idTipoSolicitud);
                	$("#locacion_list").val(response.idLocacion);
                	$("#cuentaContable_list").val(response.idCuentaContable);
                	//$('select[id=cuentaContable_list]').val(response.idCuentaContable);
                	$('.selectpicker').selectpicker('refresh')
                	$("#idUsuarioConfSolicitante").val(response.idUsuarioSolicitante);
                	// mostrando el modal
                	//abrir ventana modal
                	$('#myModal').modal('show'); 
                },
                error: function(e) {
                	loading(false);
                    console.log('Error: ' + e);
                },
            }); 
            
        	}//if
        }
        
        
        function eliminar(id){
        	console.log(id);
        	$("#eliminarButton").prop("href","deleteUsuarioConfSolicitante?id="+id);
        	$("#myModalEliminar").modal('show');
        	
        }
        
        function getValue() {
            var x = document.getElementById("cmbTipoUsuarioConfSolicitante").value;
            $("input[name='idTipoUsuarioConfSolicitante']").val(x);
        }

        function validar(){
        	
        }
    </script>

	<!-- Scripts especificos ....  -->

</body>
</html>
